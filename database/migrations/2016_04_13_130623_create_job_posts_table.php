<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateJobPostsTable extends Migration {

	public function up()
	{
		Schema::create('job_posts', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('job_title_slug');
			$table->string('job_title');
			$table->string('job_type');
			$table->decimal('minimum_salary', 20,2);
			$table->decimal('maximum_salary', 20,2);
			$table->string('paid_per', 6);
			$table->string('job_location');
			$table->binary('job_description');
			$table->binary('skills');
			$table->string('experience_level');
			$table->timestamp('start_date');
			$table->string('contact_name');
			$table->string('contact_number');
			$table->string('reference')->unique();
			$table->integer('company_profile_id')->unsigned();
			$table->integer('user_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('job_posts');
	}
}