<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserGroupsTable extends Migration {

	public function up()
	{
		Schema::create('user_groups', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('name')->unique();
			$table->string('status');
			$table->string('sku');
			$table->string('associated_access');
			$table->decimal('price', 20,2);
			$table->string('payment_plan_type'); //trial , subscription , one_time_payment
			$table->text('description');
			$table->string('attributes');
			$table->string('purchase_link');
		});
	}

	public function down()
	{
		Schema::drop('user_groups');
	}
}