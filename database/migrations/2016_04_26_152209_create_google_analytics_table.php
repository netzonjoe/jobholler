<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGoogleAnalyticsTable extends Migration {

	public function up()
	{
		Schema::create('google_analytics', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->binary('code');
		});
	}

	public function down()
	{
		Schema::drop('google_analytics');
	}
}