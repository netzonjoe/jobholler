<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmailTasksTable extends Migration {

	public function up()
	{
		Schema::create('email_tasks', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->binary('data');
			$table->boolean('is_sent')->default(false);
		});
	}

	public function down()
	{
		Schema::drop('email_tasks');
	}
}