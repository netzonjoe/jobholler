<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAppliedJobsTable extends Migration {

	public function up()
	{
		Schema::create('applied_jobs', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('firstname');
			$table->string('lastname');
			$table->string('email');
			$table->string('linkedin_profile');
			$table->string('curriculum_vitae');
			$table->text('cover_leter');
			$table->integer('job_posts_id')->unsigned();
			$table->integer('user_id');
			$table->string('status_request')->default('pending');
		});
	}

	public function down()
	{
		Schema::drop('applied_jobs');
	}
}