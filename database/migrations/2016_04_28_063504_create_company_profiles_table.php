<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCompanyProfilesTable extends Migration {

	public function up()
	{
		Schema::create('company_profiles', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->timestamps();
			$table->string('company_name')->unique();
			$table->string('company_name_slug');
			$table->string('tagline');
			$table->binary('company_description');
			$table->string('company_color');
			$table->string('company_website');
			$table->string('company_logo');
			$table->string('company_icon');
			$table->string('company_video');
			$table->binary('company_perks');
			$table->binary('social_networks');
		});
	}

	public function down()
	{
		Schema::drop('company_profiles');
	}
}