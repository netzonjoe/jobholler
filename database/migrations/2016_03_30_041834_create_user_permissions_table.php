<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserPermissionsTable extends Migration {

	public function up()
	{
		Schema::create('user_permissions', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('uri');
			$table->string('method', 4);
			$table->boolean('is_enabled');
			$table->integer('user_group_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('user_permissions');
	}
}