<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNotificationsTable extends Migration {

	public function up()
	{
		Schema::create('notifications', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('user_id');
			$table->boolean('is_viewed')->default(false);
			$table->boolean('is_clicked')->default(false);
			$table->binary('data');
		});
	}

	public function down()
	{
		Schema::drop('notifications');
	}
}