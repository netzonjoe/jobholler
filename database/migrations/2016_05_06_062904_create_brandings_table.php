<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBrandingsTable extends Migration {

	public function up()
	{
		Schema::create('brandings', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('name')->unique();
            $table->boolean('status')->default(true);
			$table->string('title');
			$table->text('description');
			$table->decimal('price', 20,2);
			$table->string('per_payment_type');
            $table->string('associated_access');
            $table->string('payment_plan_type'); //trial , subscription , one_time_payment
		});
	}

	public function down()
	{
		Schema::drop('brandings');
	}
}