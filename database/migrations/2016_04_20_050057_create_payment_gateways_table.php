<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePaymentGatewaysTable extends Migration {

	public function up()
	{
		Schema::create('payment_gateways', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('name');
			$table->string('test_key');
			$table->string('test_secret');
			$table->string('live_key');
			$table->string('live_secret');
		});
	}

	public function down()
	{
		Schema::drop('payment_gateways');
	}
}