<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateForeignKeys extends Migration {

	public function up()
	{
		Schema::table('user_permissions', function(Blueprint $table) {
			$table->foreign('user_group_id')->references('id')->on('user_groups')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('users', function(Blueprint $table) {
			$table->foreign('user_group_id')->references('id')->on('user_groups')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('job_posts', function(Blueprint $table) {
			$table->foreign('company_profile_id')->references('id')->on('company_profiles')
						->onDelete('cascade')
						->onUpdate('no action');
		});
		Schema::table('job_posts', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('no action')
						->onUpdate('no action');
		});
		Schema::table('resumes', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('cascade')
						->onUpdate('no action');
		});

		Schema::table('company_profiles', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('cascade')
						->onUpdate('no action');
		});

		Schema::table('applied_jobs', function(Blueprint $table) {
			$table->foreign('job_posts_id')->references('id')->on('job_posts')
						->onDelete('cascade')
						->onUpdate('no action');
		});
		// Schema::table('applied_jobs', function(Blueprint $table) {
		// 	$table->foreign('user_id')->references('id')->on('users')
		// 				->onDelete('no action')
		// 				->onUpdate('no action');
		// });
	}

	public function down()
	{
		Schema::table('user_permissions', function(Blueprint $table) {
			$table->dropForeign('user_permissions_user_group_id_foreign');
		});
		Schema::table('users', function(Blueprint $table) {
			$table->dropForeign('users_user_group_id_foreign');
		});
		Schema::table('job_posts', function(Blueprint $table) {
			$table->dropForeign('job_posts_company_profile_id_foreign');
		});
		Schema::table('job_posts', function(Blueprint $table) {
			$table->dropForeign('job_posts_user_id_foreign');
		});
		Schema::table('resumes', function(Blueprint $table) {
			$table->dropForeign('resumes_user_id_foreign');
		});
		Schema::table('company_profiles', function(Blueprint $table) {
			$table->dropForeign('company_profiles_user_id_foreign');
		});
		Schema::table('applied_jobs', function(Blueprint $table) {
			$table->dropForeign('applied_jobs_jobs_id_foreign');
		});
		Schema::table('applied_jobs', function(Blueprint $table) {
			$table->dropForeign('applied_jobs_user_id_foreign');
		});
	}
}