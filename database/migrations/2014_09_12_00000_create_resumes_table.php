<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateResumesTable extends Migration {

	public function up()
	{
		Schema::create('resumes', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('user_id')->unsigned();
			$table->boolean('status')->default(true);
			$table->string('viewable')->default('enabled'); /*disabled*/
			$table->string('full_name');
			$table->string('tagline');
			$table->text('about_me');
			$table->string('profile_photo');
			$table->string('years_of_experience');
			$table->decimal('expected_salary', 20, 2);
			$table->binary('personal_core_skills');
			$table->binary('technical_core_skills');
			$table->binary('hobbies');
			$table->binary('educations');
			$table->binary('experiences');
			$table->string('resume_document');
			$table->string('cover_photo');
			$table->string('cover_photo_selection');
		});
	}

	public function down()
	{
		Schema::drop('resumes');
	}
}