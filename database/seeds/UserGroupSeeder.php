<?php

use Illuminate\Database\Seeder;

class UserGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   

    	DB::table('user_groups')->insert([
            'name' => 'Administrator',
            'status' => 'active',
            'sku' => 'sku',
            'associated_access' => 'none',
            'price' => '0.00',
            'payment_plan_type' => 'none',
            'description' => 'description',
            'attributes' => 'attributes',
            'purchase_link' => 'purchase_link'
        ]);

       // 	DB::table('user_groups')->insert([
       //      'name' => 'Candidate',
       //      'has_sub_role' => true,
       //      'sub_role' => 'basic-me',
       //  ]);

       //  DB::table('user_groups')->insert([
       //      'name' => 'Candidate',
       //      'has_sub_role' => true,
       //      'sub_role' => 'amplify-me',
       //  ]);

       //  DB::table('user_groups')->insert([
       //      'name' => 'Candidate',
       //      'has_sub_role' => true,
       //      'sub_role' => 'ignite-me',
       //  ]);


      	// DB::table('user_groups')->insert([
       //      'name' => 'Employer',
       //      'has_sub_role' => true,
       //      'sub_role' => 'basic',
       //  ]);

       //  DB::table('user_groups')->insert([
       //      'name' => 'Employer',
       //      'has_sub_role' => true,
       //      'sub_role' => 'amplify',
       //  ]);

       //  DB::table('user_groups')->insert([
       //      'name' => 'Employer',
       //      'has_sub_role' => true,
       //      'sub_role' => 'ignite',
       //  ]);

        DB::table('users')->insert([
            'firstname' => 'admin',
            'lastname' => 'admin',
            'phone' => '1234567890',
            'email' => 'admin@admin.com',
            'password' => bcrypt('admin'),
            'user_group_id' => App\User_group::where('name' , 'administrator')->first()->id
        ]);
     
    }

}
