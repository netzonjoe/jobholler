<?php

use Illuminate\Database\Seeder;

class PaymentGatewaySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('payment_gateways')->insert([
            'name' => 'stripe',
            'test_key' => 'pk_test_zLublRclAITdrsILtJjSGlam',
            'test_secret' => 'sk_test_vcgp50GZZIwZH4gtCv1tePxb',
            'live_key' => 'pk_live_AmxbZfvZJavLQCyKVTHdBZH7',
            'live_secret' => 'sk_live_3pfFm9EHmcZuSGeFEPxTvVHB'
        ]);
    }
}
