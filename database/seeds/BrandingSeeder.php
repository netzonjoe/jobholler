<?php

use Illuminate\Database\Seeder;

class BrandingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {	

       	DB::table('brandings')->insert([
            'name' => 'ADVISE-QUESTIONNAIRE',
            'title' => 'ADVISE QUESTIONNAIRE',
            'description' => 'Aspernatur omnis nemo omnis porro cupiditate quaera!',
            'price' => '5.00',
            'per_payment_type' => 'ENTRY',
            'associated_access' => 'employer',
            'payment_plan_type' => 'trial'
        ]);

        DB::table('brandings')->insert([
            'name' => 'BRANDING-AUDIT',
            'title' => 'BRANDING AUDIT',
            'description' => 'Aspernatur omnis nemo omnis porro cupiditate quaera!',
            'price' => '10.00',
            'per_payment_type' => 'AUDIT',
            'associated_access' => 'employer',
            'payment_plan_type' => 'trial'
        ]);

        DB::table('brandings')->insert([
            'name' => 'FEATURED-ON-HOMEPAGE',
            'title' => 'FEATURED ON HOMEPAGE',
            'description' => 'Aspernatur omnis nemo omnis porro cupiditate quaera!',
            'price' => '39.00',
            'per_payment_type' => 'MONTH',
            'associated_access' => 'employer',
            'payment_plan_type' => 'trial'
        ]);

        DB::table('brandings')->insert([
            'name' => 'DEDICATED-CAREERS-TWITTER',
            'title' => 'DEDICATED CAREERS TWITTER',
            'description' => 'Aspernatur omnis nemo omnis porro cupiditate quaera!',
            'price' => '99.00',
            'per_payment_type' => 'MONTH',
            'associated_access' => 'employer',
            'payment_plan_type' => 'trial'
        ]);

        DB::table('brandings')->insert([
            'name' => 'DONE-FOR-YOU-BLOG-POSTING',
            'title' => 'DONE FOR YOU BLOG POSTING',
            'description' => 'Aspernatur omnis nemo omnis porro cupiditate quaera!',
            'price' => '5.00',
            'per_payment_type' => 'POST',
            'associated_access' => 'employer',
            'payment_plan_type' => 'trial'
        ]);

    }
}
