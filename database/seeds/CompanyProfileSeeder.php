<?php

use Illuminate\Database\Seeder;

class CompanyProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {	
    	DB::table('company_profiles')->insert([
            'company_name_slug' => 'jobholler',
            'company_name' => 'jobholler',
            'tagline' => 'Be Part Of The UK’s Fastest Growing Success Story.',
            'company_description' => serialize(''),
            'company_color' => '#000000',
            'company_website' => 'http://jobholler.dev/jobholler',
            'company_logo' => '',
            'company_icon' => '',
            'company_video' => '',
            'company_perks' => serialize('[]'),
            'social_networks' => serialize('[[{"key":"network_type","value":"fa fa-facebook","$$hashKey":"object:368"},{"key":"url","value":"https://www.facebook.com/wrenliving","$$hashKey":"object:369"}],[{"key":"network_type","value":"fa fa-twitter","$$hashKey":"object:418"},{"key":"url","value":"https://twitter.com/wrenkitchens","$$hashKey":"object:419"}],[{"key":"network_type","value":"fa fa-google-plus","$$hashKey":"object:466"},{"key":"url","value":"https://plus.google.com/+wrenkitchens/posts","$$hashKey":"object:467"}],[{"key":"network_type","value":"fa fa-linkedin","$$hashKey":"object:514"},{"key":"url","value":"https://www.linkedin.com/company/wren-kitchens","$$hashKey":"object:515"}],[{"key":"network_type","value":"fa fa-youtube","$$hashKey":"object:562"},{"key":"url","value":"https://www.youtube.com/user/WrenKitchens","$$hashKey":"object:563"}],[{"key":"network_type","value":"fa fa-link","$$hashKey":"object:610"},{"key":"url","value":"http://www.wrenkitchens.com/","$$hashKey":"object:611"}],[{"key":"network_type","value":"fa fa-pinterest-p ","$$hashKey":"object:658"},{"key":"url","value":"#","$$hashKey":"object:659"}]]'),
            'user_id' => App\User::where('email' , 'admin@admin.com')->first()->id
        ]);
    }
}

