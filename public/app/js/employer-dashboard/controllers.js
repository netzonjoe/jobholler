'use strict';
var jobholler = angular.module('jobhollerControllers', []);

jobholler.controller('JobhollerCtrl', ['$scope', 'global', '$timeout',  function ($scope, global, $timeout) {
	$scope.$alerts = [];
	global.alert.listTrigger(function (alert) {console.log(alert);
        $scope.$alerts.push(alert);
        $timeout(function () {
        	$scope.$alerts.splice($scope.$alerts.length - 1, 1);
        }, 8000);
	});
}]);

jobholler.controller('HeaderCtrl', ['$scope', 'http', 'validator', 'global', function ($scope, http, validator, global) {
	$scope.baseurl = $baseurl;
	$scope.has_company_profile = false;
	global.company_profile.listTrigger(function (company_profile) {
		if (company_profile !== null) {
			$scope.has_company_profile = true;
	   		$scope.company_name = company_profile.company_name;
		}
	});
	
	http.getCompanyProfileAjax({});
}]);

jobholler.controller('JobsCtrl', ['$scope', 'http', 'validator', 'global', 'generator',	function ($scope, http, validator, global, generator) {
	$scope.$job_listing  = $job_listing;
	$scope.$jobs = angular.copy($jobs);
    $scope._jobs = angular.copy($jobs);
	global.jobs.listTrigger(function ($jobs) {
        $scope.$jobs = angular.copy($jobs);
        $scope._jobs = angular.copy($jobs);
        window.$jobs = angular.copy($jobs);
	});
	$scope.baseurl = $baseurl;
	http.getAllPostedJobsAjax({});
	var options = { 
       componentRestrictions: {country: "uk"}
    }; 
	var inputAutocomplete = document.getElementById('job_location_input');
	var autocomplete = new google.maps.places.Autocomplete(inputAutocomplete, options);
	
	autocomplete.addListener('place_changed', function(event) {
		$scope.data.job_location = inputAutocomplete.value; 
  	});



	$scope.onClearForm = function () {
		if ($scope.updateJob) {
			$scope.updateJob = false;
			$scope.onClearForm();
			return;
		}

		$scope.data = {
			reference: generator.reference(),
			job_title_slug: '',
		    job_title: '',
		    job_type: '',
		    job_location: '',
		    minimum_salary: '',
		    maximum_salary: '',
		    paid_per: 'year',
		    job_description: '',
		    experience_level: '',
		    start_date: '',
		    contact_name: '',
		    contact_number: '',
		    skills: [
		    	{
		    		'name': '',
		    		'level': '1'
		    	}
		    ]
		};
		$('.jobs.form-validation').find('.ng-dirty').removeClass('ng-dirty');
	};

	$scope.onClearForm();

	$scope.errors = {};

	$scope.$watch('errors', function(errors) {
        if (errors !== undefined) { 
        	validator.errors = errors;
        }
    });

	$scope.hasError = validator.hasError;
	
	$scope.showErrorBlock = validator.showErrorBlock;

	$scope.onSubmitForm = function () {
		$scope.onCreateJob();
	};

	$scope.today = function() {
        $scope.data.start_date = new Date();
    };
    $scope.today();

    $scope.clear = function() {
        $scope.data.start_date = null;
    };

    $scope.toggleMin = function() {
        $scope.minDate = $scope.minDate ? null : new Date();
    };

    $scope.toggleMin();
    $scope.maxDate = new Date(2020, 5, 22);

    $scope.open1 = function() {
        $scope.popup1.opened = true;
    };

    $scope.open2 = function() {
        $scope.popup2.opened = true;
    };

    $scope.setDate = function(year, month, day) {
        $scope.$apply(function () {
            $scope.data.start_date = new Date(year, month, day);
        });
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
    $scope.altInputFormats = ['M!/d!/yyyy'];

    $scope.popup1 = {
        opened: false
    };

    $scope.popup2 = {
        opened: false
    };

    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    var afterTomorrow = new Date();
    afterTomorrow.setDate(tomorrow.getDate() + 1);

    $scope.experience_level = [
    	{
    		key: 'junior',
    		value: 'Junior(1 Year)'	
    	},
    	{
    		key: 'mid',
    		value: 'Mid(2 Years)'	
    	},
    	{
    		key: 'senior',
    		value: 'Senior(3 Years)'	
    	},
    	{
    		key: 'manager',
    		value: 'Manager(4 Year)'	
    	}
    ];


    $scope.onAddNewSkill = function () {
    	var obj = $scope.data.skills[$scope.data.skills.length - 1];
    	if (obj.name !== '' && obj.level.toString() !== '') {
    		if (obj.name.trim() !== '' && obj.level.toString().trim() !== '') {
				$scope.data.skills.push({
		    		name: '',
		    		level: '1'
		    	});    			
    		}
    	}
    };	

    $scope.onRemoveSkill = function (index) {
		if ($scope.data.skills.length > 1) 
			var tempList = angular.copy($scope.data.skills);
			var len = tempList.length;
    		$scope.data.skills = tempList.slice(0, index).concat( tempList.slice(index + 1) );	
	};

    $scope.numPerPageOpt = [3, 5, 10, 20];

	$scope.numPerPage = $scope.numPerPageOpt[2];

	$scope.currentPage = 1;

	$scope.select = function(page) {
        var end, start;
        start = (page - 1) * $scope.numPerPage;
        end = start + $scope.numPerPage;
        return $scope.$jobs =  angular.copy($scope._jobs.slice(start, end));
    };

	$scope.onNumPerPageChange = function() {
        $scope.select(1);
        return $scope.currentPage = 1;
    };

    $scope.select(1);

    $scope.updateJob = false;

	$scope.onCreateJob = function () {
		if ($scope.updateJob) {

			var data = angular.copy($scope.data);
			if (data.job_title !== '') {
				data.job_title_slug = $scope.data.job_title.toLowerCase().replace(new RegExp('[^a-zA-Z0-9\\-\\s]', 'g'), '').replace(/\s+/g, '-');	
			}

			data.minimum_salary = (data.minimum_salary === undefined) ? '' : parseFloat(data.minimum_salary);
			data.maximum_salary = (data.maximum_salary === undefined) ? '' : parseFloat(data.maximum_salary);
			if (data.start_date !== '') {
				var start_date = data.start_date;
				data.start_date = start_date.getFullYear() + '-' + ("0" + (start_date.getMonth() + 1)).slice(-2) + '-' + ("0" + start_date.getDate()).slice(-2);
			} else {
				data.start_date = '';
			}

			http.updateJob(data).then(function (data) {
				$scope.errors = {};
				$scope.onClearForm();
				$scope.updateJob = false;
			}, function (data) {
				if (data.errors) {
					$scope.errors = data.errors;
				}
			});
		} else {
			var data = angular.copy($scope.data);
			if (data.job_title !== '') {
				data.job_title_slug = $scope.data.job_title.toLowerCase().replace(new RegExp('[^a-zA-Z0-9\\-\\s]', 'g'), '').replace(/\s+/g, '-');	
			}
			data.minimum_salary = (data.minimum_salary === undefined) ? '' : parseFloat(data.minimum_salary);
			data.maximum_salary = (data.maximum_salary === undefined) ? '' : parseFloat(data.maximum_salary);
			if (data.start_date !== '') {
				var start_date = data.start_date;
				data.start_date = start_date.getFullYear() + '-' + ("0" + (start_date.getMonth() + 1)).slice(-2) + '-' + ("0" + start_date.getDate()).slice(-2);
			} else {
				data.start_date = '';
			}
			
			http.createJob(data).then(function (data) {
				$scope.errors = {};
				$scope.onClearForm();
			}, function (data) {
				if (data.errors) {
					$scope.errors = data.errors;
				}
			});
		}
	};

	$scope.onCloneJob = function (row_job) {
		var job = angular.copy(row_job);
		job.minimum_salary = parseFloat(job.minimum_salary);
		job.maximum_salary = parseFloat(job.maximum_salary);
		var dateObj = job.start_date.split("-");
		var year = dateObj[0];
		var month = dateObj[1];
		var day = dateObj[2].split(" ")[0];
        job.start_date = new Date(year, parseInt(month) - 1, day);
        var start_date = job.start_date;
		job.start_date = start_date.getFullYear() + '-' + ("0" + (start_date.getMonth() + 1)).slice(-2) + '-' + ("0" + start_date.getDate()).slice(-2);
        job.reference = generator.reference();
        http.cloneJob(job);
	};

	$scope.onUpdateJob = function (job) {
		$scope.updateJob = true;
		job.minimum_salary = parseFloat(job.minimum_salary);
		job.maximum_salary = parseFloat(job.maximum_salary);
		var dateObj = job.start_date.split("-");
		var year = dateObj[0];
		var month = dateObj[1];
		var day = dateObj[2].split(" ")[0];
		$scope.data = angular.copy(job);
        $scope.data.start_date = new Date(year, parseInt(month) - 1, day);
	};

	$scope.onDeleteJob = function (job) {
		var confirm = window.confirm('Are you sure?');
		if(confirm) {
			http.deleteJob(job);
		}
		return false;
	};

	$scope.searchKeywords = '';
	$scope.onSearchFilterJobs = function () {
		if ($scope.searchKeywords === '') {
			$scope.$jobs = angular.copy($scope._jobs);
			return;
		}
        var jobs = angular.copy($scope._jobs);
		var newList = [];
		for (var i in jobs) {
 			var indexOfValue = jobs[i].job_title.toLowerCase().indexOf($scope.searchKeywords.toLowerCase());
 			if (indexOfValue !== -1) {
 				newList.push(jobs[i]);
 			}
		}
		$scope.$jobs = angular.copy(newList);
	};

}]);

jobholler.controller('Expired_jobsCtrl', ['$scope', 'http', 'validator', 'global', function ($scope, http, validator, global) {

	$scope.$expired_jobs = angular.copy($expired_jobs);
	$scope._expired_jobs = angular.copy($expired_jobs);

	http.getAllExpiredJobsAjax({});

	global.jobs.listTrigger(function (jobs) {
   		window.$jobs = angular.copy(jobs);
	});

	global.expired_jobs.listTrigger(function (expired_jobs) {
        $scope.$expired_jobs = angular.copy(expired_jobs);
   		$scope._expired_jobs = angular.copy(expired_jobs);
   		window.$expired_jobs = angular.copy(expired_jobs);
	});
	
	$scope.onRepostJob = function (expired_jobs) {
        http.repostJob(expired_jobs);
	};

	$scope.searchKeywords = '';
	$scope.onSearchFilterExpired = function () {
		if ($scope.searchKeywords === '') {
			$scope.$expired_jobs = angular.copy($scope._expired_jobs);
			return;
		}
        var expired_jobs = angular.copy($scope._expired_jobs);
		var newList = [];
		for (var i in expired_jobs) {
 			var indexOfValue = expired_jobs[i].job_title.toLowerCase().indexOf($scope.searchKeywords.toLowerCase());
 			if (indexOfValue !== -1) {
 				newList.push(expired_jobs[i]);
 			}
		}
		$scope.$expired_jobs = angular.copy(newList);
	};
}]);

jobholler.controller('Company_profileCtrl', ['$scope', 'http', 'validator', 'global', '$rootScope', function ($scope, http, validator, global, $rootScope) {

	$scope.icons = [
		'ti-flickr', 'ti-flickr-alt', 'ti-instagram', 'ti-github', 'ti-facebook'
	];

	$scope.baseurl = $baseurl;
	$scope.errors = {};

	$scope.$watch('errors', function(errors) {
        if (errors !== undefined) { 
        	validator.errors = errors;
        }
    });

	$scope.hasError = validator.hasError;
	
	$scope.showErrorBlock = validator.showErrorBlock;

	$scope.has_company_profile = false;
	global.company_profile.listTrigger(function (company_profile) {
		if (company_profile !== null) {
			$scope.has_company_profile = true;
			company_profile.company_description = JSON.parse(company_profile.company_description);
			company_profile.company_perks = JSON.parse(company_profile.company_perks);
			company_profile.social_networks = JSON.parse(company_profile.social_networks);
			$scope.company_logo_src = company_profile.company_logo;
			$scope.company_icon_src = company_profile.company_icon;
			
			$('#company_perks').html('');
			$('#social_networks').html('');
	   		$scope.data = angular.copy(company_profile);
	   		$scope.addNewRow('company_perks');
			$scope.addNewRow('social_networks');
			$scope.data.company_color_text = $scope.data.company_color;
		}
	});
	
	http.getCompanyProfileAjax({});

	$scope.$watch(function () {
		if ($scope.data.company_logo === '') {
			$scope.company_logo_src = null;
		}
		if ($scope.data.company_icon === '') {
			$scope.company_icon_src = null;
		}

		if (/^#[0-9A-F]{6}$/i.test($scope.data.company_color_text)) {
			// $scope.data.company_color = $scope.data.company_color_text; 
			$scope.notValidColor = false;
		} else {
			$scope.notValidColor = true;
		}
	});

	$scope.$watch('data.company_color', function () {
		if (/^#[0-9A-F]{6}$/i.test($scope.data.company_color)) {
			$scope.data.company_color_text = $scope.data.company_color; 
		}
	});

	$scope.$watch('data.company_color_text', function () {
		if (/^#[0-9A-F]{6}$/i.test($scope.data.company_color_text)) {
			$scope.data.company_color = $scope.data.company_color_text; 
		}
	});

	$scope.data = {
		company_name_slug: '',
		company_name: '',
		tagline: '',
		company_description: '',
		company_color: '#ffffff',
		company_website: '',
		company_logo: '',
		company_icon: '',
		company_video: '',
		company_perks: [
			/*[
				{
					key : 'title',
					value: ''
				},
				{	
					key : 'description',
					value: ''
				},
				{
					key : 'icon',
					value: ''
				}
			]*/
		],
		social_networks: [
			/*[
				{
					key : 'network_type',
					value: ''
				},
				{	
					key : 'url',
					value: ''
				}
			]*/
		]
	};

	$scope.onSaveForm = function () {
		var form_data = new FormData();
		if ($scope.data.company_color_text !== '') {
			$scope.data.company_color = $scope.data.company_color_text;
		}

		if ($scope.data.company_name !== '') {
			$scope.data.company_name_slug = $scope.data.company_name.toLowerCase().replace(new RegExp('[^a-zA-Z0-9\\-\\s]', 'g'), '').replace(/\s+/g, '-');	
		}
		
		for ( var key in $scope.data ) {
			var value = $scope.data[key];
			if (key === 'company_description' || 
				key === 'company_perks' || 
				key === 'social_networks') {
				value = JSON.stringify($scope.data[key]);
			} 
		    form_data.append(key, value);
		}
		
		http.saveCompanyProfile(form_data).then(function (data) {
			$scope.errors = {};
		}, function (data) {
			if (data.errors) {
				$scope.errors = data.errors;
			}
		});;
	};

	$scope.addNewRow = function (key) {
		if (key === 'company_perks') {
			var lastIndex = $scope.data.company_perks.length - 1;
			if (lastIndex === -1) {
				$scope.data.company_perks.push([
					{
						key : 'title',
						value: ''
					},
					{	
						key : 'description',
						value: ''
					},
					{
						key : 'icon',
						value: ''
					}
				]);
			} else if ($scope.data.company_perks[lastIndex][0].value !== '' && 
				$scope.data.company_perks[lastIndex][1].value !== '' && 
				$scope.data.company_perks[lastIndex][2].value !== '') {
				$scope.data.company_perks.push([
					{
						key : 'title',
						value: ''
					},
					{	
						key : 'description',
						value: ''
					},
					{
						key : 'icon',
						value: ''
					}
				]);
			}
		} else if (key === 'social_networks') {
			var lastIndex = $scope.data.social_networks.length - 1;
			if (lastIndex === -1) {
				$scope.data.social_networks.push([
					{
						key : 'network_type',
						value: ''
					},
					{	
						key : 'url',
						value: ''
					}
				]);
			} else if ($scope.data.social_networks[lastIndex][0].value !== '' && 
				$scope.data.social_networks[lastIndex][1].value !== '' || lastIndex === -1) {
				$scope.data.social_networks.push([
					{
						key : 'network_type',
						value: ''
					},
					{	
						key : 'url',
						value: ''
					}
				]);
			}
		}
		
	};

	$scope.onRemoveRow = function (data) {
		if ($scope.data[data.key].length > 1) 
			var tempList = angular.copy($scope.data[data.key]);
			var len = tempList.length;
    		$scope.data[data.key] = tempList.slice(0, data.index).concat( tempList.slice(data.index + 1) );	
	};

	$scope.onRemoveImage = function (data_key , image_src) {
		$scope.data[data_key] = '';
		$scope[image_src] = null;
		$('#' + image_src).val('');
	};
	    	
}]);


jobholler.controller('My_accountCtrl', ['$scope', 'http', 'validator', 'global', '$uibModal', function ($scope, http, validator, global, $uibModal) {
	console.log(global.country);
	console.log(global.state);
	$scope.$country = global.country;
	$scope.$state = global.state;
	$scope.data = { 
		firstname: '',
		lastname: '',
		email: '',
		phone: '',
		password: '',
		confirm_password: '',
		address: '',
		city: '',
		state: '',
		zip: '',
		country: ''
	};
	$scope.state_label = '';
	$scope.country_label = '';
	$scope.getCountry = function () {
		if ($scope.data.country !== '') {
			for (var i in $scope.$country) {
				if ($scope.$country[i][0] == $scope.data.country) {
					$scope.country_label = $scope.$country[i][1];
					break;
				}
			}
		}
		return ($scope.country_label === '') ? '' : $scope.country_label;
	};

	$scope.getState = function () {
		if ($scope.data.state !== '') {
			for (var i in $scope.$state) {
				if ($scope.$state[i][0] == $scope.data.state) {
					$scope.state_label = $scope.$state[i][1];
					break;
				}
			}
		}
		return ($scope.state_label === '') ? '' : $scope.state_label;
	};


	$scope.ifNotYetReady = true;
	global.user_account_details.listTrigger(function (user_account_details) {console.log(user_account_details);
		$scope.ifNotYetReady = false;
		$scope.data = angular.copy(user_account_details);
	});
	
	http.getEmployerAccountAjax({});

	$scope.onOpenModal = function (templateUrl) {
		if ($scope.ifNotYetReady) return false;
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: templateUrl,
            controller: templateUrl.split('.')[0],
            // size: 'lg',
            resolve: {
                data: function () {
                    return $scope.data;
                },
                country: function () {
                    return global.country;
                },
                state: function () {
                    return global.state;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
         	console.log('Success Modal dismissed at: ' + new Date());
        }, function () {
            console.log('Modal dismissed at: ' + new Date());
        });
    };

}]);

jobholler.controller('Account_detailsCtrl', ['$scope', '$uibModalInstance', 'http', 'validator', 'global', 'data' , function ($scope, $uibModalInstance, http, validator, global, data) {
	$scope.data = data;

	$scope.errors = {};
	$scope.$watch('errors', function(errors) {
        if (errors !== undefined) { 
        	validator.errors = errors;
        }
    });
	$scope.hasError = validator.hasError;
	$scope.showErrorBlock = validator.showErrorBlock;


	$scope.onSubmitForm = function(){
		$scope.data.form = 'account_details';
		http.saveEmployerAccount($scope.data).then(function (data) {
			$scope.errors = {};
			$uibModalInstance.close();
		}, function (data) {
			if (data.errors) {
				$scope.errors = data.errors;
			}
		});
    };

    $scope.cancel = function() {
        $uibModalInstance.dismiss("cancel");
    };

}]);

jobholler.controller('Billing_addressCtrl', ['$scope', '$uibModalInstance', 'http', 'validator', 'global', 'data', 'country' , 'state' , function ($scope, $uibModalInstance, http, validator, global, data, country, state) {
	$scope.data = data;
	$scope.$state = state;
	$scope.$country = country;

	$scope.errors = {};
	$scope.$watch('errors', function(errors) {
        if (errors !== undefined) { 
        	validator.errors = errors;
        }
    });
	$scope.hasError = validator.hasError;
	$scope.showErrorBlock = validator.showErrorBlock;


	$scope.onSubmitForm = function(){
		$scope.data.form = 'billing_address';
		http.saveEmployerAccount($scope.data).then(function (data) {
			$scope.errors = {};
			$uibModalInstance.close();
		}, function (data) {
			if (data.errors) {
				$scope.errors = data.errors;
			}
		});
    };

    $scope.cancel = function() {
        $uibModalInstance.dismiss("cancel");
    };

}]);


jobholler.controller('BrandingCtrl', ['$scope', 'http', 'validator', 'global' , function ($scope, http, validator, global) {
	$scope.$brandings = [];
	global.brandings.listTrigger(function (brandings) {console.log(brandings);
		$scope.$brandings = angular.copy(brandings);
	});
	
	http.getAllBrandingsAjax({});

	$scope.onPurchase = function (branding) {
		http.purchaseBranding(branding);
	};

}]);
