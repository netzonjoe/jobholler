'use strict';

var jobholler = angular.module('jobhollerDirectives', []);

jobholler.directive('phoneInput', function($filter, $browser, $timeout) {
    return {
        require: 'ngModel',
        link: function($scope, $element, $attrs, ngModelCtrl) {
            var listener = function() {
                var value = $element.val().replace(/[^0-9]/g, '');
                $element.val($filter('tel')(value, false));
            };

            // This runs when we update the text field
            ngModelCtrl.$parsers.push(function(viewValue) {
                return viewValue.replace(/[^0-9]/g, '').slice(0,10);
            });

            // This runs when the model gets updated on the scope directly and keeps our view in sync
            ngModelCtrl.$render = function() {
                $element.val($filter('tel')(ngModelCtrl.$viewValue, false));
            };

            $element.bind('change', listener);
            $element.bind('keydown', function(event) {
                var key = event.keyCode;
                // If the keys include the CTRL, SHIFT, ALT, or META keys, or the arrow keys, do nothing.
                // This lets us support copy and paste too
                if (key == 91 || (15 < key && key < 19) || (37 <= key && key <= 40)){
                    return;
                }
                $browser.defer(listener); // Have to do this or changes don't get picked up properly
            });

            $element.bind('paste cut', function() {
                $browser.defer(listener);
            });

            $timeout($.proxy(function (e) {
              this.trigger( 'change' );
            },$element), 250);
        }

    };
});
jobholler.filter('tel', function () {
    return function (tel) {
        if (!tel) { return ''; }

        var value = tel.toString().trim().replace(/^\+/, '');

        if (value.match(/[^0-9]/)) {
            return tel;
        }

        var country, city, number;

        switch (value.length) {
            case 1:
            case 2:
            case 3:
                city = value;
                break;

            default:
                city = value.slice(0, 3);
                number = value.slice(3);
        }

        if(number){
            if(number.length>3){
                number = number.slice(0, 3) + '-' + number.slice(3,7);
            }
            else{
                number = number;
            }

            return ("(" + city + ") " + number).trim();
        }
        else{
            return "(" + city;
        }

    };
});

jobholler.directive('selectPicker', function($timeout){
    return {
      restrict: 'A',
      link:function(scope, elem){
        $timeout(function() {
          elem.selectpicker({showSubtext:true});
          elem.selectpicker('refresh');
        }, 0);
      }
    };
});

jobholler.directive('checkboxPicker', function(){
    return {
      restrict: 'A',
      link:function(scope, elem){
        elem.click(function () {
            jQuery('input[name*=\'selected\']').prop('checked', elem[0].checked);
        });
      }
    };
});

jobholler.directive('restrict', function($parse){
    return {
      restrict: 'A',
      require: 'ngModel',
        link: function(scope, iElement, iAttrs, controller) {
            scope.$watch(iAttrs.ngModel, function(value) {
                if (!value) {
                    return;
                }
                $parse(iAttrs.ngModel).assign(scope, value.toUpperCase().replace(new RegExp(iAttrs.restrict, 'g'), '').replace(/\s+/g, '-'));
            });
        }
    };
});

jobholler.directive('fileModel', ['$parse', function ($parse) {
    return {
       restrict: 'A',
       link: function(scope, element, attrs) {
          var model = $parse(attrs.fileModel);
          var modelSetter = model.assign;
          
          element.bind('change', function(){
             scope.$apply(function(){
                modelSetter(scope, element[0].files[0]);
             });
          });
       }
    };
 }]);

jobholler.directive('fileImageSrc', ['$parse', function ($parse) {
    return {
       restrict: 'A',
       scope: {
            fileImageSrc: '='
       },
       link: function(scope, element, attrs) {
          // var model = $parse(attrs.fileImageModel);
          // var modelSetter = model.assign;
          element.bind('change', function(){
             scope.$apply(function(){
                // modelSetter(scope, element[0].files[0]);
                if (element[0].files[0]) {
                    var FR = new FileReader();
                    FR.onload = function(e) {
                        scope.$apply(function () {
                            scope.fileImageSrc = e.target.result;
                        });
                    };       
                    FR.readAsDataURL( element[0].files[0] );
                }
             });

          });
       }
    };
 }]);

jobholler.directive('onReadyState', function () {
    return {
       restrict: 'A',
       link: function(scope, element, attrs) {
          element[0].style.display = "block";
       }
    };
 });


jobholler.directive('tableRow', function ($compile, $timeout) {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            rows: "=",
            key: "=",
            index: "=",
            length: "=",
            icons: "=",
            onRemoveRow: "&"
        },
        link: function (scope, element, attrs) {
            var tableRowHtml;
            if (scope.key === 'company_perks') {
                tableRowHtml  = 
                  '<tr ng-repeat="row in rows" > ' +
                  '    <td ng-if="$index == 0" rowspan="3" class="side-background">{{index + 1}}</td>' +
                  '    <td ng-if="$index == 0"> Perk Title </td> ' +
                  '    <td ng-if="$index == 0"> ' + 
                  '       <input type="text" class="form-control" ' +
                  '                placeholder="" required ' +
                  '                data-ng-minlength=1 ng-model="rows[0].value"> </td> ' +
                  /*--------------------------------------------------------------------*/
                  '    <td ng-if="$index == 1"> Perk Description </td> ' +
                  '    <td ng-if="$index == 1"> ' + 
                  '      <textarea class="form-control" ' +
                  '              rows="4" ' +
                  '              data-ng-minlength=1 ' +
                  '              required ' +
                  '              data-ng-model="rows[1].value"></textarea> </td>' +
                  /*--------------------------------------------------------------------*/
                  '    <td ng-if="$index == 2"> Icon </td> ' +
                  '    <td ng-if="$index == 2"> ' + 
                  '         <button icon-picker row="rows[2].value" ' +
                  '            class="icon-picker-input btn btn-default" ' +
                  '            data-icon="{{rows[2].value}}" ' + 
                  '            data-placement="right" style="margin-left: 30px;" > ' + 
                  '        </button>' +
                  '    </td> ' +
               
                  /*--------------------------------------------------------------------*/
                  '    <td ng-if="$index == 0" rowspan="3" class="side-background"> ' +
                  '        &nbsp;&nbsp; ' +
                  '        <div class="position-relative"> ' + /*ng-if="(length == index + 1)" */
                  '            <div class="form-group btn-icon-round-minus"> ' +
                  '                <a href="javascript:void(0);" ' +
                  '                    ng-click="onRemoveRowItem(index)" ' +
                  '                    class="btn-icon-lined btn-icon-round btn-icon-sm gray"> ' +
                  '                    <span class="ti-minus"></span> ' +
                  '                </a> ' +
                  '            </div> ' +
                  '        </div> ' +
                  '    </td> ' +
                  '</tr>';
            } else if (scope.key === 'social_networks') {
              tableRowHtml  = 
                  '<tr ng-repeat="row in rows"> ' +
                  '    <td ng-if="$index == 0" rowspan="2" class="side-background">{{index + 1}}</td>' +
                  '    <td ng-if="$index == 0"> Network Type </td> ' +
                  '    <td ng-if="$index == 0"> ' + 

                  '        <span class="ui-select" style="height: 34px; width: 98%;"> ' +
                  '            <select style="width: 100%;" ng-model="rows[0].value"> ' +
                  '                <option ng-repeat="network in network_type" ' +
                  '                        value="{{network.icon}}" ng-selected="network.icon == rows[0].value" >{{network.label}}</option> ' +
                  '            </select> ' +
                  '        </span> </td>' +
                  /*--------------------------------------------------------------------*/
                  '    <td ng-if="$index == 1"> URL </td> ' +
                  '    <td ng-if="$index == 1"> ' + 
                  '       <input type="text" class="form-control" ' +
                  '                placeholder="" required ' +
                  '                data-ng-minlength=1 ng-model="rows[1].value"> </td> ' +
                  /*--------------------------------------------------------------------*/
                  '    <td ng-if="$index == 0" rowspan="2" class="side-background"> ' +
                  '        &nbsp;&nbsp; ' +
                  '        <div class="position-relative"> ' +
                  '            <div class="form-group btn-icon-round-minus"> ' +
                  '                <a href="javascript:void(0);" ' +
                  '                    ng-click="onRemoveRowItem(index)" ' +
                  '                    class="btn-icon-lined btn-icon-round btn-icon-sm gray"> ' +
                  '                    <span class="ti-minus"></span> ' +
                  '                </a> ' +
                  '            </div> ' +
                  '        </div> ' +
                  '    </td> ' +
                  '</tr>';
                  scope.network_type = [
                     {
                       icon: 'fa fa-facebook',
                       label: 'Facebook'
                     },
                     {
                       icon: 'fa fa-twitter',
                       label: 'Twitter'
                     },
                     {
                       icon: 'fa fa-google-plus',
                       label: 'Google+'
                     },
                     {
                       icon: 'fa fa-linkedin',
                       label: 'LinkedIn'
                     },
                     {
                       icon: 'fa fa-youtube',
                       label: 'Youtube'
                     },
                     {
                        icon: 'fa fa-pinterest-p',
                        label: 'Pinterest'
                     }
                  ];
            }
            
            $('#' + scope.key).append($compile(tableRowHtml)(scope));

            scope.onRemoveRowItem = function (index) {
                if (scope.length > 1) {
                    $('#' + scope.key).html('');
                    scope.onRemoveRow({index: index});
                }
            };
        }
    };
 });

jobholler.directive('iconPicker', function ($timeout) {
    return {
       restrict: 'A',
       scpoe: {
          row: '='
       },
       link: function(scope, element, attrs) {
          var self = {
            scope : scope,
            element: element, 
            attrs: attrs
          };
          if (scope.row.value !== '') {
              $timeout($.proxy(function (e) {
                  this.element[0].getElementsByTagName('i')[0].setAttribute('class', this.scope.row.value);
              },self), 100);
          }

          scope.onChangeValueInput= function (icon) {
            scope.row.value = icon;
          };
          element.iconpicker().iconpicker('setIconset', $.iconset_fontawesome)
                .bind('change', $.proxy(function (e) {
              // e.target.getElementsByTagName('i')[0].getAttribute('class')
              // e.icon
              this.onChangeValueInput(e.target.getElementsByTagName('i')[0].getAttribute('class'));
           },scope));
       }
    };
 });

jobholler.directive('noCopyPaste', function ($timeout) {
    return {
       restrict: 'A',
       link: function(scope, element, attrs) {
          $timeout($.proxy(function () {
              this.find('.ta-scroll-window.ta-text.ta-editor .ta-bind').bind('paste cut copy',function(e) {
                  e.preventDefault();
                  return false;
              });
           },element), 10);
       }
    };
 });
