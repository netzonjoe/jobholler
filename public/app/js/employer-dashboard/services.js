'use strict';

var jobholler = angular.module('jobhollerServices', []);

jobholler.factory('http', ['$http', '$q', 'global', 'cfpLoadingBar', '$anchorScroll', function($http, $q, global, cfpLoadingBar, $anchorScroll) {
	return {
		alert: {
			type: 'success',
            msg: 'Product information was updated successfully'
		},
		_token: $_token,
		post: function (url, data) {
			var self = this;
			cfpLoadingBar.start();
			return $q(function(resolve, reject) {
				data._token = self._token; 
		        jQuery.ajax({
		        	cache: false,
		        	type: 'POST',
		        	dataType: 'json',
		        	url: $route_list['POST'][url],
				    data: data,
				    headers: {
				        'X-CSRF-TOKEN': self._token
				    },
				   	success: function(data) {
				   		if (data.type !== undefined && data.msg !== undefined) {
				   			self.alert.type = data.type;	
				   			self.alert.msg = data.msg;	
				   			global.alert.listUpdate(self.alert);
				   		}

				   		self._token = data._token;
				   		delete data['_token'];
				      	resolve({
				   			data:data,
							status:200,
							statusText:"OK"	
				   		});
				   		cfpLoadingBar.complete();
				   		$anchorScroll();
				   	},
				   	error: function(data) {
				   		var data = JSON.parse(data.responseText);

				   		if (data.errors.type !== undefined && data.errors.msg !== undefined) {
				   			self.alert.type = data.errors.type;	
				   			self.alert.msg = data.errors.msg;	
				   			global.alert.listUpdate(self.alert);
				   		}

				   		self._token = data._token;
				   		delete data['_token'];
				      	reject(data);
				      	cfpLoadingBar.complete();
				      	$anchorScroll();
				   	}
		        });
  			});
		},
		postAndUpload: function (url, data) {
			var self = this;
			cfpLoadingBar.start();
			return $q(function(resolve, reject) {
				data._token = self._token; 
		        jQuery.ajax({
		        	type: 'POST',
		        	dataType: 'json',
		        	url: $route_list['POST'][url],
				    data: data,
				    headers: {
				        'X-CSRF-TOKEN': self._token
				    },
				    async: false,
					cache: false,
					contentType: false,
					processData: false,
				   	success: function(data) {
				   		if (data.type !== undefined && data.msg !== undefined) {
				   			self.alert.type = data.type;	
				   			self.alert.msg = data.msg;	
				   			global.alert.listUpdate(self.alert);
				   		}

				   		self._token = data._token;
				   		delete data['_token'];
				      	resolve({
				   			data:data,
							status:200,
							statusText:"OK"	
				   		});
				   		cfpLoadingBar.complete();
				   		$anchorScroll();
				   	},
				   	error: function(data) {
				   		var data = JSON.parse(data.responseText);
				   		if (data.type !== undefined && data.msg !== undefined) {
				   			self.alert.type = data.type;	
				   			self.alert.msg = data.msg;	
				   			global.alert.listUpdate(self.alert);
				   		}

				   		self._token = data._token;
				   		delete data['_token'];
				      	reject(data);
				      	cfpLoadingBar.complete();
				      	$anchorScroll();
				   	}
		        });
  			});
		},
		createJob: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('EmployerDashboardController@createJob', data).then(function(response) {
					if (response.data.$jobs !== undefined) {
			   			global.jobs.listUpdate(response.data.$jobs);	
			   		}
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		deleteJob: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('EmployerDashboardController@deleteJob', data).then(function(response) {
					if (response.data.$jobs !== undefined) {
			   			global.jobs.listUpdate(response.data.$jobs);	
			   		}
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		updateJob: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('EmployerDashboardController@updateJob', data).then(function(response) {
					if (response.data.$jobs !== undefined) {
			   			global.jobs.listUpdate(response.data.$jobs);	
			   		}
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		cloneJob: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('EmployerDashboardController@cloneJob', data).then(function(response) {
					if (response.data.$jobs !== undefined) {
			   			global.jobs.listUpdate(response.data.$jobs);	
			   		}
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		getAllExpiredJobsAjax: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('EmployerDashboardController@getAllExpiredJobsAjax', data).then(function(response) {

					if (response.data.$expired_jobs !== undefined) {
			   			global.expired_jobs.listUpdate(response.data.$expired_jobs);	
			   		}

			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		getAllPostedJobsAjax: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('EmployerDashboardController@getAllPostedJobsAjax', data).then(function(response) {

					if (response.data.$jobs !== undefined) {
			   			global.jobs.listUpdate(response.data.$jobs);	
			   		}

			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		repostJob: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('EmployerDashboardController@repostJob', data).then(function(response) {

					if (response.data.$expired_jobs !== undefined) {
			   			global.expired_jobs.listUpdate(response.data.$expired_jobs);	
			   		}

			   		if (response.data.$jobs !== undefined) {
			   			global.jobs.listUpdate(response.data.$jobs);	
			   		}
			   		
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		saveCompanyProfile: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.postAndUpload('EmployerDashboardController@saveCompanyProfile', data).then(function(response) {

					if (response.data.$company_profile !== undefined) {
			   			global.company_profile.listUpdate(response.data.$company_profile);	
			   		}

			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		getCompanyProfileAjax: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('EmployerDashboardController@getCompanyProfileAjax', data).then(function(response) {

					if (response.data.$company_profile !== undefined) {
			   			global.company_profile.listUpdate(response.data.$company_profile);	
			   		}

			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		saveEmployerAccount: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('EmployerDashboardController@saveEmployerAccount', data).then(function(response) {

					if (response.data.$user_account_details !== undefined) {
			   			global.user_account_details.listUpdate(response.data.$user_account_details);	
			   		}

			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		getEmployerAccountAjax: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('EmployerDashboardController@getEmployerAccountAjax', data).then(function(response) {

					if (response.data.$user_account_details !== undefined) {
			   			global.user_account_details.listUpdate(response.data.$user_account_details);	
			   		}

			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		getAllBrandingsAjax: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('EmployerDashboardController@getAllBrandingsAjax', data).then(function(response) {

					if (response.data.$brandings !== undefined) {
			   			global.brandings.listUpdate(response.data.$brandings);	
			   		}

			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		purchaseBranding: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('EmployerDashboardController@purchaseBranding', data).then(function(response) {

					// if (response.data.$brandings !== undefined) {
			  //  			global.brandings.listUpdate(response.data.$brandings);	
			  //  		}

			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		}
	};
}]);

jobholler.factory('global', [function() {
	return {
		state: [
			['ABE' , 'Aberdeen City'], 
			['ABD' , 'Aberdeenshire'], 
			['ANS' , 'Angus'], 
			['ANT' , 'Antrim'], 
			['ARD' , 'Ards'], 
			['AGB' , 'Argyll and Bute'], 
			['ARM' , 'Armagh'], 
			['BLA' , 'Ballymena'], 
			['BLY' , 'Ballymoney'], 
			['BNB' , 'Banbridge'], 
			['BDG' , 'Barking and Dagenham'], 
			['BNE' , 'Barnet'], 
			['BNS' , 'Barnsley'], 
			['BAS' , 'Bath and North East Somerset'], 
			['BDF' , 'Bedford'], 
			['BFS' , 'Belfast'], 
			['BEX' , 'Bexley'], 
			['BIR' , 'Birmingham'], 
			['BBD' , 'Blackburn with Darwen'], 
			['BPL' , 'Blackpool'], 
			['BGW' , 'Blaenau Gwent'], 
			['BOL' , 'Bolton'], 
			['BMH' , 'Bournemouth'], 
			['BRC' , 'Bracknell Forest'], 
			['BRD' , 'Bradford'], 
			['BEN' , 'Brent'], 
			['BGE' , 'Bridgend [Pen-y-bont ar Ogwr GB-POG]'], 
			['BNH' , 'Brighton and Hove'], 
			['BST' , 'Bristol, City of'], 
			['BRY' , 'Bromley'], 
			['BKM' , 'Buckinghamshire'], 
			['BUR' , 'Bury'], 
			['CAY' , 'Caerphilly [Caerffili GB-CAF]'], 
			['CLD' , 'Calderdale'], 
			['CAM' , 'Cambridgeshire'], 
			['CMD' , 'Camden'], 
			['CRF' , 'Cardiff [Caerdydd GB-CRD]'], 
			['CMN' , 'Carmarthenshire [Sir Gaerfyrddin GB-GFY]'], 
			['CKF' , 'Carrickfergus'], 
			['CSR' , 'Castlereagh'], 
			['CBF' , 'Central Bedfordshire'], 
			['CGN' , 'Ceredigion [Sir Ceredigion]'], 
			['CHE' , 'Cheshire East'], 
			['CHW' , 'Cheshire West and Chester'], 
			['CLK' , 'Clackmannanshire'], 
			['CLR' , 'Coleraine'], 
			['CWY' , 'Conwy'], 
			['CKT' , 'Cookstown'], 
			['CON' , 'Cornwall'], 
			['COV' , 'Coventry'], 
			['CGV' , 'Craigavon'], 
			['CRY' , 'Croydon'], 
			['CMA' , 'Cumbria'], 
			['DAL' , 'Darlington'], 
			['DEN' , 'Denbighshire [Sir Ddinbych GB-DDB]'], 
			['DER' , 'Derby'], 
			['DBY' , 'Derbyshire'], 
			['DRY' , 'Derry'], 
			['DVV' , 'Devon'], 
			['DEV' , 'Devon'], 
			['DNC' , 'Doncaster'], 
			['DOR' , 'Dorset'], 
			['DOW' , 'Down'], 
			['DUD' , 'Dudley'], 
			['DGY' , 'Dumfries and Galloway'], 
			['DND' , 'Dundee City'], 
			['DGN' , 'Dungannon'], 
			['DUR' , 'Durham'], 
			['EAL' , 'Ealing'], 
			['EAY' , 'East Ayrshire'], 
			['EDU' , 'East Dunbartonshire'], 
			['ELN' , 'East Lothian'], 
			['ERW' , 'East Renfrewshire'], 
			['ERY' , 'East Riding of Yorkshire'], 
			['ESX' , 'East Sussex'], 
			['EDH' , 'Edinburgh, City of'], 
			['ELS' , 'Eilean Siar'], 
			['ENF' , 'Enfield'], 
			['ESS' , 'Essex'], 
			['FAL' , 'Falkirk'], 
			['FER' , 'Fermanagh'], 
			['FIF' , 'Fife'], 
			['FLN' , 'Flintshire [Sir y Fflint GB-FFL]'], 
			['GAT' , 'Gateshead'], 
			['GLG' , 'Glasgow City'], 
			['GLS' , 'Gloucestershire'], 
			['GRE' , 'Greenwich'], 
			['GWN' , 'Gwynedd'], 
			['HCK' , 'Hackney'], 
			['HAL' , 'Halton'], 
			['HMF' , 'Hammersmith and Fulham'], 
			['HAM' , 'Hampshire'], 
			['HRY' , 'Haringey'], 
			['HRW' , 'Harrow'], 
			['HPL' , 'Hartlepool'], 
			['HAV' , 'Havering'], 
			['HEF' , 'Herefordshire, County of'], 
			['HRT' , 'Hertfordshire'], 
			['HLD' , 'Highland'], 
			['HIL' , 'Hillingdon'], 
			['HNS' , 'Hounslow'], 
			['IVC' , 'Inverclyde'], 
			['AGY' , 'Isle of Anglesey [Sir Ynys Môn GB-YNM]'], 
			['IOW' , 'Isle of Wight'], 
			['ISL' , 'Islington'], 
			['KEC' , 'Kensington and Chelsea'], 
			['KEN' , 'Kent'], 
			['KHL' , 'Kingston upon Hull, City of'], 
			['KTT' , 'Kingston upon Thames'], 
			['KIR' , 'Kirklees'], 
			['KWL' , 'Knowsley'], 
			['LBH' , 'Lambeth'], 
			['LAN' , 'Lancashire'], 
			['LRN' , 'Larne'], 
			['LDS' , 'Leeds'], 
			['LCE' , 'Leicester'], 
			['LEC' , 'Leicestershire'], 
			['LEW' , 'Lewisham'], 
			['LMV' , 'Limavady'], 
			['LIN' , 'Lincolnshire'], 
			['LSB' , 'Lisburn'], 
			['LIV' , 'Liverpool'], 
			['LND' , 'London, City of'], 
			['LUT' , 'Luton'], 
			['MFT' , 'Magherafelt'], 
			['MAN' , 'Manchester'], 
			['MDW' , 'Medway'], 
			['MTY' , 'Merthyr Tydfil [Merthyr Tudful GB-MTU]'], 
			['MRT' , 'Merton'], 
			['MDB' , 'Middlesbrough'], 
			['MLN' , 'Midlothian'], 
			['MIK' , 'Milton Keynes'], 
			['MON' , 'Monmouthshire [Sir Fynwy GB-FYN]'], 
			['MRY' , 'Moray'], 
			['MYL' , 'Moyle'], 
			['NTL' , 'Neath Port Talbot [Castell-nedd Port Talbot GB-CTL]'], 
			['NET' , 'Newcastle upon Tyne'], 
			['NWM' , 'Newham'], 
			['NWP' , 'Newport [Casnewydd GB-CNW]'], 
			['NYM' , 'Newry and Mourne'], 
			['NTA' , 'Newtownabbey'], 
			['NFK' , 'Norfolk'], 
			['NAY' , 'North Ayrshire'], 
			['NDN' , 'North Down'], 
			['NEL' , 'North East Lincolnshire'], 
			['NLK' , 'North Lanarkshire'], 
			['NLN' , 'North Lincolnshire'], 
			['NSM' , 'North Somerset'], 
			['NTY' , 'North Tyneside'], 
			['NYK' , 'North Yorkshire'], 
			['NTH' , 'Northamptonshire'], 
			['NBL' , 'Northumberland'], 
			['NGM' , 'Nottingham'], 
			['NTT' , 'Nottinghamshire'], 
			['OLD' , 'Oldham'], 
			['OMH' , 'Omagh'], 
			['ORK' , 'Orkney Islands'], 
			['OXF' , 'Oxfordshire'], 
			['PEM' , 'Pembrokeshire [Sir Benfro GB-BNF]'], 
			['PKN' , 'Perth and Kinross'], 
			['PTE' , 'Peterborough'], 
			['PLY' , 'Plymouth'], 
			['POL' , 'Poole'], 
			['POR' , 'Portsmouth'], 
			['POW' , 'Powys'], 
			['RDG' , 'Reading'], 
			['RDB' , 'Redbridge'], 
			['RCC' , 'Redcar and Cleveland'], 
			['RFW' , 'Renfrewshire'], 
			['RCT' , 'Rhondda, Cynon, Taff [Rhondda, Cynon,Taf]'], 
			['RIC' , 'Richmond upon Thames'], 
			['RCH' , 'Rochdale'], 
			['ROT' , 'Rotherham'], 
			['RUT' , 'Rutland'], 
			['SLF' , 'Salford'], 
			['SAW' , 'Sandwell'], 
			['SCB' , 'Scottish Borders, The'], 
			['SFT' , 'Sefton'], 
			['SHF' , 'Sheffield'], 
			['ZET' , 'Shetland Islands'], 
			['SHR' , 'Shropshire'], 
			['SLG' , 'Slough'], 
			['SOL' , 'Solihull'], 
			['SOM' , 'Somerset'], 
			['SAY' , 'South Ayrshire'], 
			['SGC' , 'South Gloucestershire'], 
			['SLK' , 'South Lanarkshire'], 
			['STY' , 'South Tyneside'], 
			['STH' , 'Southampton'], 
			['SOS' , 'Southend-on-Sea'], 
			['SWK' , 'Southwark'], 
			['SHN' , 'St. Helens'], 
			['STS' , 'Staffordshire'], 
			['STG' , 'Stirling'], 
			['SKP' , 'Stockport'], 
			['STT' , 'Stockton-on-Tees'], 
			['STE' , 'Stoke-on-Trent'], 
			['STB' , 'Strabane'], 
			['SFK' , 'Suffolk'], 
			['SND' , 'Sunderland'], 
			['SRY' , 'Surrey'], 
			['STN' , 'Sutton'], 
			['SWA' , 'Swansea [Abertawe GB-ATA]'], 
			['SWD' , 'Swindon'], 
			['TAM' , 'Tameside'], 
			['TFW' , 'Telford and Wrekin'], 
			['THR' , 'Thurrock'], 
			['TOB' , 'Torbay'], 
			['TOF' , 'Torfaen [Tor-faen]'], 
			['TWH' , 'Tower Hamlets'], 
			['TRF' , 'Trafford'], 
			['VGL' , 'Vale of Glamorgan, The [Bro Morgannwg GB-BMG]'], 
			['WKF' , 'Wakefield'], 
			['WLL' , 'Walsall'], 
			['WFT' , 'Waltham Forest'], 
			['WND' , 'Wandsworth'], 
			['WRT' , 'Warrington'], 
			['WAR' , 'Warwickshire'], 
			['WBK' , 'West Berkshire'], 
			['WDU' , 'West Dunbartonshire'], 
			['WLN' , 'West Lothian'], 
			['WSX' , 'West Sussex'], 
			['WSM' , 'Westminster'], 
			['WGN' , 'Wigan'], 
			['WIL' , 'Wiltshire'], 
			['WNM' , 'Windsor and Maidenhead'], 
			['WRL' , 'Wirral'], 
			['WOK' , 'Wokingham'], 
			['WLV' , 'Wolverhampton'], 
			['WOR' , 'Worcestershire'], 
			['WRX' , 'Wrexham [Wrecsam GB-WRC]'], 
			['YOR' , 'York'], 
			['-1' , 'Other…']
		],
		country: [
			['GB' , 'United Kingdom']
		],
		alert: {
			callback: null,
			listTrigger: function (callback) {
				if (callback !== undefined) {
					this.callback = callback;
				}
			},
			listUpdate: function ($data) {
				if (this.callback !== null) {
					this.callback($data);
				}
			}
		},
		user_account_details: {
			callback: null,
			listTrigger: function (callback) {
				if (callback !== undefined) {
					this.callback = callback;
				}
			},
			listUpdate: function ($data) {
				if (this.callback !== null) {
					this.callback($data);
				}
			}
		},
		company_profile: {
			callback: null,
			listTrigger: function (callback) {
				if (callback !== undefined) {
					this.callback = callback;
				}
			},
			listUpdate: function ($data) {
				if (this.callback !== null) {
					this.callback($data);
				}
			}
		},
		jobs: {
			callback: null,
			listTrigger: function (callback) {
				if (callback !== undefined) {
					this.callback = callback;
				}
			},
			listUpdate: function ($data) {
				if (this.callback !== null) {
					this.callback($data);
				}
			}
		},
		expired_jobs: {
			callback: null,
			listTrigger: function (callback) {
				if (callback !== undefined) {
					this.callback = callback;
				}
			},
			listUpdate: function ($data) {
				if (this.callback !== null) {
					this.callback($data);
				}
			}
		},
		brandings: {
			callback: null,
			listTrigger: function (callback) {
				if (callback !== undefined) {
					this.callback = callback;
				}
			},
			listUpdate: function ($data) {
				if (this.callback !== null) {
					this.callback($data);
				}
			}
		}
	};
}]);

jobholler.factory('validator', [function() {
	return {
		errors: {},
		hasError: function (key, classError) {
			return (this.errors[key] === undefined) ? '' : classError;
		},
		showErrorBlock: function (key, classError) {
			return (this.errors[key] === undefined) ? classError : '';
		}
	};
}]);

jobholler.factory('generator', [function() {
	return {
		reference: function () {
			var first3Char = "";
		    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

		    for( var i=0; i < 3; i++ )
		        first3Char += possible.charAt(Math.floor(Math.random() * possible.length));

		    return first3Char + Math.round(new Date().getTime()).toString().substring(6);
		}
	};
}]);


