(function () {
    'use strict';

    String.prototype.toController = function() {
        return this.charAt(0).toUpperCase() + this.slice(1) + 'Ctrl';
    };
    angular.module('app')
        .config(['$routeProvider', function($routeProvider) {
            var routes, setRoutes;

            routes = [
                'dashboard',
                'company_profile',
                'jobs',
                'expired_jobs',
                'tips_box',
                'applicants',
                'branding',
                'my_account'
            ]

            setRoutes = function(route) {
                var config, url;
                url = '/' + route;
                config = {
                    templateUrl: 'templates/employer-dashboard/' + route + '.html',
                    controller: route.toController()
                };
                $routeProvider.when(url, config);
                return $routeProvider;
            };

            routes.forEach(function(route) {
                return setRoutes(route);
            });

            $routeProvider
                .when('/', {redirectTo: '/dashboard'})
                .when('/404', {templateUrl: 'templates/employer-dashboard/page/404.html'})
                .otherwise({ redirectTo: '/404'});

        }]
    );

})(); 