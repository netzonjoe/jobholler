'use strict';
var jobholler = angular.module('jobhollerControllers', []);

jobholler.controller('JobhollerCtrl', ['$scope', 'global', '$timeout',  function ($scope, global, $timeout) {
	$scope.$alerts = [];
	global.alert.listTrigger(function (alert) {console.log(alert);
        $scope.$alerts.push(alert);
        $timeout(function () {
        	$scope.$alerts.splice($scope.$alerts.length - 1, 1);
        }, 8000);
	});
}]);

jobholler.controller('HeaderCtrl', ['$scope', 'http', 'validator', 'global', function ($scope, http, validator, global) {

	var resetFormData = function () {
		$scope.admin = {
		    firstname: '',
		    lastname: '',
		    email: '',
		    password: ''
		};
		$('.admin-option-settings.form-validation').find('.ng-dirty').removeClass('ng-dirty');
	};

	resetFormData();

	http.getAdminProfileDetails({}).then(function (data) {
		$scope.admin = angular.copy(data.data.$admin);
	});

	$scope.onUpdateProfile = function () {
		var password = angular.copy($scope.admin.password);
		http.updateAdminProfile($scope.admin).then(function (data) {
			$scope.admin = data.data.$admin;
			$scope.admin.password = password;
			$scope.errors = {};
			$scope.status.isopenSettings = false;
		}, function (data) {
			if (data.errors) {
				$scope.errors = data.errors;
			}
		});
	};

	$scope.onClearForm = function () {
		resetFormData();
	};

	global.notifications.listTrigger(function (data) {
		$scope.is_clicked = 0;
		$scope.is_viewed = 0;
		var not_yet_viewed = [];
		for(var i in data) {
    		if (data[i].is_clicked === 0) {
    			$scope.is_clicked += 1;
    		}
    		if (data[i].is_viewed === 0) {
    			$scope.is_viewed += 1;
    			not_yet_viewed.push(data[i]);
    		}
    	}
      	$scope.notification = angular.copy(not_yet_viewed);
	});

	http.getAllNotificationsAjax({});
	$scope.notification = [];
	$scope.is_clicked = 0;
	$scope.is_viewed = 0;
	var pusher = new Pusher($pusher_key, {
      encrypted: true
    });
    var channel = pusher.subscribe('purchase_branding_channel');
    channel.bind('purchase_branding_event', function(data) {
    	console.log(data);
    	$scope.$apply(function () {
    		$scope.is_clicked = 0;
			$scope.is_viewed = 0;
			var not_yet_viewed = [];
    		for(var i in data) {
	    		if (data[i].is_clicked === 0) {
	    			$scope.is_clicked += 1;
	    		}
	    		if (data[i].is_viewed === 0) {
	    			$scope.is_viewed += 1;
	    			not_yet_viewed.push(data[i]);
	    		}
	    	}
	      	$scope.notification = angular.copy(not_yet_viewed);
    	})
    });
	
	$scope.onClickedNotifications = function (data) {
		if ($scope.notification.length > 0) {
			http.updateAllNotificationOnClicked({user_id: $scope.notification[0].user_id});
		}
	};

	$scope.onViewedNotifications = function (data) {
		if ($scope.notification.length > 0) {
			http.updateAllNotificationOnViewed({id: data.id});
		}
	};

	$scope.onViewAllNotification = function (ifhide) {
		if (ifhide) {
			http.hideAllNotificationOnViewed({});
		} else {
			http.showAllNotificationOnViewed({});	
		}
	};


	$scope.baseurl = $baseurl;
	$scope.has_company_profile = false;
	global.company_profile.listTrigger(function (company_profile) {
		if (company_profile !== null) {
			$scope.has_company_profile = true;
	   		$scope.company_name = company_profile.company_name;
		}
	});
	
	http.getCompanyProfileAjax({});


}]);

jobholler.controller('MembersCtrl', ['$scope', 'http', 'validator', 'global', function ($scope, http, validator, global) {

	var resetFormData = function () {
		$scope.data = {
		    user_group_id: '',
		    firstname: '',
		    lastname: '',
		    email: '',
		    phone: '',
		    password: ''
		};
		$('.members.form-validation').find('.ng-dirty').removeClass('ng-dirty');
	};

	resetFormData();

	$scope.errors = {};
	
	$scope.$user_groups = $user_groups;

	http.getAllUsersWithUserGroupAjax({});
	http.getUserGroups({}).then(function (data) {
		$scope.$user_groups = angular.copy(data.data.$user_groups);
	});

	global.users.listTrigger(function (users) {
        $scope.$users = angular.copy(users);
    	$scope._users = angular.copy(users);
    	window.$users = angular.copy(users);
	});

	$scope.$users = angular.copy(window.$users);
    $scope._users = angular.copy(window.$users);

	$scope.$watch('errors', function(errors) {
        if (errors !== undefined) { 
        	validator.errors = errors;
        }
    });

	$scope.hasError = validator.hasError;
	
	$scope.showErrorBlock = validator.showErrorBlock;

	$scope.onGeneratePassword = function () {
		document.getElementById("random-password-field").classList.add('ng-dirty');
		$scope.data.password = Math.random().toString(36).slice(-8);
	};

	$scope.updateMember = false;
	$scope.onCreateMember = function () {
		if ($scope.updateMember) {
			http.updateMember($scope.data).then(function (data) {
				$scope.errors = {};
				resetFormData();
				$scope.updateMember = false;
			}, function (data) {
				if (data.errors) {
					$scope.errors = data.errors;
				}
			});
		} else {
			http.createMember($scope.data).then(function (data) {
				$scope.errors = {};
				resetFormData();
			}, function (data) {
				if (data.errors) {
					$scope.errors = data.errors;
				}
			});
		}
	};

	$scope.onUpdateMember = function (user) {
		$scope.updateMember = true;
		$scope.data = user;
	};

	$scope.onResetMember = function () {
		$scope.errors = {};
		resetFormData();
		if ($scope.updateMember) {
			$scope.updateMember = false;	
		}
	};

	$scope.onDeleteMember = function (user) {
		var confirm = window.confirm('Are you sure?');
		if(confirm) {
			http.deleteMember(user);
		}
		return false;
	};

	$scope.tabs = [
		{
			'class': 'active',
			'id' : 'all',
			'name' : 'All'	
		},
		{
			'class': '',
			'id' : 'employers',
			'name' : 'Employers'	
		},
		{
			'class': '',
			'id' : 'candidates',
			'name' : 'Candidates'	
		}
	];
	$scope.tab = 0;
	$scope.keyUsers = ['all', 'employers', 'candidates'];
	$scope.onTabSelected = function (id) {
		$scope.tabs[$scope.tab].class = '';
		$scope.tabs[id].class = 'active';
		$scope.tab = id;
		$scope.searchKeywords = '';
		$scope.onSearchFilterUser();
	};
	
	$scope.onSearchFilterUser = function () {
		if ($scope.searchKeywords === '') {
			$scope.$users = angular.copy($scope._users);
			return;
		}
        var users = angular.copy($scope._users[$scope.keyUsers[$scope.tab]]);
		var newList = [];
		for (var i in users) {
			var fullname = users[i].firstname + ' ' + users[i].lastname;
 			var indexOfValue = fullname.toLowerCase().indexOf($scope.searchKeywords.toLowerCase());
 			if (indexOfValue !== -1) {
 				newList.push(users[i]);
 			}
		}
		$scope.$users[$scope.keyUsers[$scope.tab]] = angular.copy(newList);
	};


	$scope.numPerPageOpt = [3, 5, 10, 20];

	var numPerPage = localStorage.getItem('numPerPage');
	$scope.numPerPage = (numPerPage === null) ? $scope.numPerPageOpt[2] : parseInt(numPerPage);

	$scope.currentPage = 1;

	$scope.select = function(page) {
        var end, start;
        start = (page - 1) * $scope.numPerPage;
        end = start + $scope.numPerPage;
        return $scope.$users[$scope.keyUsers[$scope.tab]] =  angular.copy($scope._users[$scope.keyUsers[$scope.tab]].slice(start, end));
    };

	$scope.onNumPerPageChange = function() {
		localStorage.setItem('numPerPage', $scope.numPerPage);
        $scope.select(1);
        return $scope.currentPage = 1;
    };

    $scope.onDisabledMember = function (user) {
    	http.updateMemberStatus(user);
    };

}]);

jobholler.controller('DashboardCtrl', ['$scope', '$http', function ($scope, $http) {
	
}]);

jobholler.controller('JobsCtrl', ['$scope', 'http', 'validator', 'global', 'generator',	function ($scope, http, validator, global, generator) {
	$scope.$job_listing  = $job_listing;
	$scope.$jobs = angular.copy($jobs);
    $scope._jobs = angular.copy($jobs);
	global.jobs.listTrigger(function ($jobs) {
        $scope.$jobs = angular.copy($jobs);
        $scope._jobs = angular.copy($jobs);
        window.$jobs = angular.copy($jobs);
	});
	$scope.$registered_companies = [];
	global.registered_companies.listTrigger(function ($registered_companies) {
        $scope.$registered_companies = angular.copy($registered_companies);
	});
	http.getAllCompaniesAjax({});

	$scope.$baseurl = $baseurl;
	http.getAllPostedJobsAjax({});

    var options = { 
       componentRestrictions: {country: "uk"}
    }; 

	var inputAutocomplete = document.getElementById('job_location_input');
	var autocomplete = new google.maps.places.Autocomplete(inputAutocomplete, options);

	autocomplete.addListener('place_changed', function(event) {
		$scope.data.job_location = inputAutocomplete.value; 
  	});

	$scope.onClearForm = function () {
		if ($scope.updateJob) {
			$scope.updateJob = false;
			$scope.onClearForm();
			return;
		}

		$scope.data = {
			reference: generator.reference(),
		    job_title: '',
		    job_type: '',
		    job_location: '',
		    minimum_salary: '',
		    maximum_salary: '',
		    paid_per: 'year',
		    job_description: '',
		    experience_level: '',
		    start_date: '',
		    company_name: '',
		    contact_number: '',
		    skills: [
		    	{
		    		'name': '',
		    		'level': '1'
		    	}
		    ]
		};
		$('.jobs.form-validation').find('.ng-dirty').removeClass('ng-dirty');
	};

	$scope.onClearForm();

	$scope.errors = {};

	$scope.$watch('errors', function(errors) {
        if (errors !== undefined) { 
        	validator.errors = errors;
        }
    });

	$scope.hasError = validator.hasError;
	
	$scope.showErrorBlock = validator.showErrorBlock;

	$scope.onSubmitForm = function () {
		console.log($scope.data);
		$scope.onCreateJob();
	};

	$scope.today = function() {
        $scope.data.start_date = new Date();
    };
    $scope.today();

    $scope.clear = function() {
        $scope.data.start_date = null;
    };

    $scope.toggleMin = function() {
        $scope.minDate = $scope.minDate ? null : new Date();
    };

    $scope.toggleMin();
    $scope.maxDate = new Date(2020, 5, 22);

    $scope.open1 = function() {
        $scope.popup1.opened = true;
    };

    $scope.open2 = function() {
        $scope.popup2.opened = true;
    };

    $scope.setDate = function(year, month, day) {
        $scope.$apply(function () {
            $scope.data.start_date = new Date(year, month, day);
        });
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
    $scope.altInputFormats = ['M!/d!/yyyy'];

    $scope.popup1 = {
        opened: false
    };

    $scope.popup2 = {
        opened: false
    };

    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    var afterTomorrow = new Date();
    afterTomorrow.setDate(tomorrow.getDate() + 1);

    $scope.experience_level = [
    	{
    		key: 'junior',
    		value: 'Junior(1 Year)'	
    	},
    	{
    		key: 'mid',
    		value: 'Mid(2 Years)'	
    	},
    	{
    		key: 'senior',
    		value: 'Senior(3 Years)'	
    	},
    	{
    		key: 'manager',
    		value: 'Manager(4 Year)'	
    	}
    ];


    $scope.onAddNewSkill = function () {
    	var obj = $scope.data.skills[$scope.data.skills.length - 1];
    	if (obj.name !== '' && obj.level.toString() !== '') {
    		if (obj.name.trim() !== '' && obj.level.toString().trim() !== '') {
				$scope.data.skills.push({
		    		name: '',
		    		level: '1'
		    	});    			
    		}
    	}
    };	

    $scope.onRemoveSkill = function (index) {
		if ($scope.data.skills.length > 1) 
			var tempList = angular.copy($scope.data.skills);
			var len = tempList.length;
    		$scope.data.skills = tempList.slice(0, index).concat( tempList.slice(index + 1) );	
	};


    $scope.numPerPageOpt = [3, 5, 10, 20];

	$scope.numPerPage = $scope.numPerPageOpt[2];

	$scope.currentPage = 1;

	$scope.select = function(page) {
        var end, start;
        start = (page - 1) * $scope.numPerPage;
        end = start + $scope.numPerPage;
        return $scope.$jobs =  angular.copy($scope._jobs.slice(start, end));
    };

	$scope.onNumPerPageChange = function() {
        $scope.select(1);
        return $scope.currentPage = 1;
    };

    $scope.select(1);

    $scope.updateJob = false;

	$scope.onCreateJob = function () {
		if ($scope.updateJob) {

			var data = angular.copy($scope.data);
			if (data.job_title !== '') {
				data.job_title_slug = $scope.data.job_title.toLowerCase().replace(new RegExp('[^a-zA-Z0-9\\-\\s]', 'g'), '').replace(/\s+/g, '-');	
			}
			data.minimum_salary = (data.minimum_salary === undefined) ? '' : parseFloat(data.minimum_salary);
			data.maximum_salary = (data.maximum_salary === undefined) ? '' : parseFloat(data.maximum_salary);
			if (data.start_date !== '') {
				var start_date = data.start_date;
				data.start_date = start_date.getFullYear() + '-' + ("0" + (start_date.getMonth() + 1)).slice(-2) + '-' + ("0" + start_date.getDate()).slice(-2);
			} else {
				data.start_date = '';
			}

			http.updateJob(data).then(function (data) {
				$scope.errors = {};
				$scope.onClearForm();
				$scope.updateJob = false;
			}, function (data) {
				if (data.errors) {
					$scope.errors = data.errors;
				}
			});
		} else {
			var data = angular.copy($scope.data);
			if (data.job_title !== '') {
				data.job_title_slug = $scope.data.job_title.toLowerCase().replace(new RegExp('[^a-zA-Z0-9\\-\\s]', 'g'), '').replace(/\s+/g, '-');	
			}
			data.minimum_salary = (data.minimum_salary === undefined) ? '' : parseFloat(data.minimum_salary);
			data.maximum_salary = (data.maximum_salary === undefined) ? '' : parseFloat(data.maximum_salary);
			if (data.start_date !== '') {
				var start_date = data.start_date;
				data.start_date = start_date.getFullYear() + '-' + ("0" + (start_date.getMonth() + 1)).slice(-2) + '-' + ("0" + start_date.getDate()).slice(-2);
			} else {
				data.start_date = '';
			}
			
			http.createJob(data).then(function (data) {
				$scope.errors = {};
				$scope.onClearForm();
			}, function (data) {
				if (data.errors) {
					$scope.errors = data.errors;
				}
			});
		}
	};

	$scope.onCloneJob = function (row_job) {
		var job = angular.copy(row_job);
		job.minimum_salary = parseFloat(job.minimum_salary);
		job.maximum_salary = parseFloat(job.maximum_salary);
		var dateObj = job.start_date.split("-");
		var year = dateObj[0];
		var month = dateObj[1];
		var day = dateObj[2].split(" ")[0];
        job.start_date = new Date(year, parseInt(month) - 1, day);
        var start_date = job.start_date;
		job.start_date = start_date.getFullYear() + '-' + ("0" + (start_date.getMonth() + 1)).slice(-2) + '-' + ("0" + start_date.getDate()).slice(-2);
        job.reference = generator.reference();
        http.cloneJob(job);
	};

	$scope.onUpdateJob = function (job) {
		$scope.updateJob = true;
		job.minimum_salary = parseFloat(job.minimum_salary);
		job.maximum_salary = parseFloat(job.maximum_salary);
		var dateObj = job.start_date.split("-");
		var year = dateObj[0];
		var month = dateObj[1];
		var day = dateObj[2].split(" ")[0];
		$scope.data = angular.copy(job);
		$scope.data.company_name = job.company_profile_id;
        $scope.data.start_date = new Date(year, parseInt(month) - 1, day);
	};

	$scope.onDeleteJob = function (job) {
		var confirm = window.confirm('Are you sure?');
		if(confirm) {
			http.deleteJob(job);
		}
		return false;
	};

	$scope.searchKeywords = '';
	$scope.onSearchFilterJobs = function () {
		if ($scope.searchKeywords === '') {
			$scope.$jobs = angular.copy($scope._jobs);
			return;
		}
        var jobs = angular.copy($scope._jobs);
		var newList = [];
		for (var i in jobs) {
 			var indexOfValue = jobs[i].job_title.toLowerCase().indexOf($scope.searchKeywords.toLowerCase());
 			if (indexOfValue !== -1) {
 				newList.push(jobs[i]);
 			}
		}
		$scope.$jobs = angular.copy(newList);
	};

}]);


jobholler.controller('ResumeCtrl', ['$scope', '$uibModal', 'global', 'http', function ($scope, $uibModal, global, http) {

	$scope.$resume_listing = $resume_listing;
	$scope.$baseurl = $baseurl;
	$scope.$users_with_resume = angular.copy($users_with_resume);
    $scope._users_with_resume = angular.copy($users_with_resume);
    global.users_with_resume.listTrigger(function ($resumes) {
        $scope.$users_with_resume = angular.copy($resumes);
        $scope.$_users_with_resume = angular.copy($resumes);
        window.$users_with_resume = angular.copy($resumes);
	});

	http.getUsersWithResumeAjax({}).then(function (data) {
		$scope.$users_with_resume = angular.copy(data.data.$users_with_resume);
        $scope.$_users_with_resume = angular.copy(data.data.$users_with_resume);
        window.$users_with_resume = angular.copy(data.data.$users_with_resume);
	});

    $scope.status = {
		'0' : {
			class: 'label-warning',
			value: 'Hired'
		},
		'1' : {
			class: 'label-info',
			value: 'Open'
		}
    };

    $scope.numPerPageOpt = [3, 5, 10, 20];

	$scope.numPerPage = $scope.numPerPageOpt[2];

	$scope.currentPage = 1;

	$scope.select = function(page) {
        var end, start;
        start = (page - 1) * $scope.numPerPage;
        end = start + $scope.numPerPage;
        return $scope.$users_with_resume =  angular.copy($scope._users_with_resume.slice(start, end));
    };

	$scope.onNumPerPageChange = function() {
        $scope.select(1);
        return $scope.currentPage = 1;
    };

    $scope.select(1);

    $scope.onCreateOnlineResumeForm = function (size) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'myModalContent.html',
            controller: 'CreateOnlineResumeFormCtrl',
            size: size,
            resolve: {
                updateResume: function () {
                    return $scope.updateResume;
                },
                users_with_resume: function () {
                    return $scope.users_with_resume;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
            $scope.updateResume = false;
        }, function () {
        	$scope.updateResume = false;
            console.log('Modal dismissed at: ' + new Date());
        });
    };

    $scope.onStatusChange = function (users_with_resume) {
    	http.updateUserWithResumeStatus(users_with_resume);
    };

    $scope.onViewableResume = function (users_with_resume) {
    	http.updateUserWithResumeViewable(users_with_resume);
    };

    $scope.updateResume = false;
    $scope.users_with_resume = {};
    $scope.onUpdateResume = function (users_with_resume) {
    	$scope.updateResume = true;
    	$scope.users_with_resume = angular.copy(users_with_resume);
    	$scope.onCreateOnlineResumeForm('lg');
    };

    $scope.onDeleteResume = function (users_with_resume) {
    	http.deleteUserWithResume(users_with_resume);
    };


    $scope.searchKeywords = '';
	$scope.onSearchFilterResumes = function () {
		if ($scope.searchKeywords === '') {
			$scope.$users_with_resume = angular.copy($scope._users_with_resume);
			return;
		}
        var users_with_resume = angular.copy($scope._users_with_resume);
		var newList = [];
		for (var i in users_with_resume) {
 			var indexOfValue = users_with_resume[i].full_name.toLowerCase().indexOf($scope.searchKeywords.toLowerCase());
 			if (indexOfValue !== -1) {
 				newList.push(users_with_resume[i]);
 			}
		}
		$scope.$users_with_resume = angular.copy(newList);
	};

}]);


jobholler.controller('CreateOnlineResumeFormCtrl', ['$scope', '$uibModalInstance', 'http', 'validator', 'global', 'updateResume' , 'users_with_resume', function ($scope, $uibModalInstance, http, validator, global, updateResume, users_with_resume) {
	$scope.$imagesDir = $imagesDir;
	$scope.updateResume = updateResume;
	if (!updateResume) {
		$scope.$users_without_resume = $users_without_resume;

		http.getUsersWithoutResumeAjax({}).then(function (data) {
			$scope.$users_without_resume = data.data.$users_without_resume;	
		});

		global.users_without_resume.listTrigger(function ($resumes) {
	        $scope.$users_without_resume = angular.copy($resumes);
	        window.$users_without_resume = angular.copy($resumes);
		});	
	}

	$scope.onClearForm = function () {
		if (updateResume) {
			$scope.$users_without_resume = [{
				id : users_with_resume.user_id,
				firstname: users_with_resume.firstname,
				lastname: users_with_resume.lastname
			}];
            $scope.data = {
            	id: users_with_resume['id'],
				candidate_user: parseInt(users_with_resume['user_id']),
				full_name: users_with_resume['full_name'],
				tagline: users_with_resume['tagline'],
				about_me: users_with_resume['about_me'],
				profile_photo: users_with_resume['profile_photo'],
				years_of_experience: parseInt(users_with_resume['years_of_experience']),
				expected_salary: parseInt(users_with_resume['expected_salary']),
				personal_core_skills: users_with_resume['personal_core_skills'],
				technical_core_skills: users_with_resume['technical_core_skills'],
				hobbies: users_with_resume['hobbies'],
				educations: users_with_resume['educations'],
				experiences: users_with_resume['experiences'],
				resume_document: users_with_resume['resume_document'],
				cover_photo: users_with_resume['cover_photo'],
				cover_photo_selection: users_with_resume['cover_photo_selection']
			};
			$scope.profile_photo_src = $baseurl + users_with_resume['profile_photo'];
			$scope.cover_photo_src = (users_with_resume['cover_photo'] == '') 
										? undefined : $baseurl + users_with_resume['cover_photo'];
		} else {
			$scope.data = {
				candidate_user: '',
				full_name: '',
				tagline: '',
				about_me: '',
				profile_photo: '',
				years_of_experience: '',
				expected_salary: '',
				personal_core_skills: [
					{
						name: '',
						description: '',
						level: '1'
					}
				],
				technical_core_skills: [
					{
						name: '',
						description: '',
						level: '1'
					}
				],
				hobbies: [
					{
						name: ''
					}
				],
				educations: [
					{
						school_name: '',
						qualifications: '',
						year_attended_from: '',
						year_attended_to: ''
					}
				],
				experiences: [
					{
						company: '',
						job_description: '',
						start_date: '',
						end_date: ''
					}
				],
				resume_document: '',
				cover_photo: '',
				cover_photo_selection: '1'

			};
		}
		$('.resume.form-validation').find('.ng-dirty').removeClass('ng-dirty');
	};

	$scope.onClearForm();
	
	$scope.coverPhotoSelection = function (selected) {
		$scope.data.cover_photo_selection = selected;
	};
	$scope.coverPhotoSelection($scope.data.cover_photo_selection);

	$scope.onRemoveRow = function (key) {
    	if ($scope.data[key].length > 1) 
    		$scope.data[key] = angular.copy($scope.data[key].slice(0, $scope.data[key].length - 1));
	};
	$scope.addNewRow = function (key) {
		var obj = $scope.data[key][$scope.data[key].length - 1];
		var newObj = angular.copy(obj);
		var ifNotEmptyValues = true;
		for (var i in obj) {
			newObj[i] = '';
			if (obj[i].toString().trim() == '') {
				ifNotEmptyValues = false;
				break;
			}
		}
    	if (ifNotEmptyValues) {
			$scope.data[key].push(newObj);    			
    	}
	};

	$scope.onSubmitForm = function(){

  		var form_data = new FormData();
		for ( var key in $scope.data ) {
			var value = $scope.data[key];
			if (key === 'personal_core_skills' || 
				key === 'technical_core_skills' || 
				key === 'hobbies' || 
				key === 'educations' || 
				key === 'experiences') {
				value = JSON.stringify($scope.data[key]);
			} 
		    form_data.append(key, value);
		}

		if (updateResume) {
			http.updateResume(form_data).then(function (data) {
				$scope.errors = {};
				$scope.onClearForm();
				$uibModalInstance.close();
			}, function (data) {
				if (data.errors) {
					$scope.errors = data.errors;
				}
			});
		} else {
			http.createResume(form_data).then(function (data) {
				$scope.errors = {};
				$scope.onClearForm();
				$uibModalInstance.close();
			}, function (data) {
				if (data.errors) {
					$scope.errors = data.errors;
				}
			});			
		}
		
    };

    $scope.cancel = function() {
    	// $scope.items = items;
        $uibModalInstance.dismiss("cancel");
    };

}]);

jobholler.controller('ProductsCtrl', ['$scope', 'http', 'validator', 'global', function ($scope, http, validator, global) {

	$scope.$products = angular.copy($products);
    $scope._products = angular.copy($products);
	global.products.listTrigger(function ($products) {
        $scope.$products = angular.copy($products);
   		$scope._products = angular.copy($products);
        window.$products = angular.copy($products);
	});

	$scope.onClearForm = function () {
		if ($scope.updateProduct) {
			$scope.updateProduct = false;
			$scope.onClearForm();
			return;
		}

		$scope.data = {
			name: '',
			status: 'active',
			sku: '',
			associated_access: 'none',
			price: '',
			payment_plan_type: 'trial', /* trial, subscription, one_time_payment*/
			description: ''
		};

		$('.products.form-validation').find('.ng-dirty').removeClass('ng-dirty');
	};

	$scope.onClearForm();

	$scope.errors = {};

	$scope.$watch('errors', function(errors) {
        if (errors !== undefined) { 
        	validator.errors = errors;
        }
    });

	$scope.hasError = validator.hasError;
	
	$scope.showErrorBlock = validator.showErrorBlock;

	$scope.onSubmitForm = function () {
		if ($scope.updateProduct) {
			onUpdateProduct();
		} else {
			$scope.onCreateProduct();			
		}

	};

	var onUpdateProduct = function () {
		var data = angular.copy($scope.data);
		console.log(data);
		data.price = (data.price === undefined) ? '' : parseFloat(data.price);
		http.updateProduct(data).then(function (data) {
			$scope.errors = {};
			$scope.onClearForm();
			$scope.updateProduct = false;
		}, function (data) {
			if (data.errors) {
				$scope.errors = data.errors;
			}
		});
	};

	$scope.onCreateProduct = function () {
		var data = angular.copy($scope.data);
		data.price = (data.price === undefined) ? '' : parseFloat(data.price);
		http.createProduct(data).then(function (data) {
			$scope.errors = {};
			$scope.onClearForm();
		}, function (data) {
			if (data.errors) {
				$scope.errors = data.errors;
			}
		});
	};

	$scope.onUpdateProduct = function (product, name) {
		$scope.updateProduct = true;
		$scope.data = angular.copy(product);
		$scope.data.stripe_name = name;
		$scope.data.price = parseFloat(product.price);
	};

	$scope.onDeleteProduct = function (product) {
		var confirm = window.confirm('Are you sure?');
		if(confirm) {
			http.deleteProduct(product);
		}
		return false;
	};


	$scope.numPerPageOpt = [3, 5, 10, 20];

	$scope.numPerPage = $scope.numPerPageOpt[2];

	$scope.currentPage = 1;

	$scope.select = function(page) {
        var end, start;
        start = (page - 1) * $scope.numPerPage;
        end = start + $scope.numPerPage;
        return $scope.$products =  angular.copy($scope._products.slice(start, end));
    };

	$scope.onNumPerPageChange = function() {
        $scope.select(1);
        return $scope.currentPage = 1;
    };

    $scope.select(1);

	$scope.searchKeywords = '';
	$scope.onSearchFilterJobs = function () {
		if ($scope.searchKeywords === '') {
			$scope.$products = angular.copy($scope._products);
			return;
		}
        var products = angular.copy($scope._products);
		var newList = [];
		for (var i in products) {
 			var indexOfValue = products[i].name.toLowerCase().indexOf($scope.searchKeywords.toLowerCase());
 			if (indexOfValue !== -1) {
 				newList.push(products[i]);
 			}
		}
		$scope.$products = angular.copy(newList);
	};

	$scope.priceFieldDisabled = false;
	$scope.$watch('data.payment_plan_type', function (value) {console.log(value);
		$scope.priceFieldDisabled = false;
		if ($scope.data.payment_plan_type === 'trial') 
			$scope.priceFieldDisabled = true;
		if ($scope.priceFieldDisabled) {
			$scope.data.price = '';
			$('.products.form-validation').find('.ng-dirty.price').removeClass('ng-dirty');
		}
	});

}]);

jobholler.controller('BrandingsCtrl', ['$scope', 'http', 'validator', 'global', function ($scope, http, validator, global) {

	$scope.$brandings = [];
    $scope._brandings = [];
	global.brandings.listTrigger(function ($brandings) {
        $scope.$brandings = angular.copy($brandings);
   		$scope._brandings = angular.copy($brandings);
	});
	http.getAllBrandingsAjax({});

	$scope.onClearForm = function () {
		if ($scope.updateForm) {
			$scope.updateForm = false;
			$scope.onClearForm();
			return;
		}

		$scope.data = {
			title: '',
			description: '',
			price: '',
			per_payment_type: 'ENTRY',
			associated_access: 'none',
			payment_plan_type: 'trial'
		};

		$('.branding.form-validation').find('.ng-dirty').removeClass('ng-dirty');
	};

	$scope.onClearForm();
	$scope.errors = {};
	$scope.$watch('errors', function(errors) {
        if (errors !== undefined) { 
        	validator.errors = errors;
        }
    });
	$scope.hasError = validator.hasError;
	$scope.showErrorBlock = validator.showErrorBlock;
	$scope.onSubmitForm = function () {
		if ($scope.updateForm) {
			var data = angular.copy($scope.data);
			if (data.title !== ' ') {
				data['name'] = data.title.split(' ').join('-');
				data.price = (data.price === undefined) ? '' : parseFloat(data.price);
				http.updateBranding(data).then(function (data) {
					$scope.errors = {};
					$scope.onClearForm();
					$scope.updateForm = false;
				}, function (data) {
					if (data.errors) {
						$scope.errors = data.errors;
					}
				});
			}
		} else {
			var data = angular.copy($scope.data);
			if (data.title !== ' ') {
				data['name'] = data.title.split(' ').join('-');
				data.price = (data.price === undefined) ? '' : parseFloat(data.price);
				http.createBranding(data).then(function (data) {
					$scope.errors = {};
					$scope.onClearForm();
				}, function (data) {
					if (data.errors) {
						$scope.errors = data.errors;
					}
				});	

			}
					
		}

	};

	$scope.onUpdateForm = function (data) {
		$scope.updateForm = true;
		$scope.data = angular.copy(data);
		$scope.data.price = parseFloat(data.price);
	};

	$scope.onDeleteForm = function (data) {
		var confirm = window.confirm('Are you sure?');
		if(confirm) {
			http.deleteBranding(data);
		}
		return false;
	};


	$scope.numPerPageOpt = [3, 5, 10, 20];

	$scope.numPerPage = $scope.numPerPageOpt[2];

	$scope.currentPage = 1;

	$scope.select = function(page) {
        var end, start;
        start = (page - 1) * $scope.numPerPage;
        end = start + $scope.numPerPage;
        return $scope.$brandings =  angular.copy($scope._brandings.slice(start, end));
    };

	$scope.onNumPerPageChange = function() {
        $scope.select(1);
        return $scope.currentPage = 1;
    };

    $scope.select(1);

	$scope.searchKeywords = '';
	$scope.onSearchFilter = function () {
		if ($scope.searchKeywords === '') {
			$scope.$brandings = angular.copy($scope._brandings);
			return;
		}
        var brandings = angular.copy($scope._brandings);
		var newList = [];
		for (var i in brandings) {
 			var indexOfValue = brandings[i].title.toLowerCase().indexOf($scope.searchKeywords.toLowerCase());
 			if (indexOfValue !== -1) {
 				newList.push(brandings[i]);
 			}
		}
		$scope.$brandings = angular.copy(newList);
	};

	$scope.priceFieldDisabled = false;
	$scope.$watch('data.payment_plan_type', function (value) {
		$scope.priceFieldDisabled = false;
		if ($scope.data.payment_plan_type === 'trial') 
			$scope.priceFieldDisabled = true;
		if ($scope.priceFieldDisabled) {
			$scope.data.price = '';
			$('.products.form-validation').find('.ng-dirty.price').removeClass('ng-dirty');
		}
	});

}]);



jobholler.controller('BillingCtrl', ['$scope', 'http', 'validator', 'global', function ($scope, http, validator, global) {

	$scope.data = {
		test_key: '',
		test_secret: '',
		live_key: '',
		live_secret: ''
	};

	http.getBillingCredentials({}).then(function (data) {
		$scope.data = data.data.$stripe;	
	});

	$scope.onSubmitForm = function () {
		http.saveBillingCredentials($scope.data).then(function (data) {
			$scope.data = data.data.$stripe;	
		});
		
	};
	
}]);

jobholler.controller('IntegrationsCtrl', ['$scope', 'http', 'validator', 'global', function ($scope, http, validator, global) {

	
	
}]);

jobholler.controller('CouponCtrl', ['$scope', 'http', 'validator', 'global', function ($scope, http, validator, global) {

	http.getAllCoupon({});

	global.coupons.listTrigger(function (coupons) {
        $scope.$coupons = angular.copy(coupons);
   		$scope._coupons = angular.copy(coupons);
	});

	$scope.discount_type = [
		{
		'code' : 'amount_off',
		'value' : 'Amount Off'
		},
		{
		'code' : 'percent_off',
		'value' : 'Percent Off'
		}
	];

	$scope.duration_type = [
		{
			code: 'once',
			value: 'Once'
		},
		{
			code: 'repeating',
			value: 'Multi-Month'
		},
		{
			code: 'forever',
			value: 'Forever'
		}
	];	
	$scope.onClearForm = function () {
		if ($scope.updateCoupon) {
			$scope.updateCoupon = false;
			$scope.onClearForm();
			return false;
		} 

		$scope.data = {
			coupon: '',
			discount_type: '',
			discount_value: '',
			duration_type: '',
			duration_value: '',
			max_redemptions: '',
			redeem_by: ''
		};

		$scope.updateCoupon = false;
		$scope.ifDurationDisabled = false;

		$('.coupon.form-validation').find('.ng-dirty').removeClass('ng-dirty');
	};

	$scope.onClearForm();

	$scope.errors = {};

	$scope.$watch('errors', function(errors) {
        if (errors !== undefined) { 
        	validator.errors = errors;
        }
    });

	$scope.hasError = validator.hasError;
	
	$scope.showErrorBlock = validator.showErrorBlock;

	$scope.onSubmitForm = function () {
		if ($scope.updateCoupon) {
			$scope.onUpdateCoupon();
		} else {
			$scope.onCreateCoupon();
		}
		
	};
	$scope.onUpdateCoupon = function () {
		var data = angular.copy($scope.data);

		if (data.redeem_by === undefined) {
			data.redeem_by = '';
		}
		if (data.redeem_by !== '') {
			data.redeem_by = new Date(data.redeem_by.getFullYear(), data.redeem_by.getMonth(),  data.redeem_by.getDate(), 23, 59, 0, 0).getTime() / 1000;
		} 

		http.updateCoupon(data).then(function (response) {
			$scope.errors = {};
			$scope.onClearForm();
			$scope.updateCoupon = false;
		}, function (data) {
			$('.coupon.form-validation').find('.ng-dirty').removeClass('ng-dirty');
			if (data.errors) {
				$scope.errors = data.errors;
			}
			if (data.errors.type) {
				$scope.onClearForm();
				$scope.updateCoupon = false;
			}
			if (data.errors.$coupon.data) {
				$scope.$coupons = angular.copy(data.errors.$coupon.data);
   				$scope._coupons = angular.copy(data.errors.$coupon.data);
			}
		});
	};

	$scope.onCreateCoupon = function () {
		var data = angular.copy($scope.data);
		if (data.redeem_by === undefined) {
			data.redeem_by = '';
		}
		if (data.redeem_by !== '') {
			data.redeem_by = new Date(data.redeem_by.getFullYear(), data.redeem_by.getMonth(),  data.redeem_by.getDate(), 23, 59, 0, 0).getTime() / 1000;
		} 

		http.createCoupon(data).then(function (response) {
			$scope.errors = {};
			$scope.onClearForm();
			$scope.updateCoupon = false;
		}, function (data) {
			$('.coupon.form-validation').find('.ng-dirty').removeClass('ng-dirty');
			if (data.errors) {
				$scope.errors = data.errors;
			}
		});
	};

	$scope.onDeleteCoupon = function (coupon) {
		var confirm = window.confirm('Are you sure?');
		if(confirm) {
			http.deleteCoupon(coupon);
		}
		return false;
	};

	$scope.onUpdateCouponToForm = function (coupon) {
		$scope.updateCoupon = true;
		$scope.data = {
			id: coupon.id,
			coupon: coupon.id,
			discount_type: (coupon.percent_off === null) ? 'amount_off' : 'percent_off',
			discount_value: (coupon.percent_off === null) ? coupon.amount_off : coupon.percent_off,
			duration_type: coupon.duration,
			duration_value: (coupon.duration_in_months === null) ? '' : coupon.duration_in_months,
			max_redemptions: (coupon.max_redemptions === null) ? '' : coupon.max_redemptions,
			redeem_by: new Date(coupon.redeem_by * 1000)
		};
		$scope.onChangeDurationType();
	};

	$scope.onChangeDurationType = function () {
		switch ($scope.data.duration_type) {
			case 'once': 
				$scope.ifDurationDisabled = true;
				$scope.data.duration_value = 1;
			break;
			case 'multi-month':
			case 'repeating': 
				$scope.ifDurationDisabled = false;
			break;
			case 'forever': 
				$scope.ifDurationDisabled = true;
				$scope.data.duration_value = 0;
			break;
		}
	};


	$scope.today = function() {
        $scope.data.redeem_by = new Date();
    };
    $scope.today();

    $scope.clear = function() {
        $scope.data.redeem_by = null;
    };

    $scope.toggleMin = function() {
        $scope.minDate = $scope.minDate ? null : new Date();
    };

    $scope.toggleMin();
    $scope.maxDate = new Date(2020, 5, 22);

    $scope.open1 = function() {
        $scope.popup1.opened = true;
    };

    $scope.open2 = function() {
        $scope.popup2.opened = true;
    };

    $scope.setDate = function(year, month, day) {
        $scope.$apply(function () {
            $scope.data.redeem_by = new Date(year, month, day);
        });
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
    $scope.altInputFormats = ['M!/d!/yyyy'];

    $scope.popup1 = {
        opened: false
    };

    $scope.popup2 = {
        opened: false
    };

    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    var afterTomorrow = new Date();
    afterTomorrow.setDate(tomorrow.getDate() + 1);
	

	$scope.searchKeywords = '';
	$scope.onSearchFilterCoupon = function () {
		if ($scope.searchKeywords === '') {
			$scope.$coupons = angular.copy($scope._coupons);
			return;
		}
        var coupons = angular.copy($scope._coupons);
		var newList = [];
		for (var i in coupons) {
 			var indexOfValue = coupons[i].id.toLowerCase().indexOf($scope.searchKeywords.toLowerCase());
 			if (indexOfValue !== -1) {
 				newList.push(coupons[i]);
 			}
		}
		$scope.$coupons = angular.copy(newList);
	};
}]);

jobholler.controller('Google_analyticsCtrl', ['$scope', 'http', 'validator', 'global', function ($scope, http, validator, global) {

	$scope.data = {
		code: ''
	};

	http.getCodeGoogleAnalytics({}).then(function (response) {
		$scope.data.code = angular.copy(response.data.code);
	});
	
	$scope.onSubmitForm = function () {
		http.saveCodeGoogleAnalytics($scope.data).then(function (response) {
			$scope.data.code = angular.copy(response.data.code);
		});
	};
	
}]);

jobholler.controller('View_holler_historyCtrl', ['$scope', 'http', 'validator', 'global', function ($scope, http, validator, global) {

	$scope.$expired_jobs = angular.copy($expired_jobs);
	$scope._expired_jobs = angular.copy($expired_jobs);

	http.getAllExpiredJobsAjax({});

	global.jobs.listTrigger(function (jobs) {
   		window.$jobs = angular.copy(jobs);
	});

	global.expired_jobs.listTrigger(function (expired_jobs) {
        $scope.$expired_jobs = angular.copy(expired_jobs);
   		$scope._expired_jobs = angular.copy(expired_jobs);
   		window.$expired_jobs = angular.copy(expired_jobs);
	});
	
	$scope.onRepostJob = function (expired_jobs) {
        http.repostJob(expired_jobs);
	};

	$scope.searchKeywords = '';
	$scope.onSearchFilterExpired = function () {
		if ($scope.searchKeywords === '') {
			$scope.$expired_jobs = angular.copy($scope._expired_jobs);
			return;
		}
        var expired_jobs = angular.copy($scope._expired_jobs);
		var newList = [];
		for (var i in expired_jobs) {
 			var indexOfValue = expired_jobs[i].job_title.toLowerCase().indexOf($scope.searchKeywords.toLowerCase());
 			if (indexOfValue !== -1) {
 				newList.push(expired_jobs[i]);
 			}
		}
		$scope.$expired_jobs = angular.copy(newList);
	};
}]);

