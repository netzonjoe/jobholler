(function () {
    'use strict';

    String.prototype.toController = function() {
        return this.charAt(0).toUpperCase() + this.slice(1) + 'Ctrl';
    };
    angular.module('app')
        .config(['$routeProvider', function($routeProvider) {
            var routes, setRoutes;

            routes = [
                'dashboard',
                'members',
                'jobs',
                'view_holler_history',
                'resume',
                'products',
                'billing',
                'integrations',
                'coupon',
                'google_analytics',
                'brandings'
            ]

            setRoutes = function(route) {
                var config, url;
                url = '/' + route;
                config = {
                    templateUrl: 'templates/admin-dashboard/' + route + '.html',
                    controller: route.toController()
                };
                $routeProvider.when(url, config);
                return $routeProvider;
            };

            routes.forEach(function(route) {
                return setRoutes(route);
            });

            $routeProvider
                .when('/', {redirectTo: '/dashboard'})
                .when('/404', {templateUrl: 'templates/admin-dashboard/page/404.html'})
                .otherwise({ redirectTo: '/404'});

        }]
    );

})(); 