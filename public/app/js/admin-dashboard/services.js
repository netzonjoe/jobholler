'use strict';

var jobholler = angular.module('jobhollerServices', []);

jobholler.factory('http', ['$http', '$q', 'global', 'cfpLoadingBar', '$anchorScroll' , function($http, $q, global, cfpLoadingBar, $anchorScroll) {
	return {
		alert: {
			type: 'success',
            msg: 'Product information was updated successfully'
		},
		_token: $_token,
		post: function (url, data) {
			var self = this;
			cfpLoadingBar.start();
			return $q(function(resolve, reject) {
				data._token = self._token; 
		        jQuery.ajax({
		        	cache: false,
		        	type: 'POST',
		        	dataType: 'json',
		        	url: $route_list['POST'][url],
				    data: data,
				    headers: {
				        'X-CSRF-TOKEN': self._token
				    },
				   	success: function(data) {
				   		if (data.type !== undefined && data.msg !== undefined) {
				   			self.alert.type = data.type;	
				   			self.alert.msg = data.msg;	
				   			global.alert.listUpdate(self.alert);
				   		}

				   		self._token = data._token;
				   		delete data['_token'];
				      	resolve({
				   			data:data,
							status:200,
							statusText:"OK"	
				   		});
				   		cfpLoadingBar.complete();
				   		$anchorScroll();
				   	},
				   	error: function(data) {
				   		var data = JSON.parse(data.responseText);

				   		if (data.errors.type !== undefined && data.errors.msg !== undefined) {
				   			self.alert.type = data.errors.type;	
				   			self.alert.msg = data.errors.msg;	
				   			global.alert.listUpdate(self.alert);
				   		}

				   		self._token = data._token;
				   		delete data['_token'];
				      	reject(data);
				      	cfpLoadingBar.complete();
				   		$anchorScroll();
				   	}
		        });
  			});
		},
		postAndUpload: function (url, data) {
			var self = this;
			cfpLoadingBar.start();
			return $q(function(resolve, reject) {
				data._token = self._token; 
		        jQuery.ajax({
		        	type: 'POST',
		        	dataType: 'json',
		        	url: $route_list['POST'][url],
				    data: data,
				    headers: {
				        'X-CSRF-TOKEN': self._token
				    },
				    async: false,
					cache: false,
					contentType: false,
					processData: false,
				   	success: function(data) {
				   		if (data.type !== undefined && data.msg !== undefined) {
				   			self.alert.type = data.type;	
				   			self.alert.msg = data.msg;	
				   			global.alert.listUpdate(self.alert);
				   		}

				   		self._token = data._token;
				   		delete data['_token'];
				      	resolve({
				   			data:data,
							status:200,
							statusText:"OK"	
				   		});
				   		cfpLoadingBar.complete();
				   		$anchorScroll();
				   	},
				   	error: function(data) {
				   		var data = JSON.parse(data.responseText);
				   		if (data.type !== undefined && data.msg !== undefined) {
				   			self.alert.type = data.type;	
				   			self.alert.msg = data.msg;	
				   			global.alert.listUpdate(self.alert);
				   		}

				   		self._token = data._token;
				   		delete data['_token'];
				      	reject(data);
				      	cfpLoadingBar.complete();
				   		$anchorScroll();
				   	}
		        });
  			});
		},
		getAllUsersWithUserGroupAjax: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@getAllUsersWithUserGroupAjax', data).then(function(response) {
					if (response.data.$users !== undefined) {
			   			global.users.listUpdate(response.data.$users);	
			   		}
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		getUserGroups: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@getUserGroups', data).then(function(response) {
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
			
		},
		createMember: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@createMember', data).then(function(response) {
					if (response.data.$users !== undefined) {
			   			global.users.listUpdate(response.data.$users);	
			   		}
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
			
		},
		updateMember: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@updateMember', data).then(function(response) {
					if (response.data.$users !== undefined) {
			   			global.users.listUpdate(response.data.$users);	
			   		}
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		deleteMember: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@deleteMember', data).then(function(response) {
					if (response.data.$users !== undefined) {
			   			global.users.listUpdate(response.data.$users);	
			   		}
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		updateMemberStatus: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@updateMemberStatus', data).then(function(response) {
					if (response.data.$users !== undefined) {
			   			global.users.listUpdate(response.data.$users);	
			   		}
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		createJob: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@createJob', data).then(function(response) {
					if (response.data.$jobs !== undefined) {
			   			global.jobs.listUpdate(response.data.$jobs);	
			   		}
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		deleteJob: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@deleteJob', data).then(function(response) {
					if (response.data.$jobs !== undefined) {
			   			global.jobs.listUpdate(response.data.$jobs);	
			   		}
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		updateJob: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@updateJob', data).then(function(response) {
					if (response.data.$jobs !== undefined) {
			   			global.jobs.listUpdate(response.data.$jobs);	
			   		}
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		cloneJob: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@cloneJob', data).then(function(response) {
					if (response.data.$jobs !== undefined) {
			   			global.jobs.listUpdate(response.data.$jobs);	
			   		}
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		createProduct: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@createProduct', data).then(function(response) {
					if (response.data.$products !== undefined) {
			   			global.products.listUpdate(response.data.$products);	
			   		}
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		deleteProduct: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@deleteProduct', data).then(function(response) {
					if (response.data.$products !== undefined) {
			   			global.products.listUpdate(response.data.$products);	
			   		}
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		updateProduct: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@updateProduct', data).then(function(response) {
					if (response.data.$products !== undefined) {
			   			global.products.listUpdate(response.data.$products);	
			   		}
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		getBillingCredentials: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@getBillingCredentials', data).then(function(response) {
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		saveBillingCredentials: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@saveBillingCredentials', data).then(function(response) {
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		updateAdminProfile: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@updateAdminProfile', data).then(function(response) {
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		getAdminProfileDetails: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@getAdminProfileDetails', data).then(function(response) {
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		createResume: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.postAndUpload('AdminDashboardController@createResume', data).then(function(response) {

					if (response.data.$users_without_resume !== undefined) {
			   			global.users_without_resume.listUpdate(response.data.$users_without_resume);	
			   		}

			   		if (response.data.$users_with_resume !== undefined) {
			   			global.users_with_resume.listUpdate(response.data.$users_with_resume);	
			   		}
			   		
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		getUsersWithoutResumeAjax: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@getUsersWithoutResumeAjax', data).then(function(response) {
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		getUsersWithResumeAjax: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@getUsersWithResumeAjax', data).then(function(response) {
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		updateUserWithResumeStatus: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@updateUserWithResumeStatus', data).then(function(response) {
					if (response.data.$users_with_resume !== undefined) {
			   			global.users_with_resume.listUpdate(response.data.$users_with_resume);	
			   		}
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		updateUserWithResumeViewable: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@updateUserWithResumeViewable', data).then(function(response) {
					if (response.data.$users_with_resume !== undefined) {
			   			global.users_with_resume.listUpdate(response.data.$users_with_resume);	
			   		}
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		deleteUserWithResume: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@deleteUserWithResume', data).then(function(response) {
					if (response.data.$users_with_resume !== undefined) {
			   			global.users_with_resume.listUpdate(response.data.$users_with_resume);	
			   		}
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		updateResume: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.postAndUpload('AdminDashboardController@updateResume', data).then(function(response) {

					if (response.data.$users_without_resume !== undefined) {
			   			global.users_without_resume.listUpdate(response.data.$users_without_resume);	
			   		}

			   		if (response.data.$users_with_resume !== undefined) {
			   			global.users_with_resume.listUpdate(response.data.$users_with_resume);	
			   		}
			   		
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		createCoupon: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@createCoupon', data).then(function(response) {

					if (response.data.$coupon !== undefined) {
			   			global.coupons.listUpdate(response.data.$coupon.data);	
			   		}
			   		
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		getAllCoupon: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@getAllCoupon', data).then(function(response) {
					
					if (response.data.$coupon !== undefined) {
			   			global.coupons.listUpdate(response.data.$coupon.data);	
			   		}
			   		
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		deleteCoupon: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@deleteCoupon', data).then(function(response) {
					
					if (response.data.$coupon !== undefined) {
			   			global.coupons.listUpdate(response.data.$coupon.data);	
			   		}
			   		
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		updateCoupon: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@updateCoupon', data).then(function(response) {
					
					if (response.data.$coupon !== undefined) {
			   			global.coupons.listUpdate(response.data.$coupon.data);	
			   		}
			   		
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		saveCodeGoogleAnalytics: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@saveCodeGoogleAnalytics', data).then(function(response) {
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		getCodeGoogleAnalytics: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@getCodeGoogleAnalytics', data).then(function(response) {
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		getAllExpiredJobsAjax: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@getAllExpiredJobsAjax', data).then(function(response) {

					if (response.data.$expired_jobs !== undefined) {
			   			global.expired_jobs.listUpdate(response.data.$expired_jobs);	
			   		}

			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		getAllPostedJobsAjax: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@getAllPostedJobsAjax', data).then(function(response) {

					if (response.data.$jobs !== undefined) {
			   			global.jobs.listUpdate(response.data.$jobs);	
			   		}

			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		repostJob: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@repostJob', data).then(function(response) {

					if (response.data.$expired_jobs !== undefined) {
			   			global.expired_jobs.listUpdate(response.data.$expired_jobs);	
			   		}

			   		if (response.data.$jobs !== undefined) {
			   			global.jobs.listUpdate(response.data.$jobs);	
			   		}
			   		
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		getAllNotificationsAjax: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@getAllNotificationsAjax', data).then(function(response) {

					if (response.data.$notifications !== undefined) {
			   			global.notifications.listUpdate(response.data.$notifications);	
			   		}
			   		
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		updateAllNotificationOnClicked: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@updateAllNotificationOnClicked', data).then(function(response) {

					if (response.data.$notifications !== undefined) {
			   			global.notifications.listUpdate(response.data.$notifications);	
			   		}
			   		
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		updateAllNotificationOnViewed: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@updateAllNotificationOnViewed', data).then(function(response) {

					if (response.data.$notifications !== undefined) {
			   			global.notifications.listUpdate(response.data.$notifications);	
			   		}
			   		
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		showAllNotificationOnViewed: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@showAllNotificationOnViewed', data).then(function(response) {

					if (response.data.$notifications !== undefined) {
			   			global.notifications.listUpdate(response.data.$notifications);	
			   		}
			   		
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		hideAllNotificationOnViewed: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@hideAllNotificationOnViewed', data).then(function(response) {

					if (response.data.$notifications !== undefined) {
			   			global.notifications.listUpdate(response.data.$notifications);	
			   		}
			   		
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		createBranding: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@createBranding', data).then(function(response) {

					if (response.data.$brandings !== undefined) {
			   			global.brandings.listUpdate(response.data.$brandings);	
			   		}
			   		
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		deleteBranding: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@deleteBranding', data).then(function(response) {

					if (response.data.$brandings !== undefined) {
			   			global.brandings.listUpdate(response.data.$brandings);	
			   		}
			   		
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		updateBranding: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@updateBranding', data).then(function(response) {

					if (response.data.$brandings !== undefined) {
			   			global.brandings.listUpdate(response.data.$brandings);	
			   		}
			   		
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		getAllBrandingsAjax: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@getAllBrandingsAjax', data).then(function(response) {

					if (response.data.$brandings !== undefined) {
			   			global.brandings.listUpdate(response.data.$brandings);	
			   		}
			   		
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		getCompanyProfileAjax: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@getCompanyProfileAjax', data).then(function(response) {

					if (response.data.$company_profile !== undefined) {
			   			global.company_profile.listUpdate(response.data.$company_profile);	
			   		}

			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		getAllCompaniesAjax: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('AdminDashboardController@getAllCompaniesAjax', data).then(function(response) {

					if (response.data.$registered_companies !== undefined) {
			   			global.registered_companies.listUpdate(response.data.$registered_companies);	
			   		}

			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		}
	};
}]);

jobholler.factory('global', [function() {
	return {
		alert: {
			callback: null,
			listTrigger: function (callback) {
				if (callback !== undefined) {
					this.callback = callback;
				}
			},
			listUpdate: function ($data) {
				if (this.callback !== null) {
					this.callback($data);
				}
			}
		},
		users: {
			callback: null,
			listTrigger: function (callback) {
				if (callback !== undefined) {
					this.callback = callback;
				}
			},
			listUpdate: function ($data) {
				if (this.callback !== null) {
					this.callback($data);
				}
			}
		},
		jobs: {
			callback: null,
			listTrigger: function (callback) {
				if (callback !== undefined) {
					this.callback = callback;
				}
			},
			listUpdate: function ($data) {
				if (this.callback !== null) {
					this.callback($data);
				}
			}
		},
		products: {
			callback: null,
			listTrigger: function (callback) {
				if (callback !== undefined) {
					this.callback = callback;
				}
			},
			listUpdate: function ($data) {
				if (this.callback !== null) {
					this.callback($data);
				}
			}
		},
		users_without_resume: {
			callback: null,
			listTrigger: function (callback) {
				if (callback !== undefined) {
					this.callback = callback;
				}
			},
			listUpdate: function ($data) {
				if (this.callback !== null) {
					this.callback($data);
				}
			}
		},
		users_with_resume: {
			callback: null,
			listTrigger: function (callback) {
				if (callback !== undefined) {
					this.callback = callback;
				}
			},
			listUpdate: function ($data) {
				if (this.callback !== null) {
					this.callback($data);
				}
			}
		},
		coupons: {
			callback: null,
			listTrigger: function (callback) {
				if (callback !== undefined) {
					this.callback = callback;
				}
			},
			listUpdate: function ($data) {
				if (this.callback !== null) {
					this.callback($data);
				}
			}
		},
		expired_jobs: {
			callback: null,
			listTrigger: function (callback) {
				if (callback !== undefined) {
					this.callback = callback;
				}
			},
			listUpdate: function ($data) {
				if (this.callback !== null) {
					this.callback($data);
				}
			}
		},
		notifications: {
			callback: null,
			listTrigger: function (callback) {
				if (callback !== undefined) {
					this.callback = callback;
				}
			},
			listUpdate: function ($data) {
				if (this.callback !== null) {
					this.callback($data);
				}
			}
		},
		brandings: {
			callback: null,
			listTrigger: function (callback) {
				if (callback !== undefined) {
					this.callback = callback;
				}
			},
			listUpdate: function ($data) {
				if (this.callback !== null) {
					this.callback($data);
				}
			}
		},
		company_profile: {
			callback: null,
			listTrigger: function (callback) {
				if (callback !== undefined) {
					this.callback = callback;
				}
			},
			listUpdate: function ($data) {
				if (this.callback !== null) {
					this.callback($data);
				}
			}
		},
		registered_companies: {
			callback: null,
			listTrigger: function (callback) {
				if (callback !== undefined) {
					this.callback = callback;
				}
			},
			listUpdate: function ($data) {
				if (this.callback !== null) {
					this.callback($data);
				}
			}
		}
	};
}]);

jobholler.factory('validator', [function() {
	return {
		errors: {},
		hasError: function (key, classError) {
			return (this.errors[key] === undefined) ? '' : classError;
		},
		showErrorBlock: function (key, classError) {
			return (this.errors[key] === undefined) ? classError : '';
		}
	};
}]);

jobholler.factory('generator', [function() {
	return {
		reference: function () {
			var first3Char = "";
		    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

		    for( var i=0; i < 3; i++ )
		        first3Char += possible.charAt(Math.floor(Math.random() * possible.length));

		    return first3Char + Math.round(new Date().getTime()).toString().substring(6);
		}
	};
}]);


