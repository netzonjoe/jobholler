'use strict';

var jobholler = angular.module('jobhollerServices', []);

jobholler.factory('http', ['$http', '$q', 'global', 'cfpLoadingBar', '$anchorScroll', function($http, $q, global, cfpLoadingBar, $anchorScroll) {
	return {
		alert: {
			type: 'success',
            msg: 'Product information was updated successfully'
		},
		_token: $_token,
		post: function (url, data) {
			var self = this;
			cfpLoadingBar.start();
			return $q(function(resolve, reject) {
				data._token = self._token; 
		        jQuery.ajax({
		        	cache: false,
		        	type: 'POST',
		        	dataType: 'json',
		        	url: $route_list['POST'][url],
				    data: data,
				    headers: {
				        'X-CSRF-TOKEN': self._token
				    },
				   	success: function(data) {
				   		if (data.type !== undefined && data.msg !== undefined) {
				   			self.alert.type = data.type;	
				   			self.alert.msg = data.msg;	
				   			global.alert.listUpdate(self.alert);
				   		}

				   		self._token = data._token;
				   		delete data['_token'];
				      	resolve({
				   			data:data,
							status:200,
							statusText:"OK"	
				   		});
				   		cfpLoadingBar.complete();
				   		$anchorScroll();
				   	},
				   	error: function(data) {
				   		var data = JSON.parse(data.responseText);

				   		if (data.errors.type !== undefined && data.errors.msg !== undefined) {
				   			self.alert.type = data.errors.type;	
				   			self.alert.msg = data.errors.msg;	
				   			global.alert.listUpdate(self.alert);
				   		}

				   		self._token = data._token;
				   		delete data['_token'];
				      	reject(data);
				      	cfpLoadingBar.complete();
				      	$anchorScroll();
				   	}
		        });
  			});
		},
		postAndUpload: function (url, data) {
			var self = this;
			cfpLoadingBar.start();
			return $q(function(resolve, reject) {
				data._token = self._token; 
		        jQuery.ajax({
		        	type: 'POST',
		        	dataType: 'json',
		        	url: $route_list['POST'][url],
				    data: data,
				    headers: {
				        'X-CSRF-TOKEN': self._token
				    },
				    async: false,
					cache: false,
					contentType: false,
					processData: false,
				   	success: function(data) {
				   		if (data.type !== undefined && data.msg !== undefined) {
				   			self.alert.type = data.type;	
				   			self.alert.msg = data.msg;	
				   			global.alert.listUpdate(self.alert);
				   		}

				   		self._token = data._token;
				   		delete data['_token'];
				      	resolve({
				   			data:data,
							status:200,
							statusText:"OK"	
				   		});
				   		cfpLoadingBar.complete();
				   		$anchorScroll();
				   	},
				   	error: function(data) {
				   		var data = JSON.parse(data.responseText);
				   		if (data.type !== undefined && data.msg !== undefined) {
				   			self.alert.type = data.type;	
				   			self.alert.msg = data.msg;	
				   			global.alert.listUpdate(self.alert);
				   		}

				   		self._token = data._token;
				   		delete data['_token'];
				      	reject(data);
				      	cfpLoadingBar.complete();
				      	$anchorScroll();
				   	}
		        });
  			});
		},
		createResume: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.postAndUpload('CandidateDashboardController@createResume', data).then(function(response) {

					if (response.data.$resume !== undefined) {
			   			global.resume.listUpdate(response.data.$resume);	
			   		}
			   		
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		updateResume: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.postAndUpload('CandidateDashboardController@updateResume', data).then(function(response) {

					if (response.data.$resume !== undefined) {
			   			global.resume.listUpdate(response.data.$resume);	
			   		}
			   		
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		getResumeByUserAjax: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('CandidateDashboardController@getResumeByUserAjax', data).then(function(response) {

					if (response.data.$resume !== undefined) {
			   			global.resume.listUpdate(response.data.$resume);	
			   		}
			   		
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		getAllJobsApliedAjax: function (data) {
			var self = this;
			return $q(function(resolve, reject) {
				self.post('CandidateDashboardController@getAllJobsApliedAjax', data).then(function(response) {

					if (response.data.$applied_jobs !== undefined) {
			   			global.applied_jobs.listUpdate(response.data.$applied_jobs);	
			   		}
			   		
			   		resolve({
			   			data:response.data,
						status:200,
						statusText:"OK"	
			   		});
				}, function (data) {
					reject(data);
				});
			});
		},
		
	};
}]);

jobholler.factory('global', [function() {
	return {
		alert: {
			callback: null,
			listTrigger: function (callback) {
				if (callback !== undefined) {
					this.callback = callback;
				}
			},
			listUpdate: function ($data) {
				if (this.callback !== null) {
					this.callback($data);
				}
			}
		},
		resume: {
			callback: null,
			listTrigger: function (callback) {
				if (callback !== undefined) {
					this.callback = callback;
				}
			},
			listUpdate: function ($data) {
				if (this.callback !== null) {
					this.callback($data);
				}
			}
		},
		applied_jobs: {
			callback: null,
			listTrigger: function (callback) {
				if (callback !== undefined) {
					this.callback = callback;
				}
			},
			listUpdate: function ($data) {
				if (this.callback !== null) {
					this.callback($data);
				}
			}
		}
	};
}]);

jobholler.factory('validator', [function() {
	return {
		errors: {},
		hasError: function (key, classError) {
			return (this.errors[key] === undefined) ? '' : classError;
		},
		showErrorBlock: function (key, classError) {
			return (this.errors[key] === undefined) ? classError : '';
		}
	};
}]);

jobholler.factory('generator', [function() {
	return {
		reference: function () {
			var first3Char = "";
		    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

		    for( var i=0; i < 3; i++ )
		        first3Char += possible.charAt(Math.floor(Math.random() * possible.length));

		    return first3Char + Math.round(new Date().getTime()).toString().substring(6);
		}
	};
}]);


