'use strict';

var jobholler = angular.module('jobhollerDirectives', []);

jobholler.directive('phoneInput', function($filter, $browser) {
    return {
        require: 'ngModel',
        link: function($scope, $element, $attrs, ngModelCtrl) {
            var listener = function() {
                var value = $element.val().replace(/[^0-9]/g, '');
                $element.val($filter('tel')(value, false));
            };

            // This runs when we update the text field
            ngModelCtrl.$parsers.push(function(viewValue) {
                return viewValue.replace(/[^0-9]/g, '').slice(0,10);
            });

            // This runs when the model gets updated on the scope directly and keeps our view in sync
            ngModelCtrl.$render = function() {
                $element.val($filter('tel')(ngModelCtrl.$viewValue, false));
            };

            $element.bind('change', listener);
            $element.bind('keydown', function(event) {
                var key = event.keyCode;
                // If the keys include the CTRL, SHIFT, ALT, or META keys, or the arrow keys, do nothing.
                // This lets us support copy and paste too
                if (key == 91 || (15 < key && key < 19) || (37 <= key && key <= 40)){
                    return;
                }
                $browser.defer(listener); // Have to do this or changes don't get picked up properly
            });

            $element.bind('paste cut', function() {
                $browser.defer(listener);
            });
        }

    };
});
jobholler.filter('tel', function () {
    return function (tel) {
        if (!tel) { return ''; }

        var value = tel.toString().trim().replace(/^\+/, '');

        if (value.match(/[^0-9]/)) {
            return tel;
        }

        var country, city, number;

        switch (value.length) {
            case 1:
            case 2:
            case 3:
                city = value;
                break;

            default:
                city = value.slice(0, 3);
                number = value.slice(3);
        }

        if(number){
            if(number.length>3){
                number = number.slice(0, 3) + '-' + number.slice(3,7);
            }
            else{
                number = number;
            }

            return ("(" + city + ") " + number).trim();
        }
        else{
            return "(" + city;
        }

    };
});

jobholler.directive('selectPicker', function($timeout){
    return {
      restrict: 'A',
      link:function(scope, elem){
        $timeout(function() {
          elem.selectpicker({showSubtext:true});
          elem.selectpicker('refresh');
        }, 0);
      }
    };
});

jobholler.directive('checkboxPicker', function(){
    return {
      restrict: 'A',
      link:function(scope, elem){
        elem.click(function () {
            jQuery('input[name*=\'selected\']').prop('checked', elem[0].checked);
        });
      }
    };
});

jobholler.directive('restrict', function($parse){
    return {
      restrict: 'A',
      require: 'ngModel',
        link: function(scope, iElement, iAttrs, controller) {
            scope.$watch(iAttrs.ngModel, function(value) {
                if (!value) {
                    return;
                }
                $parse(iAttrs.ngModel).assign(scope, value.toUpperCase().replace(new RegExp(iAttrs.restrict, 'g'), '').replace(/\s+/g, '-'));
            });
        }
    };
});

jobholler.directive('fileModel', ['$parse', function ($parse) {
    return {
       restrict: 'A',
       link: function(scope, element, attrs) {
          var model = $parse(attrs.fileModel);
          var modelSetter = model.assign;
          
          element.bind('change', function(){
             scope.$apply(function(){
                modelSetter(scope, element[0].files[0]);
             });
          });
       }
    };
 }]);

jobholler.directive('fileImageSrc', ['$parse', function ($parse) {
    return {
       restrict: 'A',
       scope: {
            fileImageSrc: '='
       },
       link: function(scope, element, attrs) {
          // var model = $parse(attrs.fileImageModel);
          // var modelSetter = model.assign;
          element.bind('change', function(){
             scope.$apply(function(){
                // modelSetter(scope, element[0].files[0]);
                if (element[0].files[0]) {
                    var FR = new FileReader();
                    FR.onload = function(e) {
                        scope.$apply(function () {
                            scope.fileImageSrc = e.target.result;
                        });
                    };       
                    FR.readAsDataURL( element[0].files[0] );
                }
             });

          });
       }
    };
 }]);

jobholler.directive('onReadyState', function () {
    return {
       restrict: 'A',
       link: function(scope, element, attrs) {
          element[0].style.display = "block";
       }
    };
 });

