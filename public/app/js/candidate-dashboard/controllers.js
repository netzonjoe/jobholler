'use strict';
var jobholler = angular.module('jobhollerControllers', []);

jobholler.controller('JobhollerCtrl', ['$scope', 'global', '$timeout',  function ($scope, global, $timeout) {
	$scope.$alerts = [];
	global.alert.listTrigger(function (alert) {console.log(alert);
        $scope.$alerts.push(alert);
        $timeout(function () {
        	$scope.$alerts.splice($scope.$alerts.length - 1, 1);
        }, 5000);
	});
}]);

jobholler.controller('HeaderCtrl', ['$scope', 'http', 'validator', 'global', function ($scope, http, validator, global) {
	$scope.baseurl = $baseurl;
	// $scope.has_company_profile = false;
	// global.company_profile.listTrigger(function (company_profile) {
	// 	if (company_profile !== null) {
	// 		$scope.has_company_profile = true;
	//    		$scope.company_name = company_profile.company_name;
	// 	}
	// });
	
	// http.getCompanyProfileAjax({});
}]);

jobholler.controller('DashboardCtrl', ['$scope', '$http', function ($scope, $http) {
	alert(123);

	// $http.get('phones/phones.json').success(function(data) {
	//   $scope.phones = data;
	// });

	// $scope.orderProp = 'age';
}]);

jobholler.controller('ResumeCtrl', ['$scope', 'http', 'validator', 'global', function ($scope, http, validator, global) {
	$scope.$imagesDir = $imagesDir;
	$scope.baseurl = $baseurl;
	$scope.updateResume = false;
	
	global.resume.listTrigger(function (resume) {
		$scope.updateResume = true;
		resume.years_of_experience = parseInt(resume.years_of_experience);
		resume.expected_salary = parseFloat(resume.expected_salary);
		if (resume.profile_photo !== '') {
			$scope.profile_photo_src = $baseurl + resume.profile_photo;
		}
		if (resume.cover_photo !== '') {
			$scope.cover_photo_src = $baseurl + resume.cover_photo;
		}
		
	    $scope.data = angular.copy(resume);	
	});
	http.getResumeByUserAjax({});

	$scope.onClearForm = function () {
		$scope.data = {
			candidate_user: '',
			full_name: '',
			tagline: '',
			about_me: '',
			profile_photo: '',
			years_of_experience: '',
			expected_salary: '',
			personal_core_skills: [
				{
					name: '',
					description: '',
					level: '1'
				}
			],
			technical_core_skills: [
				{
					name: '',
					description: '',
					level: '1'
				}
			],
			hobbies: [
				{
					name: ''
				}
			],
			educations: [
				{
					school_name: '',
					qualifications: '',
					year_attended_from: '',
					year_attended_to: ''
				}
			],
			experiences: [
				{
					company: '',
					job_description: '',
					start_date: '',
					end_date: ''
				}
			],
			resume_document: '',
			cover_photo: '',
			cover_photo_selection: '1'

		};
		$('.resume.form-validation').find('.ng-dirty').removeClass('ng-dirty');
	};

	$scope.onClearForm();
	
	$scope.coverPhotoSelection = function (selected) {
		$scope.data.cover_photo_selection = selected;
	};
	$scope.coverPhotoSelection($scope.data.cover_photo_selection);

	$scope.onRemoveRow = function (key, index) {
    	if ($scope.data[key].length > 1) 
			var tempList = angular.copy($scope.data[key]);
			var len = tempList.length;
    		$scope.data[key] = tempList.slice(0, index).concat( tempList.slice(index + 1) );	
	};
	$scope.addNewRow = function (key) {
		var obj = $scope.data[key][$scope.data[key].length - 1];
		var newObj = angular.copy(obj);
		var ifNotEmptyValues = true;
		for (var i in obj) {
			newObj[i] = '';
			if (obj[i].toString().trim() == '') {
				ifNotEmptyValues = false;
				break;
			}
		}
    	if (ifNotEmptyValues) {
			$scope.data[key].push(newObj);    			
    	}
	};

	$scope.onSubmitForm = function(){

  		var form_data = new FormData();
		for ( var key in $scope.data ) {
			var value = $scope.data[key];
			if (key === 'personal_core_skills' || 
				key === 'technical_core_skills' || 
				key === 'hobbies' || 
				key === 'educations' || 
				key === 'experiences') {
				value = JSON.stringify($scope.data[key]);
			} 
		    form_data.append(key, value);
		}

		if ($scope.updateResume) {
			http.updateResume(form_data).then(function (data) {
				$scope.errors = {};
			}, function (data) {
				if (data.errors) {
					$scope.errors = data.errors;
				}
			});
		} else {
			http.createResume(form_data).then(function (data) {
				$scope.errors = {};
				$scope.onClearForm();
			}, function (data) {
				if (data.errors) {
					$scope.errors = data.errors;
				}
			});			
		}
		
    };

}]);

jobholler.controller('Jobs_appliedCtrl', ['$scope', 'http', 'validator', 'global', 'generator',	function ($scope, http, validator, global, generator) {
	
	$scope.$applied_jobs = [];
	$scope._applied_jobs = [];
	global.applied_jobs.listTrigger(function (applied_jobs) {
	    $scope.$applied_jobs = angular.copy(applied_jobs);	
	    $scope._applied_jobs = angular.copy(applied_jobs);
	});
	http.getAllJobsApliedAjax({});

	$scope.status = {
		pending: 'warning',
		rejected: 'danger',
		accepted: 'success'
	};
	$scope.numPerPageOpt = [3, 5, 10, 20];
	$scope.numPerPage = $scope.numPerPageOpt[2];
	$scope.currentPage = 1;

	$scope.select = function(page) {
        var end, start;
        start = (page - 1) * $scope.numPerPage;
        end = start + $scope.numPerPage;
        return $scope.$applied_jobs =  angular.copy($scope._applied_jobs.slice(start, end));
    };

	$scope.onNumPerPageChange = function() {
        $scope.select(1);
        return $scope.currentPage = 1;
    };

    $scope.select(1);

    $scope.searchKeywords = '';
	$scope.onSearchFilter = function () {
		if ($scope.searchKeywords === '') {
			$scope.$applied_jobs = angular.copy($scope._applied_jobs);
			return;
		}
        var applied_jobs = angular.copy($scope._applied_jobs);
		var newList = [];
		for (var i in applied_jobs) {
 			var indexOfValue = applied_jobs[i].job_title.toLowerCase().indexOf($scope.searchKeywords.toLowerCase());
 			if (indexOfValue !== -1) {
 				newList.push(applied_jobs[i]);
 			}
		}
		$scope.$applied_jobs = angular.copy(newList);
	};
}]);

jobholler.controller('BrandingCtrl', ['$scope', 'http', 'validator', 'global', 'generator',	function ($scope, http, validator, global, generator) {

}]);

