<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company_profile extends Model {

	protected $table = 'company_profiles';
	public $timestamps = true;
	protected $fillable = array('user_id', 'company_name_slug', 'company_name', 'tagline', 'company_description', 'company_color', 'company_website', 'company_logo', 'company_icon', 'company_video', 'company_perks', 'social_networks');

	 /**
     * Get the the Job for the User.
     */
    public function job()
    {
        return $this->hasMany('App\Job');
    }
}