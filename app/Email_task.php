<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Email_task extends Model {

	protected $table = 'email_tasks';
	public $timestamps = true;
	protected $fillable = array('data', 'is_sent');

}