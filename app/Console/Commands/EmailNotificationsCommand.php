<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Event;
use App\Events\ActionEmailSender;
use App\Email_task;
class EmailNotificationsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cronjob:email-notifications';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cronjob executes email notifications.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {   
        $emails_to_send = Email_task::where('is_sent', false)->get()->toArray();
        if (count($emails_to_send) > 0) {
            // \Log::error('There is an update.');
        } else {
            // \Log::error('There is no update');
        }
        foreach ($emails_to_send as $key => $value) {
            $data = unserialize($value['data']);
            Event::fire(new ActionEmailSender($data));
            $email_to_update = Email_task::find($value['id']);
            $email_to_update->is_sent = true;
            $email_to_update->save();
            // \Log::error('Update id is : ' . $email_to_update->id );
        }
        
        $this->info('This command can only be seen on EmailNotificationsCommand file.');
    }
}
