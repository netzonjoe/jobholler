<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job_post extends Model {

	protected $table = 'job_posts';
	public $timestamps = true;
	protected $fillable = array('job_title_slug', 'job_title', 'job_type', 'minimum_salary', 'maximum_salary', 'paid_per', 'job_location', 'job_description', 'skills', 'experience_level', 'start_date', 'contact_name', 'contact_number', 'reference', 
        'company_profile_id', 'user_id');
    
    protected $dates = ['start_date'];
    /**
    * Get the Job that owns the User.
    */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function company()
    {
        return $this->belongsTo('App\Company_profile', 'id');
    }

}