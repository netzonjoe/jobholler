<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_permission extends Model {

	protected $table = 'user_permissions';
	public $timestamps = true;
	protected $fillable = array('uri', 'method', 'is_enabled');

    /**
    * Get the User_group that owns the User_permission.
    */
    public function userGroup()
    {
        return $this->belongsTo('App\User_group');
    }


}