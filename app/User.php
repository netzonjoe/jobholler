<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;
use Laravel\Cashier\Contracts\Billable as BillableContract;

class User extends Authenticatable implements BillableContract 
{

    use Billable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'firstname', 'lastname', 'phone', 'email', 'password', 'address', 'city', 'state', 'zip', 'country', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dates = ['trial_ends_at', 'subscription_ends_at'];


    /**
    * Get the User_group that owns the User.
    */
    public function userGroup()
    {
        return $this->belongsTo('App\User_group');
    }

    /**
     * Get the the Job for the User.
     */
    public function job()
    {
        return $this->hasMany('App\Job');
    }

    /**
     * Get the resume record associated with the user.
     */
    public function resume()
    {
        return $this->hasOne('App\Resume');
    }
}
