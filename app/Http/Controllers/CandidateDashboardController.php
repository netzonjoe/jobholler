<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Validator;
use Auth;
use App\Branding;
use App\Google_analytic;
use App\Payment_gateway;
use App\Company_profile;
use App\Notification;
use App\Applied_job;
use App\User_group;
use App\Product;
use App\Resume;
use App\User;
use App\Job_post;
use App\Email_task;
use Mail;
use Route;
use DB;
use Acme\Rest;
use Acme\GlobalCtrlHelper;
use Carbon\Carbon;
use Stripe\Stripe;
use Stripe\Plan;
use Stripe\Coupon;
use Config;
use Exception;
use File;
use Event;
use App\Events\ActionEmailSender;

class CandidateDashboardController extends Controller
{	

  	public function index() {
  		$_token = csrf_token();
          $route_list = json_encode(GlobalCtrlHelper::getRouteList('CandidateDashboardController'));
         	$baseurl = asset('');
          $imagesDir = asset('/img');
          $angular_directive_alert = GlobalCtrlHelper::getAngularDirectiveAlert();
          $google_analytic = '';

          $job_listing = asset('/job-listing');
          // $jobs = json_encode($this->getAllPostedJobs());
          // $expired_jobs = json_encode($this->getAllExpiredJobs());

     		return view('candidate_dashboard.index', 
     			compact('_token', 'route_list', 'baseurl', 'imagesDir', 'angular_directive_alert', 'google_analytic',
     					'job_listing'));
  	}
 	  
    protected function getResumeByUser() {
        $resume = Resume::where('user_id', Auth::user()->id)->first()->toArray();
        foreach ($resume as $key => $value) {
            if ($key === 'personal_core_skills') {
              $value = json_decode(unserialize($value));
            } else if ($key === 'technical_core_skills') {
              $value = json_decode(unserialize($value));
            } else if ($key === 'hobbies') {
              $value = json_decode(unserialize($value));
            } else if ($key === 'educations') {
              $value = json_decode(unserialize($value));
            } else if ($key === 'experiences') {
              $value = json_decode(unserialize($value));
            }
            $resume[$key] = $value; 
        }
        return $resume;
    }

    public function getResumeByUserAjax() {
        return Rest::success(['$resume' => $this->getResumeByUser()]);
    }

    protected function resumeValidator(array $data, $option = []) {
        $fields = [
            'full_name' => 'required|min:1|max:255',
            'tagline' => 'required|min:1|max:255',
            'about_me' => 'required',
            'profile_photo' => 'required|mimes:jpeg,bmp,png',
            'cover_photo' => 'mimes:jpeg,bmp,png',
            'years_of_experience' => 'required',
            'expected_salary' => 'required'
        ];
        // 'user_id', 'full_name', 'tagline', 'about_me', 'profile_photo', 'years_of_experience', 'expected_salary', 'personal_core_skills', 'technical_core_skills', 'hobbies', 'educations', 'experiences', 'resume_document', 'cover_photo', 'cover_photo_selection'
        return Validator::make($data , $fields + $option);
    }

    protected function resumeUpdateValidator(array $data, $option = []) {
        $fields = [
            'full_name' => 'required|min:1|max:255',
            'tagline' => 'required|min:1|max:255',
            'about_me' => 'required',
            'years_of_experience' => 'required',
            'expected_salary' => 'required'
        ];
        return Validator::make($data, $fields + $option);
    }

    protected function resumeCreate(array $data) {
        $resume = new Resume([
            'user_id' => $data['user_id'] , 
            'full_name' => $data['full_name'] , 
            'tagline' => $data['tagline'] , 
            'about_me' => $data['about_me'] , 
            'profile_photo' => $data['profile_photo'] , 
            'years_of_experience' => $data['years_of_experience'] , 
            'expected_salary' => $data['expected_salary'] , 
            'personal_core_skills' => serialize($data['personal_core_skills']) , 
            'technical_core_skills' => serialize($data['technical_core_skills']) , 
            'hobbies' => serialize($data['hobbies']) , 
            'educations' => serialize($data['educations']) , 
            'experiences' => serialize($data['experiences']) , 
            'resume_document' => $data['resume_document'] , 
            'cover_photo' => $data['cover_photo'] , 
            'cover_photo_selection' => $data['cover_photo_selection'] 
        ]);
        return $resume->save();
    }

    public function createResume(Request $request) {
        if ($request->ajax()) { 
            $params = $request->all();
            $params['user_id'] = Auth::user()->id;
            $validator = $this->resumeValidator($params);
            if($validator->fails()) {
                return Rest::failed($validator);
            }

            if ($request->hasFile('profile_photo')) {
                $params['profile_photo'] = 
                    $request->file('profile_photo')->move('uploads/curriculum_vitae/profile_photo/', User_group::MakeAttributeId() . '.jpg');
            }

            if ($request->hasFile('cover_photo')) {
                $params['cover_photo'] = 
                    $request->file('cover_photo')->move('uploads/curriculum_vitae/cover_photo/', User_group::MakeAttributeId() . '.jpg');
            }

            if ($request->hasFile('resume_document')) {
                $extension = $request->file('resume_document')->getClientOriginalExtension();
                $params['resume_document'] = 
                    $request->file('resume_document')->move('uploads/curriculum_vitae/file/', User_group::MakeAttributeId() . '.' . $extension);
            }

            $this->resumeCreate($params); 

            $resume = $this->getResumeByUser();
         
            return Rest::success(['$resume' => $resume, 'msg' => 'Resume information was created successfully']);
        }
    }

    public function updateResume(Request $request) {
        if ($request->ajax()) { 
            $params = $request->all();
            $params['user_id'] = Auth::user()->id;
            $validator = $this->resumeUpdateValidator($params);
            if($validator->fails()) {
                return Rest::failed($validator);
            }

            $resume = Resume::find($params['id']);

            if ($request->hasFile('profile_photo')) {
                $filename = public_path() . '/' . $resume->profile_photo;
                if (File::exists($filename)) {
                    File::delete($filename);
                } 
                $resume->profile_photo = 
                    $request->file('profile_photo')->move('uploads/curriculum_vitae/profile_photo/', User_group::MakeAttributeId() . '.jpg');
            }

            if ($request->hasFile('cover_photo')) {
                $filename = public_path() . '/' . $resume->cover_photo;
                if (File::exists($filename)) {
                    File::delete($filename);
                } 
                $resume->cover_photo = 
                    $request->file('cover_photo')->move('uploads/curriculum_vitae/cover_photo/', User_group::MakeAttributeId() . '.jpg');
            }

            if ($request->hasFile('resume_document')) {
                $filename = public_path() . '/' . $resume->resume_document;
                if (File::exists($filename)) {
                    File::delete($filename);
                } 
                $resume->resume_document = 
                    $request->file('resume_document')->move('uploads/curriculum_vitae/file/', User_group::MakeAttributeId() . '.jpg');
            }
            
            $resume->full_name = $params['full_name'];
            $resume->tagline = $params['tagline'];
            $resume->about_me = $params['about_me'];
            $resume->years_of_experience = $params['years_of_experience'];
            $resume->expected_salary = $params['expected_salary'];
            $resume->personal_core_skills = serialize($params['personal_core_skills']);
            $resume->technical_core_skills = serialize($params['technical_core_skills']);
            $resume->hobbies = serialize($params['hobbies']);
            $resume->educations = serialize($params['educations']);
            $resume->experiences = serialize($params['experiences']);
            $resume->cover_photo_selection = $params['cover_photo_selection'];

            $resume->save();

            $resume = $this->getResumeByUser();

            return Rest::success(['$resume' => $resume, 'msg' => 'Resume information was updated successfully']);
        }
    }

    private function getAllJobsAplied() {
         $applied_jobs = DB::table('applied_jobs')
            ->where('applied_jobs.user_id', Auth::user()->id)
            ->join('job_posts', 'job_posts.id', '=', 'applied_jobs.jobs_id')
            ->join('company_profiles', 'company_profiles.id', '=', 'job_posts.company_profile_id')
            ->select('job_posts.job_title', 'company_profiles.company_name', 'applied_jobs.*', DB::raw('DATE_FORMAT(applied_jobs.created_at, \'%M %d, %Y\') as created_at'))
            ->orderBy('applied_jobs.created_at', 'desc')
            ->get();
        return json_decode(json_encode($applied_jobs), true);
    }

    public function getAllJobsApliedAjax() {
        return Rest::success(['$applied_jobs' => $this->getAllJobsAplied()]);
    }


}
