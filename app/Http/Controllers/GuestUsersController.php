<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

use App\Http\Requests;

use Acme\GlobalCtrlHelper;
use App\Applied_job;
use App\User;
use App\User_group;
use App\Resume;
use App\Job_post;
use App\Company_profile;
use App\Email_task;
use Validator;
use Auth;
use Config;
use Stripe\Stripe;
use Stripe\Plan;
use Stripe\Coupon;
use Response;

class GuestUsersController extends Controller
{
    public function downloadResumeDocumentFile($fullname, $resume_id) {
        $resume = Resume::find($resume_id);
        if ($resume !== null) {
            $pathToFile = public_path() . '/' . $resume->resume_document;
            return response()->download($pathToFile);
        } 
    }

    // /fullname/resume/id  
    public function resumeDetails($fullname, $resume_id) {
        $resume = Resume::find($resume_id);
        if ($resume !== null) {
            $resume = $resume->toArray();
            $resume['personal_core_skills'] = json_decode(unserialize($resume['personal_core_skills']), true);
            $resume['technical_core_skills'] = json_decode(unserialize($resume['technical_core_skills']), true);
            $resume['hobbies'] = json_decode(unserialize($resume['hobbies']), true);
            $resume['educations'] = json_decode(unserialize($resume['educations']), true);
            $resume['experiences'] = json_decode(unserialize($resume['experiences']), true);
            $resume['cover_photo_selection'] = asset('img/option-' . $resume['cover_photo_selection'] . '.jpg');
            $resume['download_url'] = asset("/$fullname/resume/$resume_id/download");
            return view('guest.resume-details', compact('resume'));
        } 
    }

    public function jobsListing($company_name_slug) {
        $company = Company_profile::where('company_name_slug', $company_name_slug)->first();
        if ($company !== null) {
            $company = $company->toArray();
            $company['company_perks'] = json_decode(unserialize($company['company_perks']), true);
            foreach ($company['company_perks'] as $key => $value) {
                $value[1] = str_replace("\n", "<br/>", $value[1]);
                $company['company_perks'][$key] = $value;
            }
         
            $company['social_networks'] = json_decode(unserialize($company['social_networks']), true);
            $company['company_description'] = json_decode(unserialize($company['company_description']), true);
            
            $company['company_logo'] = ($company['company_logo'] === '') ? '' : asset($company['company_logo']);
            $company['company_icon'] = ($company['company_icon'] === '') ? '' : asset($company['company_icon']);
            $company['job_link'] = asset($company['company_name_slug']);
            

            $company['meter'] = array(
                'junior' => asset('img/meter1.png'),
                'mid' => asset('img/meter2.png'),
                'senior' => asset('img/meter3.png'),
                'manager' => asset('img/meter4.png')
            );
          
            $jobs = Job_post::where('user_id' , $company['user_id'])->get();
            if ($jobs === null) {
                $jobs = [];
            } else {
                $jobs = $jobs->toArray(); 
                foreach ($jobs as $key => $job) {
                    $jobs[$key]['job_description'] = unserialize($job['job_description']);
                    $job['skills'] = unserialize($job['skills']);
                    usort($job['skills'], array($this, 'compare_level'));
                    $jobs[$key]['skills'] = $job['skills'];
                    $jobs[$key]['start_date'] = GlobalCtrlHelper::time_elapsed_string(strtotime($jobs[$key]['start_date']));
                }  
            }

            return view('guest.job-listing', compact('company', 'jobs'));    
        }
    }

    private function compare_level($a, $b) {
        return strnatcmp($b['level'], $a['level']);
    }

    // /company-name/jobtitle/id
    public function jobDetails($company_name_slug, $job_title, $job_id) {
        $user_type = null;
        if (Auth::check()) 
            $user_type = Auth::user()->userGroup()->get()->toArray()[0]['associated_access'];

        $if_authenticated = Auth::check();

        $company = Company_profile::where('company_name_slug', $company_name_slug)->first();
        if ($company !== null) {
            $company = $company->toArray();
            $company['company_perks'] = json_decode(unserialize($company['company_perks']), true);
            foreach ($company['company_perks'] as $key => $value) {
                $value[1] = str_replace("\n", "<br/>", $value[1]);
                $company['company_perks'][$key] = $value;
            }
            $company['social_networks'] = json_decode(unserialize($company['social_networks']), true);
            
            $company['company_description'] = unserialize($company['company_description']);
          
            $company['company_logo'] = ($company['company_logo'] === '') ? '' : asset($company['company_logo']);
            $company['company_icon'] = ($company['company_icon'] === '') ? '' : asset($company['company_icon']);
            $company['job_link'] = asset($company['company_name_slug']);
            $company['meter'] = array(
                'junior' => asset('img/meter1.png'),
                'mid' => asset('img/meter2.png'),
                'senior' => asset('img/meter3.png'),
                'manager' => asset('img/meter4.png')
            );
            
            $job = Job_post::find($job_id);
            if ($job === null) {
                $job = [];
            } else {
                $job = $job->toArray(); 
                $job['job_description'] = unserialize($job['job_description']);
                $job['skills'] = unserialize($job['skills']);
                usort($job['skills'], array($this, 'compare_level'));
                $job['start_date'] = date("F j, Y", strtotime($job['start_date']));
                $job_url = asset($company_name_slug . '/' . $job_title . '/' . $job_id);
                $network = array('fa fa-facebook', 'fa fa-twitter', 'fa fa-google-plus', 'fa fa-linkedin', 
                                 'fa fa-pinterest-p');
                $shareLinks = [];
                foreach ($company['social_networks'] as $key => $value) {
                    if ($value[0]['value'] === $network[0]) {
                        $value[]['sharelink'] = 'http://www.facebook.com/sharer/sharer.php?u=' . $job_url;    
                    } else if ($value[0]['value'] === $network[1]) {
                        $value[]['sharelink'] = 'https://twitter.com/share?url=' . $job_url . '&text=' . $job['job_title']; //. '&via=[via]&hashtags=[hashtags]'
                    } else if ($value[0]['value'] === $network[2]) {
                        $value[]['sharelink'] = 'https://plus.google.com/share?url=' . $job_url;    
                    } else if ($value[0]['value'] === $network[3]) {
                        $value[]['sharelink'] = 'http://www.linkedin.com/shareArticle?url=' . $job_url . '&title=' . $job['job_title'];
                    } else if ($value[0]['value'] === $network[4]) {
                        $value[]['sharelink'] = 'https://pinterest.com/pin/create/bookmarklet/?media=' .$company['company_logo']. '&url=' . $job_url . '&is_video=' . $company['company_video'] . '&description=' . $job['job_description'];
                    }
                    $company['social_networks'][$key] = $value;
                }
            }
            // experience_level
            return view('guest.job-details', compact('company', 'job', 'if_authenticated', 'user_type', 
                    'company_name_slug', 'job_title', 'job_id'));    
        }
    }

    private function sendEmailJobApply(array $data = []) {

        $data['template'] = 'job-apply';
        (new Email_task([
            'data' => serialize($data)
        ]))->save();

    }

    public function jobApply(Request $request) {
        $params = $request->all();
        $params['is_authenticated'] = Auth::check();

        $rules = [
            'firstname' => 'min:3|max:255',
            'lastname' => 'min:3|max:255',
            'email' => 'email',
            'linkedin_profile' => '',
            'cover_leter' => 'min:255',
            'curriculum_vitae' => '',
            'jobs_id' => 'required',
            'user_id' => ''
        ];
        if ($params['is_authenticated']) {

            $resume = Resume::where('user_id', Auth::user()->id)->first();
          
            $params['user_id'] = Auth::user()->id; 
            $params['firstname'] = Auth::user()->firstname;
            $params['lastname'] = Auth::user()->lastname;
            $params['email'] = Auth::user()->email;
            $params['linkedin_profile'] = asset($resume->full_name . '/resume/' . $resume->id);
            $params['curriculum_vitae'] = $resume->resume_document;
        } else {
            $params['user_id'] = '-1';
    
            $rules['firstname'] = 'required|min:3|max:255';
            $rules['lastname'] = 'required|min:3|max:255';
            $rules['email'] = 'required|email';
            $rules['linkedin_profile'] = 'required';
            $rules['curriculum_vitae'] = 'required';
        }
        if (!isset($params['cover_leter'])) {
            $params['cover_leter'] = '';
        }
       
        $validator = Validator::make($params , $rules);
        
        if($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }
        if ($request->hasFile('curriculum_vitae')) {
            $ext = $request->file('curriculum_vitae')->getClientOriginalExtension();
            $params['curriculum_vitae'] = 
                $request->file('curriculum_vitae')->move('uploads/curriculum_vitae/', User_group::MakeAttributeId() . '.' . $ext);
        } else {
            $params['curriculum_vitae'] = '';
        }

        $user_group = User_group::where('name', strtolower('administrator'))->first();
        if ($user_group !== null) {
            $params['admin_email'] = $user_group->userDetails()->first()->email;
        }

        $this->sendEmailJobApply($params);

        (new Applied_job([
            'firstname' => $params['firstname'],
            'lastname' => $params['lastname'],
            'email' => $params['email'],
            'linkedin_profile' => $params['linkedin_profile'],
            'cover_leter' => $params['cover_leter'],
            'curriculum_vitae' => $params['curriculum_vitae'],
            'jobs_id' => $params['jobs_id'],
            'user_id' => $params['user_id']
        ]))->save();
        $request->session()->flash('alert-success', 'Job was applied successful.');
        return redirect($params['company_name_slug'] . '/' . $params['job_title'] . '/' . $params['jobs_id']);
    }
}

