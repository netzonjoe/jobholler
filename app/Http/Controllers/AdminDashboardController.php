<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Validator;
use Auth;
use App\Google_analytic;
use App\Company_profile;
use App\Payment_gateway;
use App\Notification;
use App\User_group;
use App\Branding;
use App\Product;
use App\Resume;
use App\User;
use App\Job_post;
use App\Email_task;
use Route;
use DB;
use Acme\Rest;
use Acme\GlobalCtrlHelper;
use Carbon\Carbon;
use Stripe\Stripe;
use Stripe\Plan;
use Stripe\Coupon;
use Config;
use Exception;
use File;
use Mail;

class AdminDashboardController extends Controller
{   

   	protected function getAllUsersWithUserGroup() {
        $employers = DB::table('user_groups')
            ->where('user_groups.associated_access', 'employer')
            ->join('users', 'users.user_group_id', '=', 'user_groups.id')
            ->select('users.id', 'users.user_group_id', 'users.firstname', 'users.lastname', 'users.phone', 'users.email', 'users.status', DB::raw('DATE_FORMAT(users.created_at, \'%M %d, %Y\') as created_at'), 
                'user_groups.name','user_groups.associated_access' )
            ->orderBy('users.created_at', 'desc')
            ->get();
        $employers = json_decode(json_encode($employers), true);

        $candidates = DB::table('user_groups')
            ->where('user_groups.associated_access', 'candidate')
            ->join('users', 'users.user_group_id', '=', 'user_groups.id')
            ->select('users.id', 'users.user_group_id', 'users.firstname', 'users.lastname', 'users.phone', 'users.email', 'users.status', DB::raw('DATE_FORMAT(users.created_at, \'%M %d, %Y\') as created_at'), 
                'user_groups.name','user_groups.associated_access')
            ->orderBy('users.created_at', 'desc')
            ->get();
        $candidates = json_decode(json_encode($candidates), true);

        $users = DB::table('user_groups')
            ->whereNotIn('user_groups.associated_access', ['none'])
            ->join('users', 'users.user_group_id', '=', 'user_groups.id')
            ->select('users.id', 'users.user_group_id', 'users.firstname', 'users.lastname', 'users.phone', 'users.email', 'users.status', DB::raw('DATE_FORMAT(users.created_at, \'%M %d, %Y\') as created_at'),
                'user_groups.name', 'user_groups.associated_access')
            ->orderBy('users.created_at', 'desc')
            ->get();
        $users = json_decode(json_encode($users), true);

        $start = (new Carbon('now'))->hour(0)->minute(0)->second(0);
        $end = (new Carbon('now'))->hour(23)->minute(59)->second(59);
        $new_members = DB::table('user_groups')
            ->whereBetween('users.created_at', [$start , $end])
            ->join('users', 'users.user_group_id', '=', 'user_groups.id')
            ->select('users.id', 'users.user_group_id', 'users.firstname', 'users.lastname', 'users.phone', 'users.email', 'users.status', DB::raw('DATE_FORMAT(users.created_at, \'%M %d, %Y\') as created_at'), 
                'user_groups.name','user_groups.associated_access')
            ->get();
        $new_members = json_decode(json_encode($new_members), true);

        return array('all' => $users, 'employers' => $employers , 'candidates' => $candidates, 'new_members' => $new_members);
    }

    protected function getAllPostedJobs() {
        $job_posted = [];
        $expired_date = new Carbon;
        $expired_date->subDays(7);

        $jobs = DB::table('job_posts')
            ->where('job_posts.start_date', '>' , $expired_date->toDateTimeString())
            ->join('company_profiles', 'job_posts.company_profile_id', '=', 'company_profiles.id')
            ->select('job_posts.*', 'company_profiles.company_name', 'company_profiles.company_name_slug')
            ->orderBy('job_posts.created_at', 'desc')
            ->get();
        $jobs = json_decode(json_encode($jobs), true);

        
        foreach ($jobs as $key => $value) {
            $value['skills'] = unserialize($value['skills']);
            $value['job_description'] = unserialize($value['job_description']);
            $job_posted[] = $value;
        }
        return $job_posted;
    }
    
    public function getAllPostedJobsAjax(Request $request) {
        if ($request->ajax())
        {   
            $jobs = $this->getAllPostedJobs();
            return Rest::success(['$jobs' => $jobs]);
        } 
    }
    
    protected function getAllExpiredJobs() {
        $job_expired = [];
        $expired_date = new Carbon;
        $expired_date->subDays(7);
        $jobs = DB::table('job_posts')
            ->where('job_posts.start_date', '<' , $expired_date->toDateTimeString())
            ->join('company_profiles', 'job_posts.company_profile_id', '=', 'company_profiles.id')
            ->select('job_posts.*', 'company_profiles.company_name', 'company_profiles.company_name_slug')
            ->orderBy('job_posts.created_at', 'desc')
            ->get();
        $jobs = json_decode(json_encode($jobs), true);
        
        foreach ($jobs as $key => $value) {
            $value['skills'] = unserialize($value['skills']);
            $value['job_description'] = unserialize($value['job_description']);
            $job_expired[] = $value;
        }
        return $job_expired;
    }

    protected function getAllPostedProducts() {
        return User_group::whereNotIn('name', ['Administrator'])->orderBy('created_at', 'desc')->get()->toArray();
    }

    public function getUserGroups() {
        $user_groups_by_name = User_group::where('associated_access', '!=' , 'none')->groupBy('name')->get()->toArray();
        $user_groups = [];
        foreach ($user_groups_by_name as $key => $value) {
            $user_groups_obj = User_group::where([
                'name' => $value['name']
            ])->get()->toArray();
            foreach ($user_groups_obj as $ke => $val) {
                $user_groups[] = $val;
            }
        }
        return Rest::success(['$user_groups' => $user_groups]);
    }

    protected function getUsersWithoutResume() {
        $users = DB::table('users')
            ->where('user_groups.associated_access', 'candidate')
            ->where('resumes.user_id', null)
            ->join('user_groups', 'users.user_group_id', '=', 'user_groups.id')
            ->leftJoin('resumes', 'users.id', '=', 'resumes.user_id')
            ->select('users.id', 'users.user_group_id', 'users.firstname', 'users.lastname', 'users.phone', 'users.email', DB::raw('DATE_FORMAT(users.created_at, \'%M %d, %Y\') as created_at'), 
                'user_groups.name','user_groups.associated_access', 'resumes.status', 'resumes.viewable', 'resumes.full_name', 'resumes.tagline', 'resumes.about_me', 'resumes.profile_photo', 'resumes.years_of_experience', 'resumes.expected_salary', 'resumes.personal_core_skills', 'resumes.technical_core_skills', 'resumes.hobbies', 'resumes.educations', 'resumes.experiences', 'resumes.resume_document', 'resumes.cover_photo', 'resumes.cover_photo_selection', 'resumes.user_id')
            ->orderBy('users.firstname', 'asc')
            ->get();
        $users = json_decode(json_encode($users), true);
        return $users;
    }

    public function getUsersWithoutResumeAjax() {
        return Rest::success(['$users_without_resume' => $this->getUsersWithoutResume()]);
    }

    protected function getUsersWithResume() {
        $users = DB::table('users')
            ->where('user_groups.associated_access', 'candidate')
            ->join('user_groups', 'users.user_group_id', '=', 'user_groups.id')
            ->join('resumes', 'users.id', '=', 'resumes.user_id')
            ->select('users.user_group_id', 'users.firstname', 'users.lastname', 'users.phone', 'users.email', DB::raw('DATE_FORMAT(users.created_at, \'%M %d, %Y\') as created_at'), 
                'user_groups.name','user_groups.associated_access', 'resumes.status', 'resumes.viewable', 'resumes.full_name', 'resumes.tagline', 'resumes.about_me', 'resumes.profile_photo', 'resumes.years_of_experience', 'resumes.expected_salary', 'resumes.personal_core_skills', 'resumes.technical_core_skills', 'resumes.hobbies', 'resumes.educations', 'resumes.experiences', 'resumes.resume_document', 'resumes.cover_photo', 'resumes.cover_photo_selection', 'resumes.user_id', 'resumes.id')
            ->orderBy('users.firstname', 'asc')
            ->get();

        $usersWithResume = [];
        foreach ($users as $key => $value) {
            $value->personal_core_skills = json_decode(unserialize($value->personal_core_skills));
            $value->technical_core_skills = json_decode(unserialize($value->technical_core_skills));
            $value->hobbies = json_decode(unserialize($value->hobbies));
            $value->educations = json_decode(unserialize($value->educations));
            $value->experiences = json_decode(unserialize($value->experiences));
            $usersWithResume[] = $value;
        }

        $usersWithResume = json_decode(json_encode($usersWithResume), true);
        return $usersWithResume;
    }


    public function getUsersWithResumeAjax() {
        return Rest::success(['$users_with_resume' => $this->getUsersWithResume()]);
    }

   	public function index()
   	{	
        $_token = csrf_token();
   		$user_groups_by_name = User_group::where('associated_access', '!=' , 'none')->groupBy('name')->get(['id', 'name', 'associated_access'])->toArray();
        $user_groups = [];
        foreach ($user_groups_by_name as $key => $value) {
            $user_groups_obj = User_group::where([
                'name' => $value['name']
            ])->get(['id', 'name', 'associated_access'])->toArray();
            foreach ($user_groups_obj as $ke => $val) {
                $user_groups[] = $val;
            }
        }
        $user_groups = json_encode($user_groups);

        $users = json_encode($this->getAllUsersWithUserGroup());

        $jobs = json_encode($this->getAllPostedJobs());
        $route_list = json_encode(GlobalCtrlHelper::getRouteList('AdminDashboardController'));
     
        $job_listing = asset('/job-listing');
        $resume_listing = asset('/resume-listing');
        $imagesDir = asset('/img');
        $baseurl = asset('');
        $products = json_encode($this->getAllPostedProducts());

        $expired_jobs = json_encode($this->getAllExpiredJobs());
        $users_with_resume = json_encode($this->getUsersWithResume());
        $users_without_resume = json_encode($this->getUsersWithoutResume());
        $google_analytic = $this->getGoogleAnalyticsCode();
        $angular_directive_alert = GlobalCtrlHelper::getAngularDirectiveAlert();
        $pusher_key = Config::get('services.pusher.key');
   		return view('admin_dashboard.index', compact('user_groups', 'users', 'route_list', '_token', 'jobs', 
            'job_listing', 'resume_listing', 'products', 'imagesDir', 'users_without_resume', 'users_with_resume', 
            'baseurl', 'google_analytic', 'expired_jobs', 'angular_directive_alert', 'pusher_key'));
   	}

    public function getAllUsersWithUserGroupAjax(Request $request)
    {   
        if ($request->ajax())
        {   
            $users = $this->getAllUsersWithUserGroup();
            return Rest::success(['$users' => $users]);
        }
    }
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function userValidator(array $data, $option = [])
    {
        $fields = [
            'user_group_id' => 'required',
            'firstname' => 'required|max:255',
            'lastname' => 'required|max:255',
            'phone'    => 'min:10|max:10',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6',
        ];

        return Validator::make($data, $fields + $option);
    }   

    protected function userUpdateValidator(array $data, $option = [])
    {
        $fields = [
            'user_group_id' => 'required',
            'firstname' => 'required|max:255',
            'lastname' => 'required|max:255',
            'phone'    => 'required|min:10|max:10',
            'email' => 'required|email|max:255',
            'password' => 'min:6'
        ];

        return Validator::make($data, $fields + $option);
    }  

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function userCreate(array $data)
    {   
        $user_group = User_group::find($data['user_group_id']);
        return $user_group->userDetails()->save(new User([
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'phone' => (isset($data['phone'])) ? $data['phone'] : '',
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'address' => (isset($data['address'])) ? $data['address'] : '',
            'city' => (isset($data['city'])) ? $data['city'] : '',
            'state'    => (isset($data['state'])) ? $data['state'] : '',
            'zip' => (isset($data['zip'])) ? $data['zip'] : '',
            'country' => (isset($data['country'])) ? $data['country'] : ''
        ]));
    }

    public function createMember(Request $request)
    {   
        if ($request->ajax())
        {   
            $validator = $this->userValidator($request->all());
            if($validator->fails()) {
                return Rest::failed($validator);
            }
            $this->userCreate($request->all());
            $this->sendEmailCreateMember($request->all());
            $users = $this->getAllUsersWithUserGroup();
            return Rest::success(['$users' => $users, 'msg' => 'Member information was created successfully']);
        } 

    }
    
    private function sendEmailCreateMember(array $data = []) {

        $data['template'] = 'create-member';
        (new Email_task([
            'data' => serialize($data)
        ]))->save();
    }

    public function updateMemberStatus(Request $request)
    {
        if ($request->ajax()) { 
            $params = $request->all();
            $user = User::find($params['id']);
            $user->status = ($params['status'] == 1) ? 0 : 1;
            $user->save();
            $users = $this->getAllUsersWithUserGroup();
            return Rest::success(['$users' => $users, 'msg' => 'Member status was updated successfully']);
        }
    }

    protected function jobValidator(array $data, $option = []) {
        $fields = [
            'job_title' => 'required|max:255',
            'job_type' => 'required',
            'maximum_salary' => 'required|numeric|min:1',
            'paid_per' => 'required',
            'job_location' => 'required|max:255',
            'job_description' => 'required|min:1',
            'skills' => 'required',
            'experience_level' => 'required',
            'start_date' => 'required',
            'contact_name' => 'min:3|max:255',
            'contact_number' => 'min:10|max:10',
            'company_name' => 'required',
            'user_id' => 'required'
        ];
        return Validator::make($data , $fields + $option);
    }

    protected function jobCreate(array $data) {
        $date = explode("-", $data['start_date']);
        return (new Job_post([
            'job_title_slug' => $data['job_title_slug'],
            'job_title' => $data['job_title'],
            'job_type' => $data['job_type'],
            'minimum_salary' => $data['minimum_salary'],
            'maximum_salary' => $data['maximum_salary'],
            'paid_per' => $data['paid_per'],
            'job_location' => $data['job_location'],
            'job_description' => serialize($data['job_description']),
            'skills' => serialize($data['skills']),
            'experience_level' => $data['experience_level'],
            'start_date' => Carbon::createFromDate($date[0],$date[1],$date[2])->toDateTimeString() ,
            'contact_name' => $data['contact_name'],
            'contact_number' => $data['contact_number'],
            'reference' => $data['reference'],
            'company_profile_id' => $data['company_name'],
            'user_id' => $data['user_id']
        ]))->save();
    }

    protected function jobUpdate(array $data) {

        $job = Job_post::find($data['id']);
        $job->job_title_slug = $data['job_title_slug'];
        $job->job_title = $data['job_title'];
        $job->job_type = $data['job_type'];
        $job->minimum_salary = $data['minimum_salary'];
        $job->maximum_salary = $data['maximum_salary'];
        $job->paid_per = $data['paid_per'];
        $job->job_location = $data['job_location']; 
        $job->job_description = serialize($data['job_description']); 
        $job->skills = serialize($data['skills']);
        $job->experience_level = $data['experience_level'];
        $job->start_date = $data['start_date'];
        $job->contact_name = $data['contact_name'];
        $job->contact_number = $data['contact_number'];
        $job->company_profile_id = $data['company_name'];
        $job->save();
    }

     protected function userUpdate(array $data)
    {   

        $allowUpdate = false;        
        $ifEmailExist = User::where('email' , $data['email'])->first();
        if ($ifEmailExist !== null) {
            if ($data['id'] == $ifEmailExist->id) {
                $allowUpdate = true;
            }
        } else {
            $allowUpdate = true;
        }
        
        if ($allowUpdate) {
            $user = User::find($data['id']);
            $user->user_group_id = $data['user_group_id'];
            $user->firstname = $data['firstname'];
            $user->lastname = $data['lastname'];
            $user->phone = (isset($data['phone'])) ? $data['phone'] : '';
            $user->email = $data['email'];
            if (isset($data['password'])) 
                $user->password = bcrypt($data['password']);    
            $user->address = (isset($data['address'])) ? $data['address'] : '';
            $user->city = (isset($data['city'])) ? $data['city'] : '';
            $user->state = (isset($data['state'])) ? $data['state'] : '';
            $user->zip = (isset($data['zip'])) ? $data['zip'] : '';
            $user->country = (isset($data['country'])) ? $data['country'] : '';
            $user->save();
        }
        return $allowUpdate;
    }
    
    public function updateMember(Request $request) {
        if ($request->ajax())
        {   
            $validator = $this->userUpdateValidator($request->all());
            if($validator->fails()) {
                return Rest::failed($validator);
            }
            if (!$this->userUpdate($request->all())) {
                return Rest::dataExist(['email' => ['Email already exist.']]);
            }
            $this->sendEmailUpdateMember($request->all());
            $users = $this->getAllUsersWithUserGroup();

            return Rest::success(['$users' => $users, 'msg' => 'Member information was updated successfully']);
        } 
    }
    
    private function sendEmailUpdateMember(array $data = []) {
        $data['template'] = 'update-member';
        (new Email_task([
            'data' => serialize($data)
        ]))->save();
    }

    public function deleteMember(Request $request) {
        if ($request->ajax())
        {   
            User::destroy($request->get('id'));
            $users = $this->getAllUsersWithUserGroup();
            return Rest::success(['$users' => $users, 'msg' => 'Member information was deleted successfully']);
        } 
    }

    public function getAllCompaniesAjax(Request $request) {
        if ($request->ajax())
        {   
            return Rest::success(['$registered_companies' => Company_profile::all()->toArray()]);
        } 
    }

    public function getAllExpiredJobsAjax(Request $request) {
        if ($request->ajax())
        {   
            $expired_jobs = $this->getAllExpiredJobs();
            return Rest::success(['$expired_jobs' => $expired_jobs]);
        } 
    }
    private function sendEmailJobPosting(array $data = []) {
        $user_group = User_group::where('name', strtolower('administrator'))->first();
        if ($user_group !== null) {
            $data['admin_email'] = $user_group->userDetails()->first()->email;
            $data['user_id'] = $user_group->userDetails()->first()->id;
            $data['from_user_id'] = Auth::user()->id;
            $data['from_user_email'] = Auth::user()->email;
            $data['template'] = 'job-posting';
            (new Email_task([
                'data' => serialize($data)
            ]))->save();
        }
    }
    public function createJob(Request $request)
    {   
        if ($request->ajax())
        {   
            $params = $request->all();

            $params['company_profile_id'] = $params['company_name'];

            $params['user_id'] = Auth::user()->id;

            $additional_options = ['minimum_salary' => 'numeric|min:1', 'reference' => 'required|unique:job_posts'];
            if (is_numeric($params['minimum_salary'])) {
                if ($params['minimum_salary'] + 0 == 0) {
                    $params['minimum_salary'] = 'NaN';       
                }
            }
            if($params['minimum_salary'] === 'NaN') {
                $params['minimum_salary'] = '0.00';
                $additional_options['minimum_salary'] = 'numeric|min:0';
            }
            $validator = $this->jobValidator($params, $additional_options);
            if($validator->fails()) {
                return Rest::failed($validator);
            }

            $this->jobCreate($params);
            $profile = Company_profile::find($params['company_name'])->first();
            $params['company_name'] = $profile->company_name;
            $this->sendEmailJobPosting($params);

            $jobs = $this->getAllPostedJobs();
            return Rest::success(['$jobs' => $jobs, 'msg' => 'Job information was created successfully']);
        } 

    }

    public function cloneJob(Request $request) {
        if ($request->ajax())
        {   

            $params = $request->all();
         
            $params['company_name'] = $params['company_profile_id'];

            $params['user_id'] = Auth::user()->id;
        
            $additional_options = ['minimum_salary' => 'numeric|min:1', 'reference' => 'required|unique:job_posts'];
            if (is_numeric($params['minimum_salary'])) {
                if ($params['minimum_salary'] + 0 == 0) {
                    $params['minimum_salary'] = 'NaN';       
                }
            }
            if($params['minimum_salary'] === 'NaN') {
                $params['minimum_salary'] = '0.00';
                $additional_options['minimum_salary'] = 'numeric|min:0';
            }
            $validator = $this->jobValidator($params, $additional_options);
            if($validator->fails()) {
                return Rest::warning(['msg' => 'Network error. Please try again']);
            }
            $this->jobCreate($params);
            $jobs = $this->getAllPostedJobs();
            return Rest::success(['$jobs' => $jobs, 'msg' => 'Job information was cloned successfully']);
        } 
    }

    public function deleteJob(Request $request) {
        if ($request->ajax())
        {   
            Job_post::destroy($request->get('id'));
            $jobs = $this->getAllPostedJobs();
            return Rest::success(['$jobs' => $jobs, 'msg' => 'Job information was deleted successfully']);
        } 
    }

    public function updateJob(Request $request) {
        if ($request->ajax())
        {   
            $params = $request->all();  
            $additional_options = ['minimum_salary' => 'numeric|min:1', 'reference' => 'required'];
            if (is_numeric($params['minimum_salary'])) {
                if ($params['minimum_salary'] + 0 == 0) {
                    $params['minimum_salary'] = 'NaN';       
                }
            }
            if($params['minimum_salary'] === 'NaN') {
                $params['minimum_salary'] = '0.00';
                $additional_options['minimum_salary'] = 'numeric|min:0';
            }
            $validator = $this->jobValidator($params, $additional_options);
            if($validator->fails()) {
                return Rest::failed($validator);
            }

            $allowUpdate = false;        
            $ifExist = Job_post::where('reference' , $params['reference'])->first();
            if ($ifExist !== null) {
                if ($params['id'] == $ifExist->id) {
                    $allowUpdate = true;
                }
            } else {
                $allowUpdate = true;
            }
            
            if ($allowUpdate) {
                $this->jobUpdate($params);
            } else {
                return Rest::dataExist(['reference' => ['The reference has already been taken.']]);
            }

            $jobs = $this->getAllPostedJobs();
            return Rest::success(['$jobs' => $jobs, 'msg' => 'Job information was updated successfully']);
        } 
    }

    public function repostJob(Request $request) {
        if ($request->ajax())
        {   
            $params = $request->all();

            $params['start_date'] = (new Carbon)->toDateTimeString();

            $this->jobUpdate($params);

            $expired_jobs = $this->getAllExpiredJobs();
            $jobs = $this->getAllPostedJobs();
            return Rest::success(['$expired_jobs' => $expired_jobs, '$jobs' => $jobs, 'msg' => 'Job information was reposted successfully']);
        } 
    }

    public function productCreate(array $data) {
        if ($data['payment_plan_type']=== 'subscription' || $data['payment_plan_type']=== 'one_time_payment') {
            Stripe::setApiKey(Payment_gateway::getPaymentKey('stripe', 'secret'));
            try {
                $plan = Plan::retrieve($data['name']);
                $plan->delete();
            } catch (Exception $e) {
                // Can either succeed or 404, all other errors are bad
            }
            try {
                Plan::create(array(
                    'amount' =>  round(floatval($data['price']) * 1.25),
                    'interval' => 'month',
                    'currency' => 'gbp',
                    'name' => $data['name'],
                    'id' => $data['name'],
                    'metadata' => [
                        'name' => $data['name'],
                        'status' => $data['status'],
                        'sku' => $data['sku'],
                        'associated_access' => $data['associated_access'],
                        'payment_plan_type' => $data['payment_plan_type'],
                        'price' => $data['price'],
                        'description' => $data['description'],
                        'attributes' => $data['attributes'],
                        'purchase_link' => $data['purchase_link']
                    ]
                ));  
            } catch (Exception $e) {
                // Can either succeed or 404, all other errors are bad
            }
        }

        $product = new User_group([
            'name' => $data['name'],
            'status' => $data['status'],
            'sku' => $data['sku'],
            'associated_access' => $data['associated_access'],
            'price' => $data['price'],
            'payment_plan_type' => $data['payment_plan_type'],
            'description' => $data['description'],
            'attributes' => $data['attributes'],
            'purchase_link' => $data['purchase_link']
        ]);

        return $product->save();
    }

    protected function productUpdate(array $data) {
        if ($data['payment_plan_type']=== 'subscription' || $data['payment_plan_type']=== 'one_time_payment') {

            Stripe::setApiKey(Payment_gateway::getPaymentKey('stripe', 'secret'));
            try {
                $plan = Plan::retrieve($data['stripe_name']);
                $plan->delete();
            } catch (Exception $e) {
                // Can either succeed or 404, all other errors are bad
            }
            try {
                Plan::create(array(
                    'amount' =>  round(floatval($data['price']) * 1.25),
                    'interval' => 'month',
                    'currency' => 'gbp',
                    'name' => $data['name'],
                    'id' => $data['name'],
                    'metadata' => [
                        'name' => $data['name'],
                        'status' => $data['status'],
                        'sku' => $data['sku'],
                        'associated_access' => $data['associated_access'],
                        'payment_plan_type' => $data['payment_plan_type'],
                        'price' => $data['price'],
                        'description' => $data['description'],
                        'attributes' => $data['attributes'],
                        'purchase_link' => $data['purchase_link']
                    ]
                ));  
            } catch (Exception $e) {
                // Can either succeed or 404, all other errors are bad
            }
        }

        $allowUpdate = false;        
        $ifEmailExist = User_group::where('name' , $data['name'])->first();
        if ($ifEmailExist !== null) {
            if ($data['id'] == $ifEmailExist->id) {
                $allowUpdate = true;
            }
        } else {
            $allowUpdate = true;
        }
        
        if ($allowUpdate) {
            $product = User_group::find($data['id']);
            $product->name = $data['name'];
            $product->status = $data['status'];
            $product->sku = $data['sku'];
            $product->associated_access = $data['associated_access'];
            $product->price = $data['price'];
            $product->payment_plan_type = $data['payment_plan_type'];
            $product->description = $data['description'];
            $product->attributes = $data['attributes'];
            $product->purchase_link = $data['purchase_link'];
            $product->save();
        } else {
            return Rest::dataExist(['name' => ['The name has already been taken.']]);
        }
    }
    
    protected function productValidator(array $data, $option = []) {
        $fields = [
            'status' => 'required',
            'sku' => 'required|min:1|max:255',
            'associated_access' => 'required',
            'payment_plan_type' => 'required',
            'description' => 'required',
            'attributes' => 'required',
            'purchase_link' => 'required'
        ];

        return Validator::make($data, $fields + $option);
    }

    public function createProduct(Request $request)
    {   
        if ($request->ajax())
        {   

            $params = $request->all();

            $params['purchase_link'] = 'purchase_link';
            $params['attributes'] = User_group::MakeAttributeId();
            $additional = [
                'price' => 'required|numeric|min:1',
                'name' => 'required|max:255|unique:user_groups',
            ];

            if ($params['payment_plan_type'] === 'trial') {
                $additional['price'] = 'required|numeric|min:0';
                $params['price'] = '0.00';
            }

            $validator = $this->productValidator($params, $additional);
            if($validator->fails()) {
                return Rest::failed($validator);
            }

            $this->productCreate($params); 
            $products = $this->getAllPostedProducts();
            return Rest::success(['$products' => $products, 'msg' => 'Product information was created successfully']);
        } 

    }

    public function deleteProduct(Request $request) {
        if ($request->ajax())
        {   
            Stripe::setApiKey(Payment_gateway::getPaymentKey('stripe', 'secret'));
            try {
                $plan = Plan::retrieve($request->get('name'));
                $plan->delete();
            } catch (Exception $e) {
                // Can either succeed or 404, all other errors are bad
            }

            $product = User_group::destroy($request->get('id'));
            $products = $this->getAllPostedProducts();
            return Rest::success(['$products' => $products, 'result' => $product, 'msg' => 'Product information was deleted successfully']);
        } 
    }

    public function updateProduct(Request $request) {
        if ($request->ajax())
        {   
            $params = $request->all();

            $additional = [
                'price' => 'required|numeric|min:1',
                'name' => 'required|max:255',
            ];
            if ($params['payment_plan_type'] === 'trial') {
                $params['price'] = '0.00';
            }
            $validator = $this->productValidator($params, $additional);

            if($validator->fails()) {
                return Rest::failed($validator);
            }

            $this->productUpdate($params);

            $products = $this->getAllPostedProducts();
            return Rest::success(['$products' => $products, 'msg' => 'Product information was updated successfully']);
        } 
    }

    public function getBillingCredentials(Request $request) {
        if ($request->ajax()) { 
            $stripe = Payment_gateway::where('name', 'stripe')->first();
            return Rest::success(['$stripe' => $stripe]);
        }
    }

    public function saveBillingCredentials(Request $request) {
        if ($request->ajax()) { 
            $stripe = Payment_gateway::where('name', 'stripe')->first();
            $params = $request->all();
            if ($stripe === null) {
                $stripe = new Payment_gateway([
                    'name' => 'stripe',
                    'test_key' => $params['test_key'],
                    'test_secret' => $params['test_secret'],
                    'live_key' => $params['live_key'],
                    'live_secret' => $params['live_secret']
                ]);
            } else {
                $stripe->name = 'stripe';
                $stripe->test_key = $params['test_key'];
                $stripe->test_secret = $params['test_secret'];
                $stripe->live_key = $params['live_key'];
                $stripe->live_secret = $params['live_secret'];
            }

            $stripe->save();
            return Rest::success(['$stripe' => $stripe, 'msg' => 'Billing Credential information was saved successfully']);
        }
    }


    protected function adminProfileValidator(array $data, $option = [])
    {
        $fields = [
            'user_group_id' => 'required',
            'firstname' => 'required|max:255',
            'lastname' => 'required|max:255',
            'email' => 'required|email|max:255',
            'password' => 'min:6'
        ];

        return Validator::make($data, $fields + $option);
    }  

    protected function getAdminProfile()
    {
        return DB::table('users')
            ->where('user_groups.associated_access', 'none')
            ->where('user_groups.name', 'ADMINISTRATOR')
            ->where('users.id', Auth::user()->id)
            ->join('user_groups', 'users.user_group_id', '=', 'user_groups.id')
            ->select('users.id', 'users.user_group_id', 'users.firstname', 'users.lastname', 'users.phone', 'users.email', 'users.status', DB::raw('DATE_FORMAT(users.created_at, \'%M %d, %Y\') as created_at'), 
                'user_groups.name', 'user_groups.status', 'user_groups.sku', 'user_groups.associated_access', 
                'user_groups.price', 'user_groups.payment_plan_type', 'user_groups.description', 'user_groups.attributes', 
                'user_groups.purchase_link' )
            ->orderBy('users.created_at', 'desc')
            ->get()[0];
    }  
    

    protected function getAdminProfileDetails(Request $request)
    {
        if ($request->ajax()) { 
            return Rest::success(['$admin' => $this->getAdminProfile()]);
        }
    }  
    
    public function updateAdminProfile(Request $request) {
        if ($request->ajax())
        {   
            $validator = $this->adminProfileValidator($request->all());
            if($validator->fails()) {
                return Rest::failed($validator);
            }
            if (!$this->userUpdate($request->all())) {
                return Rest::dataExist(['email' => ['Email already exist.']]);
            }

            return Rest::success(['$admin' => $this->getAdminProfile(), 'msg' => 'Admin Profile information was updated successfully']);
        } 
    }
    

    protected function resumeValidator(array $data, $option = []) {
        $fields = [
            'candidate_user' => 'required',
            'full_name' => 'required|min:1|max:255',
            'tagline' => 'required|min:1|max:255',
            'about_me' => 'required',
            'profile_photo' => 'required|mimes:jpeg,bmp,png',
            'cover_photo' => 'mimes:jpeg,bmp,png',
            'years_of_experience' => 'required',
            'expected_salary' => 'required'
        ];
        // 'user_id', 'full_name', 'tagline', 'about_me', 'profile_photo', 'years_of_experience', 'expected_salary', 'personal_core_skills', 'technical_core_skills', 'hobbies', 'educations', 'experiences', 'resume_document', 'cover_photo', 'cover_photo_selection'
        return Validator::make($data , $fields + $option);
    }

    protected function resumeUpdateValidator(array $data, $option = []) {
        $fields = [
            'candidate_user' => 'required',
            'full_name' => 'required|min:1|max:255',
            'tagline' => 'required|min:1|max:255',
            'about_me' => 'required',
            'years_of_experience' => 'required',
            'expected_salary' => 'required'
        ];
        return Validator::make($data, $fields + $option);
    }

    protected function resumeCreate(array $data) {
        $resume = new Resume([
            'user_id' => $data['candidate_user'] , 
            'full_name' => $data['full_name'] , 
            'tagline' => $data['tagline'] , 
            'about_me' => $data['about_me'] , 
            'profile_photo' => $data['profile_photo'] , 
            'years_of_experience' => $data['years_of_experience'] , 
            'expected_salary' => $data['expected_salary'] , 
            'personal_core_skills' => serialize($data['personal_core_skills']) , 
            'technical_core_skills' => serialize($data['technical_core_skills']) , 
            'hobbies' => serialize($data['hobbies']) , 
            'educations' => serialize($data['educations']) , 
            'experiences' => serialize($data['experiences']) , 
            'resume_document' => $data['resume_document'] , 
            'cover_photo' => $data['cover_photo'] , 
            'cover_photo_selection' => $data['cover_photo_selection'] 
        ]);
        return $resume->save();
    }

    public function createResume(Request $request) {
        if ($request->ajax()) { 
            $params = $request->all();

            $validator = $this->resumeValidator($params);
            if($validator->fails()) {
                return Rest::failed($validator);
            }

            if ($request->hasFile('profile_photo')) {
                $params['profile_photo'] = 
                    $request->file('profile_photo')->move('uploads/curriculum_vitae/profile_photo/', User_group::MakeAttributeId() . '.jpg');
            }

            if ($request->hasFile('cover_photo')) {
                $params['cover_photo'] = 
                    $request->file('cover_photo')->move('uploads/curriculum_vitae/cover_photo/', User_group::MakeAttributeId() . '.jpg');
            }

            if ($request->hasFile('resume_document')) {
                $extension = $request->file('resume_document')->getClientOriginalExtension();
                $params['resume_document'] = 
                    $request->file('resume_document')->move('uploads/curriculum_vitae/file/', User_group::MakeAttributeId() . '.' . $extension);
            }

            $this->resumeCreate($params); 

            $users_without_resume = $this->getUsersWithoutResume();
            $users_with_resume = $this->getUsersWithResume();

            return Rest::success(['$users_without_resume' => $users_without_resume, '$users_with_resume' => $users_with_resume, 'msg' => 'Resume information was created successfully']);
        }
    }

    public function updateResume(Request $request) {
        if ($request->ajax()) { 
            $params = $request->all();

            $validator = $this->resumeUpdateValidator($params);
            if($validator->fails()) {
                return Rest::failed($validator);
            }

            $resume = Resume::find($params['id']);

            if ($request->hasFile('profile_photo')) {
                $filename = public_path() . '/' . $resume->profile_photo;
                if (File::exists($filename)) {
                    File::delete($filename);
                } 
                $resume->profile_photo = 
                    $request->file('profile_photo')->move('uploads/curriculum_vitae/profile_photo/', User_group::MakeAttributeId() . '.jpg');
            }

            if ($request->hasFile('cover_photo')) {
                $filename = public_path() . '/' . $resume->cover_photo;
                if (File::exists($filename)) {
                    File::delete($filename);
                } 
                $resume->cover_photo = 
                    $request->file('cover_photo')->move('uploads/curriculum_vitae/cover_photo/', User_group::MakeAttributeId() . '.jpg');
            }

            if ($request->hasFile('resume_document')) {
                $filename = public_path() . '/' . $resume->resume_document;
                if (File::exists($filename)) {
                    File::delete($filename);
                } 
                $resume->resume_document = 
                    $request->file('resume_document')->move('uploads/curriculum_vitae/file/', User_group::MakeAttributeId() . '.jpg');
            }
            
            $resume->full_name = $params['full_name'];
            $resume->tagline = $params['tagline'];
            $resume->about_me = $params['about_me'];
            $resume->years_of_experience = $params['years_of_experience'];
            $resume->expected_salary = $params['expected_salary'];
            $resume->personal_core_skills = serialize($params['personal_core_skills']);
            $resume->technical_core_skills = serialize($params['technical_core_skills']);
            $resume->hobbies = serialize($params['hobbies']);
            $resume->educations = serialize($params['educations']);
            $resume->experiences = serialize($params['experiences']);
            $resume->cover_photo_selection = $params['cover_photo_selection'];

            $resume->save();

            $users_without_resume = $this->getUsersWithoutResume();
            $users_with_resume = $this->getUsersWithResume();

            return Rest::success(['$users_without_resume' => $users_without_resume, '$users_with_resume' => $users_with_resume, 
                'msg' => 'Resume information was updated successfully']);
        }
    }

    public function updateUserWithResumeStatus(Request $request)
    {
        if ($request->ajax()) { 
            $params = $request->all();

            $resume = Resume::where('user_id', $params['user_id'])->first();
            $resume->status = ($params['status'] == 1) ? 0 : 1;
            $resume->save();

            $users_with_resume = $this->getUsersWithResume();

            return Rest::success(['$users_with_resume' => $users_with_resume, 'msg' => 'Resume status was updated successfully']);
        }
    }

    public function updateUserWithResumeViewable(Request $request)
    {
        if ($request->ajax()) { 
            $params = $request->all();

            $resume = Resume::where('user_id', $params['user_id'])->first();
            $resume->viewable = ($params['viewable'] == 'enabled') ? 'disabled' : 'enabled';
            $resume->save();

            $users_with_resume = $this->getUsersWithResume();

            return Rest::success(['$users_with_resume' => $users_with_resume, 'msg' => 'Resume viewable was updated successfully']);
        }
    }

    public function deleteUserWithResume(Request $request)
    {
        if ($request->ajax()) { 
            Resume::destroy($request->get('id'));
            $users_with_resume = $this->getUsersWithResume();
            return Rest::success(['$users_with_resume' => $users_with_resume, 'msg' => 'Resume information was deleted successfully']);
        }
    }

    protected function couponValidator(array $data, $option = []) {
        $fields = [
            'coupon' => 'required|min:3|max:255',
            'discount_type' => 'required',
            'discount_value' => 'required|numeric|min:1',
            'duration_type' => 'required',
            'duration_value' => 'required|numeric|min:1|max:12',
            'max_redemptions' => 'numeric',
            'redeem_by' => 'required'
        ];
        return Validator::make($data, $fields + $option);
    }

    public function getAllCoupon(Request $request) {
        if ($request->ajax()) { 
            Stripe::setApiKey(Payment_gateway::getPaymentKey('stripe', 'secret'));
            return Rest::success(['$coupon' => Coupon::all()]);
        }
    }

    public function deleteCoupon(Request $request) {
        if ($request->ajax()) { 
            Stripe::setApiKey(Payment_gateway::getPaymentKey('stripe', 'secret'));
            $coupon = Coupon::retrieve($request->get('id'));
            $coupon->delete();
            return Rest::success(['$coupon' => Coupon::all(), 'msg' => 'Coupon information was deleted successfully']);
        }
    }

    public function createCoupon(Request $request) {
        if ($request->ajax()) { 
            $params = $request->all();

            $validator = $this->couponValidator($params);
            if($validator->fails()) {
                return Rest::failed($validator);
            }

            Stripe::setApiKey(Payment_gateway::getPaymentKey('stripe', 'secret'));
            try {
                Coupon::create(
                    array(
                        "currency" => "gbp",
                        "id" => $params['coupon'],
                        $params['discount_type'] => ($params['discount_type'] === 'percent_off') 
                            ? $params['discount_value'] : $params['discount_value'] * 100,
                        "duration" => $params['duration_type'],
                        "duration_in_months" => ($params['duration_type'] === 'repeating') ? $params['duration_value'] : null,
                        "max_redemptions" => ($params['max_redemptions'] !== '') ? $params['max_redemptions'] : null ,
                        "redeem_by" => ($params['redeem_by'] !== '') ? $params['redeem_by'] : null
                    )
                );

            } catch (Exception $e) {
                switch ($e->getMessage()) {
                    case 'Coupon already exists.':
                        return Rest::dataExist(['coupon' => [$e->getMessage()]]);
                        break;
                    case 'Amount must be no more than £999,999.99':
                    case 'percent_off must be between 1 and 100':
                        return Rest::dataExist(['discount_value' => [$e->getMessage()]]);
                        break;
                    case 'Invalid timestamp: must be an integer Unix timestamp in the future.':
                        return Rest::dataExist(['redeem_by' => [$e->getMessage()]]);
                        break;
                    default:
                        return Rest::dataExist(['error_on_stripe' => [$e->getMessage()] , 'type' => 'danger', 'msg' => $e->getMessage(), '$coupon' => Coupon::all() ]);
                        break;
                }
            }

            return Rest::success(['$coupon' => Coupon::all(), 'msg' => 'Coupon information was created successfully']);
        }
    }

    public function updateCoupon(Request $request) {
        if ($request->ajax()) { 
            $params = $request->all();
            $validator = $this->couponValidator($params);
            if($validator->fails()) {
                return Rest::failed($validator);
            }

            Stripe::setApiKey(Payment_gateway::getPaymentKey('stripe', 'secret'));
           
            try {

                $coupon = Coupon::retrieve($params['id']);
                $coupon->delete();
                Coupon::create(
                    array(
                        "currency" => "gbp",
                        "id" => $params['coupon'],
                        $params['discount_type'] => ($params['discount_type'] === 'percent_off') 
                            ? $params['discount_value'] : $params['discount_value'] * 100,
                        "duration" => $params['duration_type'],
                        "duration_in_months" => ($params['duration_type'] === 'repeating') ? $params['duration_value'] : null,
                        "max_redemptions" => ($params['max_redemptions'] !== '') ? $params['max_redemptions'] : null ,
                        "redeem_by" => ($params['redeem_by'] !== '') ? $params['redeem_by'] : null
                    )
                );
            } catch (Exception $e) {
                switch ($e->getMessage()) {
                    case 'Coupon already exists.':
                        return Rest::dataExist(['coupon' => [$e->getMessage()]]);
                        break;

                    case 'Amount must be no more than £999,999.99':
                    case 'percent_off must be between 1 and 100':
                        return Rest::dataExist(['discount_value' => [$e->getMessage()]]);
                        break;
                    case 'Invalid timestamp: must be an integer Unix timestamp in the future.':
                        return Rest::dataExist(['redeem_by' => [$e->getMessage()]]);
                        break;
                    default:
                        return Rest::dataExist(['error_on_stripe' => [$e->getMessage()], 'type' => 'danger', 'msg' => $e->getMessage(), '$coupon' => Coupon::all() ]);
                        break;
                }
            }

            return Rest::success(['$coupon' => Coupon::all(), 'msg' => 'Coupon information was updated successfully']);
        }
    }

    protected function getGoogleAnalyticsCode() {
        $analytic = Google_analytic::first();
        return ($analytic === null) ? '' : unserialize($analytic->code);
    }

    public function getCodeGoogleAnalytics(Request $request) {
        if ($request->ajax()) { 
            return Rest::success(['code' => $this->getGoogleAnalyticsCode()]);
        }
    }

    public function saveCodeGoogleAnalytics(Request $request) {
        if ($request->ajax()) { 
            $params = $request->all();
            $analytic = Google_analytic::first();
            if ($analytic === null) {
                $analytic = new Google_analytic([
                    'code' => serialize($params['code'])
                ]);
            } else {
                $analytic->code = serialize($params['code']);
            }
            $analytic->save();
            return Rest::success(['code' => unserialize($analytic->code), 'msg' => 'Google Analytics script was updated successfully']);
        }
    
    }

    private function getAllNotifications() {
        $notifications = 
            Notification::where('user_id', Auth::user()->id)
                        ->where('is_viewed', false)->orderBy('created_at', 'desc')->get()->toArray();

        foreach ($notifications as $key => $value) {
            $value['data'] = unserialize($value['data']);
            $value['time_ago'] = GlobalCtrlHelper::time_elapsed_string(strtotime($value['created_at']));
            $notifications[$key] = $value;
        }
        return $notifications;
    }

    public function getAllNotificationsAjax(Request $request) {
        if ($request->ajax()) { 
            return Rest::success(['$notifications' => $this->getAllNotifications()]);
        }
    }
    public function updateAllNotificationOnClicked(Request $request) {
        if ($request->ajax()) { 
            $params = $request->all();
            if (isset($params['user_id'])) {
                Notification::where('user_id', $params['user_id'])->update(['is_clicked' => true]);
            }
            return Rest::success(['$notifications' => $this->getAllNotifications()]);
        }
    }

    public function updateAllNotificationOnViewed(Request $request) {
        if ($request->ajax()) { 
            $params = $request->all();
            if (isset($params['id'])) {
                Notification::find($params['id'])->update(['is_viewed' => true]);
            }
            return Rest::success(['$notifications' => $this->getAllNotifications()]);
        }
    }

    public function showAllNotificationOnViewed(Request $request) {
        if ($request->ajax()) { 
            $params = $request->all();
            Notification::where('user_id', Auth::user()->id)->update(['is_viewed' => false]);
            return Rest::success(['$notifications' => $this->getAllNotifications()]);
        }

    }
    public function hideAllNotificationOnViewed(Request $request) {
        if ($request->ajax()) { 
            $params = $request->all();
            Notification::where('user_id', Auth::user()->id)->update(['is_viewed' => true]);
            return Rest::success(['$notifications' => $this->getAllNotifications()]);
        }
    }

    public function getAllBrandingsAjax() {
        return Rest::success(['$brandings' => $this->getAllBrandings()]);
    }

    protected function getAllBrandings() {
        return Branding::orderBy('created_at', 'desc')->get()->toArray();
    }

    protected function createBrandingValidator(array $data, $option = []) {
        $fields = [
            'name' => 'required|min:3|max:255|unique:brandings',
            'title' => 'required|min:3|max:255',
            'description' => 'required|min:3|max:1000',
            'per_payment_type' => 'required',
            'associated_access' => 'required',
            'payment_plan_type' => 'required'
        ];
        return Validator::make($data, $fields + $option);
    }

    public function createBranding(Request $request){
        $params = $request->all();
        $validator = $this->createBrandingValidator($params, 
            ($params['payment_plan_type'] === 'trial') ? [] : ['price' => 'required|numeric|min:1']);
        if($validator->fails()) {
            return Rest::failed($validator);
        }

        $branding = new Branding([
            'name' => $params['name'],
            'title' => $params['title'],
            'description' => $params['description'],
            'price' => $params['price'],
            'per_payment_type' => $params['per_payment_type'],
            'associated_access' => $params['associated_access'],
            'payment_plan_type' => $params['payment_plan_type']
        ]);

        $branding->save();

        $brandings = $this->getAllBrandings();

        return Rest::success(['$brandings' => $brandings, 'msg' => 'Branding information was created successfully']);
    }

    public function deleteBranding(Request $request) {
        if ($request->ajax()) { 
            $params = $request->all();

            Branding::destroy($request->get('id'));

            $brandings = $this->getAllBrandings();

            return Rest::success(['$brandings' => $brandings, 'msg' => 'Branding information was deleted successfully']);
        }
    }

    protected function updateBrandingValidator(array $data, $option = []) {
        $fields = [
            'name' => 'required|min:3|max:255',
            'title' => 'required|min:3|max:255',
            'description' => 'required|min:3|max:1000',
            'per_payment_type' => 'required',
            'associated_access' => 'required',
            'payment_plan_type' => 'required'
        ];
        return Validator::make($data , $fields + $option);
    }

    public function updateBranding(Request $request) {
        if ($request->ajax()) { 
            $params = $request->all();

            $validator = $this->updateBrandingValidator($params, 
                ($params['payment_plan_type'] === 'trial') ? [] : ['price' => 'required|numeric|min:1']);
            if($validator->fails()) {
                return Rest::failed($validator);
            }

            $allowUpdate = false;        
            $ifExist = Branding::where('name' , $params['name'])->first();
            if ($ifExist !== null) {
                if ($params['id'] == $ifExist->id) {
                    $allowUpdate = true;
                }
            } else {
                $allowUpdate = true;
            }
            
            if ($allowUpdate) {
                $branding = Branding::find($params['id']);
                $branding->name = $params['name'];
                $branding->title = $params['title'];
                $branding->description = $params['description'];
                $branding->price = $params['price'];
                $branding->per_payment_type = $params['per_payment_type'];
                $branding->associated_access = $params['associated_access'];
                $branding->payment_plan_type = $params['payment_plan_type'];
                $branding->save();
            } else {
                return Rest::dataExist(['title' => ['Branding title already exist.']]);
            }

            $brandings = $this->getAllBrandings();

            return Rest::success(['$brandings' => $brandings, 'msg' => 'Branding information was updated successfully']);
        }
    }
    
    protected function getCompanyProfile() {
       
        $company_profile = null;
        foreach (Company_profile::where('user_id', Auth::user()->id)->get()->toArray() as $key => $value) {
            $value['company_description'] = unserialize($value['company_description']);
            $value['company_perks'] = unserialize($value['company_perks']);
            $value['social_networks'] = unserialize($value['social_networks']);
            $company_profile = $value;
        }
        return $company_profile;
    }

    public function getCompanyProfileAjax(Request $request) {
        if ($request->ajax())
        {   
            $company_profile = $this->getCompanyProfile();
            return Rest::success(['$company_profile' => $company_profile]);
        } 
    }   
}
