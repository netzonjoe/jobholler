<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;
use App\User_group;
use App\Resume;
use App\Job_post;
use App\Company_profile;
use Validator;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Auth;
use Acme\Billing\BillingInterface;
use Config;
use Stripe\Stripe;
use Stripe\Plan;
use Stripe\Coupon;
use Response;

class GuestUserController extends Controller
{	

	use AuthenticatesAndRegistersUsers, ThrottlesLogins;


	protected $redirectTo = '/';
	public function __construct()
    {
      $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data, $option = [])
    {
    	$fields = [
            'firstname' => 'required|max:255',
            'lastname' => 'required|max:255',
            'phone'    => 'min:14|max:15',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ];

        return Validator::make($data, $fields + $option);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data, $user_type)
    {   
        $user_group = User_group::find($user_type);
        switch (strtolower($user_group->name)) {
            case 'candidate':
                $this->redirectTo = action('CandidateDashboardController@index');
                break;

            case 'employer':
                $this->redirectTo = action('EmployerDashboardController@index');
                break;
        }

        return $user_group->userDetails()->save(new User([
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'phone' => $data['phone'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'address' => (isset($data['address'])) ? $data['address'] : '',
	        'city' => (isset($data['city'])) ? $data['city'] : '',
	        'state'    => (isset($data['state'])) ? $data['state'] : '',
	        'zip' => (isset($data['zip'])) ? $data['zip'] : '',
	        'country' => (isset($data['country'])) ? $data['country'] : '',
        ]));
    }


    public function postRegister(Request $request, BillingInterface $billing)
    {
        return $this->registerUserWithStripe($request, $billing);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function registerUserWithStripe($request, $billing)
    {	
    	if ($request->get('stripeToken') === null) {
    		$validator = $this->validator($request->all());	
    	} else {
    		$user_details = $this->get_session('pending_user_for_payment');
            if ($user_details === null) {
                return redirect(action('CustomUserAuthController@getRegister'));
            }
    		$validator = $this->validator($user_details);
    	}
        

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        if ($request->get('stripeToken') === null) { 
            $payment_type = ['amplify' , 'ignite']; // Later add payment plan
            $user_group = User_group::find($request->get('user_type'));
        	if (in_array($user_group->sub_role, $payment_type)) {
	        	$this->set_session('pending_user_for_payment', $request->all());
	        	Stripe::setApiKey(Config::get('services.stripe.secret'));
    			$plan = Plan::all();
    			foreach ($plan['data'] as $key => $value) {
    				if ($value['id'] == $user_group->sub_role) {
    					$price = $value['amount'] / 100;
    				}
    			}
	        	return view('auth.register', ['goto_payment' => true, 'price' => $price]);
	        } else {
	        	Auth::guard($this->getGuard())->login($this->create($request->all()));
	        }
        } else {
	        	Auth::guard($this->getGuard())->login($this->create($user_details));
                $user_group = User_group::find($user_details['user_type']);
	        	Auth::user()->subscription($user_group->sub_role)->create($request->get('stripeToken'), [
				    'email' => $user_details['email'], 
				    'description' => $user_group->name . ' ' . $user_group->sub_role
				]);
        }
        return redirect($this->redirectPath());
    }

    

    public function employer(Request $request) {
    	return view('guest.employer');
    }

    public function submitvacancy(Request $request) {
    	return view('guest.submitvacancy');
    }

    public function postSubmitvacancy(Request $request)
    {
    	$validator = $this->validator($request->all());	
        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }
        $user_type = User_group::where(['sub_role' =>'basic' , 'name' => 'employer'])->first()->toArray();
	    Auth::guard($this->getGuard())->login($this->create($request->all(), $user_type['id']));
        return redirect($this->redirectPath());
    }
    public function checkoutAmplify(Request $request) {
    	$state = Config::get('options.state');
    	$country = Config::get('options.country');
    	$month = Config::get('options.month');
    	Stripe::setApiKey(Config::get('services.stripe.secret'));
    	$plan = Plan::retrieve('amplify');
    	$total = ($plan['amount'] / 100);
    	$amount = round($total * 0.80);
    	return view('guest.checkout-amplify', compact('state', 'country', 'month', 'amount', 'total'));
    }

    public function postAjaxCheckCouponAmplify(Request $request) {
    	if ($request->ajax())
		{
			Stripe::setApiKey(Config::get('services.stripe.secret'));
			$discount = Coupon::retrieve($request->get('coupon')); 
			$plan = Plan::retrieve('amplify');
		    $total = ($plan['amount'] / 100);
			if ($discount['percent_off'] !== null) {
		    	$amount_off = $total * ($discount['percent_off'] / 100);
		    	$discounted = $total - $amount_off;
			}
			if ($discount['amount_off'] !== null) {
				$amount_off = ($discount['amount_off'] / 100);
		    	$discounted = $total - $amount_off;
			}
			
    		return Response::json(['discounted' => $discounted , 'amount_off' => $amount_off]);
		} 
    }

    public function postCheckoutAmplify(Request $request) {
    	$validator = $this->validator($request->all(), [
    		'address' => 'required|max:255',
            'city' => 'required|max:255',
            'state'    => 'required|max:255',
            'zip' => 'required|numeric',
            'country' => 'required|max:255',
    	]);	
        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        $user_type = User_group::where(['sub_role' =>'amplify' , 'name' => 'employer'])->first()->toArray();
        Auth::guard($this->getGuard())->login($this->create($request->all(), $user_type['id']));
        $params = [
		    'email' => $request->get('email'), 
		    'description' => 'Employer - amplify',
		    "metadata" => [
		    	'firstname' => $request->get('firstname'),
	            'lastname' => $request->get('lastname'),
	            'phone' => $request->get('phone'),
		    	'address' => $request->get('address'),
		    	'city' => $request->get('city'),
		    	'state' => $request->get('state'),
		    	'zip' => $request->get('zip'),
		    	'country' => $request->get('country')
		    ]
		];
		if ($request->get('coupon') !== null) {
			$params = $params + ['coupon' => $request->get('coupon')];	
		}
    	Auth::user()->subscription($user_type['sub_role'])->create($request->get('stripeToken'), $params);

        return redirect($this->redirectPath());
    }

    public function postAjaxCheckCouponIgnite(Request $request) {
    	if ($request->ajax())
		{
			Stripe::setApiKey(Config::get('services.stripe.secret'));
			$discount = Coupon::retrieve($request->get('coupon')); 
			$plan = Plan::retrieve('ignite');
		    $total = ($plan['amount'] / 100);
			if ($discount['percent_off'] !== null) {
		    	$amount_off = $total * ($discount['percent_off'] / 100);
		    	$discounted = $total - $amount_off;
			}
			if ($discount['amount_off'] !== null) {
				$amount_off = ($discount['amount_off'] / 100);
		    	$discounted = $total - $amount_off;
			}
			
    		return Response::json(['discounted' => $discounted , 'amount_off' => $amount_off]);
		} 
    }

    public function checkoutIgnite(Request $request) {
    	$state = Config::get('options.state');
    	$country = Config::get('options.country');
    	$month = Config::get('options.month');
    	Stripe::setApiKey(Config::get('services.stripe.secret'));
    	$plan = Plan::retrieve('ignite');
    	$total = ($plan['amount'] / 100);
    	$amount = round($total * 0.80);
    	return view('guest.checkout-ignite', compact('state', 'country', 'month', 'amount', 'total'));
    }

    public function postCheckoutIgnite(Request $request) {
    	$validator = $this->validator($request->all(), [
    		'address' => 'required|max:255',
            'city' => 'required|max:255',
            'state'    => 'required|max:255',
            'zip' => 'required|numeric',
            'country' => 'required|max:255',
    	]);	
        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        $user_type = User_group::where(['sub_role' =>'ignite' , 'name' => 'employer'])->first()->toArray();
        Auth::guard($this->getGuard())->login($this->create($request->all(), $user_type['id']));
        $params = [
		    'email' => $request->get('email'), 
		    'description' => 'Employer - ignite',
		    "metadata" => [
		    	'firstname' => $request->get('firstname'),
	            'lastname' => $request->get('lastname'),
	            'phone' => $request->get('phone'),
		    	'address' => $request->get('address'),
		    	'city' => $request->get('city'),
		    	'state' => $request->get('state'),
		    	'zip' => $request->get('zip'),
		    	'country' => $request->get('country')
		    ]
		];
		if ($request->get('coupon') !== null) {
			$params = $params + ['coupon' => $request->get('coupon')];	
		}
    	Auth::user()->subscription($user_type['sub_role'])->create($request->get('stripeToken'), $params);

        return redirect($this->redirectPath());
    }
    
}
