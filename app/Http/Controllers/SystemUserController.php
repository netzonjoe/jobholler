<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Requests;
use Route;
use Validator;
use App\User_group;
use App\User_permission;
use DB;
use Config;
class SystemUserController extends Controller
{
    // use ValidatesRequests;
    public function __construct()
    {
     
    }

    public function addUserGroup() {
    	$listRoutes = Route::getRoutes()->getRoutes();
    	$excludesCtrl = ['AuthController@', 'CustomUserAuthController@', 'PasswordController@', 'HomeController@'];
    	$get_uris = [];
    	$post_uris = [];	
    	foreach ($listRoutes as $key => $value) {
    		$ctrl = $value->getAction();
    		if (isset($ctrl['controller'])) {
    			$isQualified = true;
				for ($i = 0; $i < count($excludesCtrl); $i++) {
					if (strpos($ctrl['controller'],  '\\' . $excludesCtrl[$i])) {
						$isQualified = false;
						break;
					}
				}
				if ($isQualified) {
		    		if (in_array('GET', $value->getMethods())) {
		    			$get_uris [] = $value->getUri();
		    		} else if (in_array('POST', $value->getMethods())) {
		    			$post_uris [] = $value->getUri();
		    		}
				}
    		}
    	}
    	return view('users.add_user_group', compact('get_uris', 'post_uris'));
    }

    public function saveUserGroup(Request $request) {
        $this->validate($request, [
	        'user_group_name' => 'required|min:3|max:255',
	        'sub_role' => 'min:3|max:255'
	    ]);
       	$data = $request->all();
	    $has_sub_role = false;
        if (isset($data['sub_role'])) {
        	$has_sub_role = true;
        }

        $user_group = User_group::create([
            'name' => $request->get('user_group_name'),
            'sub_role' => ($has_sub_role) ? $request->get('sub_role') : '',
            'has_sub_role' => $has_sub_role
        ]);


        if (isset($data['permission'])) {
        	if (isset($data['permission']['access'])) {
        		foreach ($data['permission']['access'] as $key => $uri) {
        			$user_group->userPermission()->save(new User_permission([
			        	'uri' => $uri,
			        	'method' => 'GET',
			        	'is_enabled' => true
			        ]));
        		}
        	}
        	if (isset($data['permission']['modify'])) {
        		foreach ($data['permission']['modify'] as $key => $uri) {
			        $user_group->userPermission()->save(new User_permission([
			        	'uri' => $uri,
			        	'method' => 'POST',
			        	'is_enabled' => true
			        ]));
        		}
        	}
        }
        $request->session()->flash('alert-success', 'User Group was successful added!');
        return redirect(action('SystemUserController@addUserGroup'));
    }

    public function listUserGroup(Request $request) {
    	$data = $request->all();
    	$order_by = (isset($data['order_by']))? ($data['order_by']==='asc')? 'desc' : 'asc' : 'asc';
    	$user_group_name = ['sort_by=name', 'order_by=asc'];
    	$sub_role_name = ['sort_by=sub_role', 'order_by=asc'];
    	$user_group_class_name = 'asc';
    	$sub_role_class_name = 'asc';
    	if (isset($data['sort_by']) && $data['sort_by'] === 'name') {
    		$user_group_name = ['sort_by=name', 'order_by=' . $order_by];
    		$user_group_class_name = $order_by;
    		$user_groups = DB::table('user_groups')->orderBy('name', $data['order_by'])->paginate(10);
    		$paginate_params = ['sort' => 'name' , 'order_by' => $order_by];
    	} else if (isset($data['sort_by']) && $data['sort_by'] === 'sub_role'){
    		$sub_role_name = ['sort_by=sub_role', 'order_by=' . $order_by];
    		$sub_role_class_name = $order_by;
    		$user_groups = DB::table('user_groups')->orderBy('sub_role', $order_by)->paginate(10);
    		$paginate_params = ['sort' => 'sub_role' , 'order_by' => $data['order_by']];
    	} else {
    		$user_groups = DB::table('user_groups')->orderBy('name', $order_by)->paginate(10);
    		$paginate_params = ['sort' => 'name' , 'order_by' => (isset($data['order_by'])) ? $data['order_by'] : 'asc'];
    	}
    	if (isset($data['page'])) {
    		$user_group_name[] = 'page=' . $data['page'];
    		$sub_role_name[] = 'page=' . $data['page'];
    	}
    
    	return view('users.list_user_group', compact('user_groups','user_group_name' ,'sub_role_name', 'user_group_class_name', 'sub_role_class_name', 'paginate_params'));
    }

    public function editUserGroup(Request $request) {
    	$data = $request->all();

    	$listRoutes = Route::getRoutes()->getRoutes();
    	$excludesCtrl = ['AuthController@', 'CustomUserAuthController@', 'PasswordController@', 'HomeController@'];
    	$get_uris = [];
    	$post_uris = [];
    	foreach ($listRoutes as $key => $value) {
    		$ctrl = $value->getAction();
    		if (isset($ctrl['controller'])) {
    			$isQualified = true;
				for ($i = 0; $i < count($excludesCtrl); $i++) {
					if (strpos($ctrl['controller'],  '\\' . $excludesCtrl[$i])) {
						$isQualified = false;
						break;
					}
				}
				if ($isQualified) {
		    		if (in_array('GET', $value->getMethods())) {
		    			$get_uris [] = $value->getUri();

		    		} else if (in_array('POST', $value->getMethods())) {
		    			$post_uris [] = $value->getUri();
		    		}
				}
    		}
    	}
    	$edit_id = '';
    	$edit_page = false;
    	if (isset($data['edit'])) {
    		$edit_page = true;
    		$edit_id = $data['edit'];
    		$user_group = User_group::find($data['edit'])->toArray();
    		$user_permissions = User_permission::where([
    			'user_group_id' => $user_group['id'], 
    			'method' 		=> 'GET'
    		])->get()->toArray();
    		$user_permissions_access = [];
    		foreach ($user_permissions as $key => $value) {
    			$user_permissions_access[$value['uri']] = $value;
    		}
    		$user_permissions = User_permission::where([
    			'user_group_id' => $user_group['id'], 
    			'method' 		=> 'POST'
    		])->get()->toArray();
    		$user_permissions_modify = [];
    		foreach ($user_permissions as $key => $value) {
    			$user_permissions_modify[$value['uri']] = $value;
    		}
    	}

    	if (!$edit_page) {
    		return redirect(action('SystemUserController@listUserGroup'));
    	}
    	return view('users.edit_user_group', compact('get_uris', 'post_uris', 
    		'user_group', 'edit_page', 'user_permissions_access' , 'user_permissions_modify',
    		'edit_id'));
    }

    public function saveEditUserGroup(Request $request) {
    	$this->validate($request, [
	        'user_group_name' => 'required|min:3|max:255',
	        'sub_role' => 'min:3|max:255'
	    ]);
       	$data = $request->all();


        if (isset($data['edit'])) {
    		$user_group = User_group::find($data['edit']);

    		$user_group->name = $data['user_group_name'];
    		$user_group->sub_role = (isset($data['is_checked_sub_role']) && 
    								$data['is_checked_sub_role'] === 'on') ? $data['sub_role'] : '';
    		$user_group->has_sub_role = (isset($data['is_checked_sub_role']) && 
    									$data['is_checked_sub_role'] === 'on') ? true : false;
    		
    		$user_group->save();

    		
    		$user_group_arr = $user_group->toArray();

    		$access = isset($data['permission']['access']) ? $data['permission']['access'] : [];
    		$modify = isset($data['permission']['modify']) ? $data['permission']['modify'] : [];
    		/*-------Update all get permissions--------------*/
    		$user_permissions = User_permission::where([
    			'user_group_id' => $user_group_arr['id'], 
    			'method' 		=> 'GET'
    		])->get()->toArray();
    		if (count($access) === 0) {
    			User_permission::where([
	    			'user_group_id' => $user_group_arr['id'], 
	    			'method' 		=> 'GET'
	    		])->update(['is_enabled' => false]);
    		}
    		foreach ($access as $index => $uri) {
    			
	    		$if_existed_permission = User_permission::where([
	    			'user_group_id' => $user_group_arr['id'], 
	    			'method' 		=> 'GET',
	    			'uri'			=> $uri
	    		])->count() > 0 ? true: false;

	    		if ($if_existed_permission) {

	    			foreach ($user_permissions as $key => $value) {
	    				$ifChecked = false;
		    			if (in_array($value['uri'], $access)) {
							$ifChecked = true;
						}
		    			$permission = User_permission::find($value['id']);
		    			if ($ifChecked) {
		    				$permission->is_enabled = true;
		    			} else {
		    				$permission->is_enabled = false;
		    			}
		    			$permission->save();
		    		}

	    		} else {

	    			$user_group->userPermission()->save(new User_permission([
			        	'uri' => $uri,
			        	'method' => 'GET',
			        	'is_enabled' => true
			        ]));

	    		}
    		}

    		/*-------Update all post permissions--------------*/

    		$user_permissions = User_permission::where([
    			'user_group_id' => $user_group_arr['id'], 
    			'method' 		=> 'POST'
    		])->get()->toArray();
    		if (count($modify) === 0) {
    			User_permission::where([
	    			'user_group_id' => $user_group_arr['id'], 
	    			'method' 		=> 'POST'
	    		])->update(['is_enabled' => false]);
    		}
    		foreach ($modify as $index => $uri) {
    			
	    		$if_existed_permission = User_permission::where([
	    			'user_group_id' => $user_group_arr['id'], 
	    			'method' 		=> 'POST',
	    			'uri'			=> $uri
	    		])->count() > 0 ? true: false;

	    		if ($if_existed_permission) {

	    			foreach ($user_permissions as $key => $value) {
	    				$ifChecked = false;
		    			if (in_array($value['uri'], $modify)) {
							$ifChecked = true;
						}
		    			$permission = User_permission::find($value['id']);
		    			if ($ifChecked) {
		    				$permission->is_enabled = true;
		    			} else {
		    				$permission->is_enabled = false;
		    			}
		    			$permission->save();
		    		}

	    		} else {

	    			$user_group->userPermission()->save(new User_permission([
			        	'uri' => $uri,
			        	'method' => 'POST',
			        	'is_enabled' => true
			        ]));

	    		}
    		}
    		$request->session()->flash('alert-success', 'Record was successful updated!');
    		return redirect(action('SystemUserController@editUserGroup') . '?edit=' . $data['edit']);
    	}

    }

    
    public function deleteUserGroup(Request $request) {
    	$data = $request->all();
    	User_group::destroy($data['selected']);
    	$request->session()->flash('alert-success', 'Record was successful deleted!');
    	return redirect(action('SystemUserController@listUserGroup'));
    }

    public function listUser(Request $request) {
        $data = $request->all();
        $order_by = (isset($data['order_by']))? ($data['order_by']==='asc')? 'desc' : 'asc' : 'asc';
        $user_email = ['sort_by=email', 'order_by=asc'];
        $user_status = ['sort_by=status', 'order_by=asc'];
        $user_email_class = 'asc';
        $user_status_class = 'asc';
        if (isset($data['sort_by']) && $data['sort_by'] === 'email') {
            $user_email = ['sort_by=email', 'order_by=' . $order_by];
            $user_email_class = $order_by;
            $users = DB::table('users')->orderBy('email', $data['order_by'])->paginate(10);
            $paginate_params = ['sort' => 'name' , 'order_by' => $order_by];
        } else if (isset($data['sort_by']) && $data['sort_by'] === 'sub_role'){
            $sub_role_name = ['sort_by=sub_role', 'order_by=' . $order_by];
            $sub_role_class_name = $order_by;
            $user_groups = DB::table('user_groups')->orderBy('sub_role', $order_by)->paginate(10);
            $paginate_params = ['sort' => 'sub_role' , 'order_by' => $data['order_by']];
        } else {
            $user_groups = DB::table('user_groups')->orderBy('name', $order_by)->paginate(10);
            $paginate_params = ['sort' => 'name' , 'order_by' => (isset($data['order_by'])) ? $data['order_by'] : 'asc'];
        }
        if (isset($data['page'])) {
            $user_group_name[] = 'page=' . $data['page'];
            $sub_role_name[] = 'page=' . $data['page'];
        }

        return view('users.list_user_group', compact('user_groups','user_group_name' ,'sub_role_name', 'user_group_class_name', 'sub_role_class_name', 'paginate_params'));
    }


    public function addUser() {
        $status = Config::get('options.status');
        return view('users.add_user', compact('status'));
    }
}




