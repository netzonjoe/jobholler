<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Validator;
use Auth;
use App\Branding;
use App\Google_analytic;
use App\Payment_gateway;
use App\Company_profile;
use App\Notification;
use App\User_group;
use App\Product;
use App\Resume;
use App\User;
use App\Job_post;
use App\Email_task;
use Mail;
use Route;
use DB;
use Acme\Rest;
use Acme\GlobalCtrlHelper;
use Carbon\Carbon;
use Stripe\Stripe;
use Stripe\Plan;
use Stripe\Coupon;
use Config;
use Exception;
use File;


class EmployerDashboardController extends Controller
{	
	protected function getAllExpiredJobs() {
        $job_expired = [];
        $profile = Company_profile::where('user_id', Auth::user()->id)->first();
        if ($profile !== null) {
            $expired_date = new Carbon;
            $expired_date->subDays(7);
            $jobs = DB::table('job_posts')
                ->where('company_profile_id', $profile->id)
                ->where('job_posts.start_date', '<' , $expired_date->toDateTimeString())
                ->join('company_profiles', 'job_posts.company_profile_id', '=', 'company_profiles.id')
                ->select('job_posts.*', 'company_profiles.company_name', 'company_profiles.company_name_slug')
                ->orderBy('job_posts.created_at', 'desc')
                ->get();
            $jobs = json_decode(json_encode($jobs), true);
            
            foreach ($jobs as $key => $value) {
                $value['skills'] = unserialize($value['skills']);
                $value['job_description'] = unserialize($value['job_description']);
                $job_expired[] = $value;
            }
        }
        return $job_expired;
    }

    protected function getAllPostedJobs() {
        $job_posted = [];
        $profile = Company_profile::where('user_id', Auth::user()->id)->first();
        if ($profile !== null) {
            $expired_date = new Carbon;
            $expired_date->subDays(7);

            $jobs = DB::table('job_posts')
                ->where('company_profile_id', $profile->id)
                ->where('job_posts.start_date', '>' , $expired_date->toDateTimeString())
                ->join('company_profiles', 'job_posts.company_profile_id', '=', 'company_profiles.id')
                ->select('job_posts.*', 'company_profiles.company_name', 'company_profiles.company_name_slug')
                ->orderBy('job_posts.created_at', 'desc')
                ->get();
            $jobs = json_decode(json_encode($jobs), true);

            
            foreach ($jobs as $key => $value) {
                $value['skills'] = unserialize($value['skills']);
                $value['job_description'] = unserialize($value['job_description']);
                $job_posted[] = $value;
            }
        }
        
        return $job_posted;
    }
    public function getAllPostedJobsAjax(Request $request) {
        if ($request->ajax())
        {   
            $jobs = $this->getAllPostedJobs();
            return Rest::success(['$jobs' => $jobs]);
        } 
    }
    public function index()
   	{	
        $_token = csrf_token();
        $route_list = json_encode(GlobalCtrlHelper::getRouteList('EmployerDashboardController'));
       	$baseurl = asset('');
        $imagesDir = asset('/img');
        $angular_directive_alert = GlobalCtrlHelper::getAngularDirectiveAlert();
        $google_analytic = '';

        $job_listing = asset('/job-listing');
        $jobs = json_encode($this->getAllPostedJobs());
        $expired_jobs = json_encode($this->getAllExpiredJobs());

   		return view('employer_dashboard.index', 
   			compact('_token', 'route_list', 'baseurl', 'imagesDir', 'angular_directive_alert', 'google_analytic',
   					'jobs', 'expired_jobs', 'job_listing'));
   	}


   	protected function jobValidator(array $data, $option = []) {
        $fields = [
            'job_title' => 'required|max:255',
            'job_type' => 'required',
            'maximum_salary' => 'required|numeric|min:1',
            'paid_per' => 'required',
            'job_location' => 'required|max:255',
            'job_description' => 'required|min:1',
            'skills' => 'required',
            'experience_level' => 'required',
            'start_date' => 'required',
            'contact_name' => 'required|max:255',
            'contact_number' => 'required|min:10|max:10',
            'user_id' => 'required'
        ];
        return Validator::make($data, $fields+ $option);
    }

    protected function jobCreate(array $data) {
        $user = User::find($data['user_id']);
        $date = explode("-", $data['start_date']);

        return $user->job()->save(new Job_post([
            'job_title_slug' => $data['job_title_slug'],
            'job_title' => $data['job_title'],
            'job_type' => $data['job_type'],
            'minimum_salary' => $data['minimum_salary'],
            'maximum_salary' => $data['maximum_salary'],
            'paid_per' => $data['paid_per'],
            'job_location' => $data['job_location'],
            'job_description' => serialize($data['job_description']),
            'skills' => serialize($data['skills']),
            'experience_level' => $data['experience_level'],
            'start_date' => Carbon::createFromDate($date[0],$date[1],$date[2])->toDateTimeString() ,
            'contact_name' => $data['contact_name'],
            'contact_number' => $data['contact_number'],
            'reference' => $data['reference'],
            'company_profile_id' => $data['company_profile_id']
        ]));
    }

    protected function jobUpdate(array $data) {
        $job = Job_post::find($data['id']);
        $job->job_title_slug = $data['job_title_slug'];
        $job->job_title = $data['job_title'];
        $job->job_type = $data['job_type'];
        $job->minimum_salary = $data['minimum_salary'];
        $job->maximum_salary = $data['maximum_salary'];
        $job->paid_per = $data['paid_per'];
        $job->job_location = $data['job_location']; 
        $job->job_description = serialize($data['job_description']); 
        $job->skills = serialize($data['skills']);
        $job->experience_level = $data['experience_level'];
        $job->start_date = $data['start_date'];
        $job->contact_name = $data['contact_name'];
        $job->contact_number = $data['contact_number'];
        $job->company_profile_id = $data['company_profile_id'];
        $job->save();
    }

   	public function getAllExpiredJobsAjax(Request $request) {
        if ($request->ajax())
        {   
            $expired_jobs = $this->getAllExpiredJobs();
            return Rest::success(['$expired_jobs' => $expired_jobs]);
        } 
    }

    private function sendEmailJobPosting(array $data = []) {
        $user_group = User_group::where('name', strtolower('administrator'))->first();
        if ($user_group !== null) {

            $data['admin_email'] = $user_group->userDetails()->first()->email;
            $data['user_id'] = $user_group->userDetails()->first()->id;
            $data['from_user_id'] = Auth::user()->id;
            $data['from_user_email'] = Auth::user()->email;
            $data['template'] = 'job-posting';
            (new Email_task([
                'data' => serialize($data)
            ]))->save();
        }
    }

    public function createJob(Request $request)
    {   
        if ($request->ajax())
        {   
            $params = $request->all();
            $profile = Company_profile::where('user_id', Auth::user()->id)->first();
            if ($profile === null) {
                return Rest::warning(['msg' => 'Company Profile has not yet setup.']);
            }

            $params['company_profile_id'] = $profile->id;
            $params['user_id'] = Auth::user()->id;
            
            $additional_options = ['minimum_salary' => 'numeric|min:1', 'reference' => 'required|unique:job_posts'];
            if (is_numeric($params['minimum_salary'])) {
                if ($params['minimum_salary'] + 0 == 0) {
                    $params['minimum_salary'] = 'NaN';       
                }
            }
            if($params['minimum_salary'] === 'NaN') {
                $params['minimum_salary'] = '0.00';
                $additional_options['minimum_salary'] = 'numeric|min:0';
            }

            $validator = $this->jobValidator($params, $additional_options);
            if($validator->fails()) {
                return Rest::failed($validator);
            }

            $this->jobCreate($params);
            $params['company_name'] = $profile->company_name;
            $this->sendEmailJobPosting($params);
            $jobs = $this->getAllPostedJobs();
            return Rest::success(['$jobs' => $jobs, 'msg' => 'Job information was created successfully']);
        } 
    }

    public function cloneJob(Request $request) {
        if ($request->ajax())
        {   
            $params = $request->all();
            $profile = Company_profile::where('user_id', Auth::user()->id)->first();
            if ($profile === null) {
                return Rest::warning(['msg' => 'Company Profile has not yet setup.']);
            }
            $params['company_profile_id'] = $profile->id;
            $params['user_id'] = Auth::user()->id;
        
            $additional_options = ['minimum_salary' => 'numeric|min:1', 'reference' => 'required|unique:job_posts'];
            if (is_numeric($params['minimum_salary'])) {
                if ($params['minimum_salary'] + 0 == 0) {
                    $params['minimum_salary'] = 'NaN';       
                }
            }
            if($params['minimum_salary'] === 'NaN') {
                $params['minimum_salary'] = '0.00';
                $additional_options['minimum_salary'] = 'numeric|min:0';
            }
            $validator = $this->jobValidator($params, $additional_options);
            if($validator->fails()) {
                return Rest::warning(['msg' => 'Network error. Please try again']);
            }

            $this->jobCreate($params);
            $params['company_name'] = $profile->company_name;
            $jobs = $this->getAllPostedJobs();
            return Rest::success(['$jobs' => $jobs, 'msg' => 'Job information was cloned successfully']);
        } 
    }

    public function deleteJob(Request $request) {
        if ($request->ajax())
        {   
            Job_post::destroy($request->get('id'));
            $jobs = $this->getAllPostedJobs();
            return Rest::success(['$jobs' => $jobs, 'msg' => 'Job information was deleted successfully']);
        } 
    }

    public function updateJob(Request $request) {
        if ($request->ajax())
        {   
            $params = $request->all();
            $additional_options = ['minimum_salary' => 'numeric|min:1', 'reference' => 'required'];
            if (is_numeric($params['minimum_salary'])) {
                if ($params['minimum_salary'] + 0 == 0) {
                    $params['minimum_salary'] = 'NaN';       
                }
            }
            if($params['minimum_salary'] === 'NaN') {
                $params['minimum_salary'] = '0.00';
                $additional_options['minimum_salary'] = 'numeric|min:0';
            }
            $validator = $this->jobValidator($params, $additional_options);

            if($validator->fails()) {
                return Rest::failed($validator);
            }
            $allowUpdate = false;        
            $ifExist = Job_post::where('reference' , $params['reference'])->first();
            if ($ifExist !== null) {
                if ($params['id'] == $ifExist->id) {
                    $allowUpdate = true;
                }
            } else {
                $allowUpdate = true;
            }
            
            if ($allowUpdate) {
                $this->jobUpdate($params);
            } else {
                return Rest::dataExist(['reference' => ['The reference has already been taken.']]);
            }

            $jobs = $this->getAllPostedJobs();
            return Rest::success(['$jobs' => $jobs, 'msg' => 'Job information was updated successfully']);
        } 
    }

    public function repostJob(Request $request) {
        if ($request->ajax())
        {   
            $params = $request->all();

            $params['start_date'] = (new Carbon)->toDateTimeString();

            $this->jobUpdate($params);

            $expired_jobs = $this->getAllExpiredJobs();
            $jobs = $this->getAllPostedJobs();
            return Rest::success(['$expired_jobs' => $expired_jobs, '$jobs' => $jobs, 'msg' => 'Job information was reposted successfully']);
        } 
    }

    protected function saveCompanyProfileValidator(array $data, $option = []) {
        $fields = [
            'company_name' => 'required|min:3|max:255',
            'tagline' => 'required|min:3|max:255',
            'company_description' => 'required|min:3',
            'company_color' => 'required|regex:/^#[0-9A-F]{6}$/i',
            'company_website' => 'required|min:3|max:255',
            'company_logo' => 'mimes:jpeg,bmp,png,jpg',
            'company_icon' => 'mimes:jpeg,bmp,png,jpg',
            'company_video' => 'min:3|max:255',
            'company_perks' => 'required',
            'social_networks' => 'required'
        ];
        return Validator::make($data , $fields+ $option);
    }

    protected function updateCompanyProfileValidator(array $data, $option = []) {
        $fields = [
            'company_name' => 'required|min:3|max:255',
            'tagline' => 'required|min:3|max:255',
            'company_description' => 'required|min:3',
            'company_color' => 'required|regex:/^#[0-9A-F]{6}$/i',
            'company_website' => 'required|min:3|max:255',
            'company_video' => 'min:3|max:255',
            'company_perks' => 'required',
            'social_networks' => 'required'
        ];
        return Validator::make($data, $fields+ $option);
    }

    public function saveCompanyProfile(Request $request) {
        if ($request->ajax()) { 
            $params = $request->all();

            $profile = Company_profile::where('user_id', Auth::user()->id)->first();
            if ($profile === null) {

                $validator = $this->saveCompanyProfileValidator($params);
                if($validator->fails()) {
                    return Rest::failed($validator);
                }

                if ($request->hasFile('company_logo')) {
                    $ext = $request->file('company_logo')->getClientOriginalExtension();
                    $params['company_logo'] = 
                        $request->file('company_logo')->move('uploads/employer/', User_group::MakeAttributeId() . '.' . $ext);
                }

                if ($request->hasFile('company_icon')) {
                    $ext = $request->file('company_icon')->getClientOriginalExtension();
                    $params['company_icon'] = 
                        $request->file('company_icon')->move('uploads/employer/', User_group::MakeAttributeId() . '.' . $ext);
                }

                $profile = new Company_profile([
                    'user_id' => Auth::user()->id,
                    'company_name_slug' => $params['company_name_slug'],
                    'company_name' => $params['company_name'],
                    'tagline' => $params['tagline'],
                    'company_description' => serialize($params['company_description']),
                    'company_color' => $params['company_color'],
                    'company_website' => $params['company_website'],
                    'company_logo' => $params['company_logo'],
                    'company_icon' => $params['company_icon'],
                    'company_video' => (isset($params['company_video'])) ? $params['company_video'] : '',
                    'company_perks' => serialize($params['company_perks']),
                    'social_networks' => serialize($params['social_networks'])
                ]);
            } else {

                $validator = $this->updateCompanyProfileValidator($params);
                if($validator->fails()) {
                    return Rest::failed($validator);
                }

                if ($request->hasFile('company_logo')) {
                    $ext = $request->file('company_logo')->getClientOriginalExtension();
                    $params['company_logo'] = 
                        $request->file('company_logo')->move('uploads/employer/', User_group::MakeAttributeId() . '.' . $ext);
                }

                if ($request->hasFile('company_icon')) {
                    $ext = $request->file('company_icon')->getClientOriginalExtension();
                    $params['company_icon'] = 
                        $request->file('company_icon')->move('uploads/employer/', User_group::MakeAttributeId() . '.' . $ext);
                }

                $profile->company_name_slug = $params['company_name_slug'];
                $profile->company_name = $params['company_name'];
                $profile->tagline = $params['tagline'];
                $profile->company_description = serialize($params['company_description']);
                $profile->company_color = $params['company_color'];
                $profile->company_website = $params['company_website'];
                $profile->company_logo = $params['company_logo'];
                $profile->company_icon = $params['company_icon'];
                $profile->company_video = (isset($params['company_video'])) ? $params['company_video'] : '';
                $profile->company_perks = serialize($params['company_perks']);
                $profile->social_networks = serialize($params['social_networks']);
            }
            $profile->save();
            $company_profile = $this->getCompanyProfile();
            return Rest::success(['$company_profile' => $company_profile, 'msg' => 'Company Profile was saved successfully']);
        }
    }


    protected function getCompanyProfile() {
       
        $company_profile = null;
        foreach (Company_profile::where('user_id', Auth::user()->id)->get()->toArray() as $key => $value) {
            $value['company_description'] = unserialize($value['company_description']);
            $value['company_perks'] = unserialize($value['company_perks']);
            $value['social_networks'] = unserialize($value['social_networks']);
            $company_profile = $value;
        }
        return $company_profile;
    }

    public function getCompanyProfileAjax(Request $request) {
        if ($request->ajax())
        {   
            $company_profile = $this->getCompanyProfile();
            return Rest::success(['$company_profile' => $company_profile]);
        } 
    }

    protected function getEmployerAccount() {
        $user_account_details = User::where('email', Auth::user()->email)->get()->toArray();
        $user_account_details = (count($user_account_details) > 0) ? $user_account_details[0] : [];
        return $user_account_details;
    }

    public function getEmployerAccountAjax(Request $request) {
        if ($request->ajax())
        {   
            $user = $this->getEmployerAccount();
            return Rest::success(['$user_account_details' => $user]);
        } 
    }

    protected function saveEmployerAccountValidator(array $data, $option = []) {
        $fields = [
            'firstname' => 'required|min:3|max:255',
            'lastname' => 'required|min:3|max:255',
            'phone' => 'required|min:3|max:255',
            'email' => 'required|min:3|max:255',
            'address' => 'min:3|max:255',
            'password' => 'min:6|max:255',
            'confim_password' => 'min:6|max:255',
            'city' => 'min:3|max:255',
            'state' => 'min:3|max:255',
            'zip' => 'min:3|max:255',
            'country' => 'max:255'
        ];
        return Validator::make($data, $fields+ $option);
    }

    protected function saveEmployerBillingAddressValidator(array $data, $option = []) {
        $fields = [
            'city' => 'min:3|max:255',
            'state' => 'min:3|max:255',
            'zip' => 'min:3|max:255',
            'country' => 'max:255'
        ];
        return Validator::make($data, $fields+ $option);
    }

    public function saveEmployerAccount(Request $request) {
        if ($request->ajax()) { 
            $params = $request->all();

            $user = User::where('email', Auth::user()->email)->first();

            if ($params['form'] === 'account_details') {
                $validator = $this->saveEmployerAccountValidator($params);
            } else if ($params['form'] === 'billing_address') {
                $validator = $this->saveEmployerBillingAddressValidator($params);
            }

            $validator = $this->saveEmployerAccountValidator($params);
            if($validator->fails()) {
                return Rest::failed($validator);
            }

            $allowUpdate = false;
            $ifEmailExist = User::where('email' , $params['email'])->first();
            if ($ifEmailExist !== null) {
                if (Auth::user()->id == $ifEmailExist->id) {
                    $allowUpdate = true;
                }
            } else {
                $allowUpdate = true;
            }

            if (!$allowUpdate) {
                return Rest::dataExist(['email' => ['Email already exist.']]);    
            }

            $ifHasPassword = false;
            if (isset($params['password'])) {
                $ifHasPassword = true;
                
            }

            if (isset($params['confirm_password'])) {
                $ifHasPassword = true;
            }

            if ($ifHasPassword) {
                if (!isset($params['password'])) {
                    return Rest::dataExist(['password' => ['The Password field is required.']]);      
                }

                if (!isset($params['confirm_password'])) {
                    return Rest::dataExist(['confirm_password' => ['The Confim Password field is required.']]);      
                }

                if ($params['password'] !== $params['confirm_password']) {
                    return Rest::dataExist(['confirm_password' => ['The Confim Password field does\'nt matched to the Password field.']]);      
                }
            }

            if (isset($params['firstname'])) {
                $user->firstname = $params['firstname'];    
            }
            
            if (isset($params['lastname'])) {
                $user->lastname = $params['lastname'];    
            }

            if (isset($params['phone'])) {
                $user->phone = $params['phone'];    
            }

            if (isset($params['email'])) {
                $user->email = $params['email'];    
            }

            if (isset($params['address'])) {
                $user->address = $params['address'];    
            }


            if (isset($params['password'])) {
                $user->password = bcrypt($params['password']);    
            }


            if (isset($params['city'])) {
                $user->city = $params['city'];    
            }

            if (isset($params['state'])) {
                $user->state = $params['state'];    
            }

            if (isset($params['zip'])) {
                $user->zip = $params['zip'];    
            }

            if (isset($params['country'])) {
                $user->country = $params['country'];    
            }

            $user->save();

            $user_account_details = $this->getEmployerAccount();
            return Rest::success(['$user_account_details' => $user_account_details, 'msg' => 'Account Billing Address was saved successfully']);
        }
    }

    private function getAllBrandings() {
        return Branding::all()->toArray();
    }

    public function getAllBrandingsAjax(Request $request) {
        if ($request->ajax())
        {   
            $brandings = $this->getAllBrandings();
            return Rest::success(['$brandings' => $brandings]);
        } 
    }

    private function sendEmailPurchaseBranding(array $data = []) {
        $user_group = User_group::where('name', strtolower('administrator'))->first();
        if ($user_group !== null) {
            $data['admin_email'] = $user_group->userDetails()->first()->email;
            $data['user_id'] = $user_group->userDetails()->first()->id;
            $data['from_user_id'] = Auth::user()->id;
            $data['from_user_email'] = Auth::user()->email;

            $data['template'] = 'purchase-branding';
            $data['from_title'] = 'Purchase Branding';
            (new Email_task([
                'data' => serialize($data)
            ]))->save();
        }
    }

    public function purchaseBranding(Request $request) {
        if ($request->ajax())
        {   
            $params = $request->all();
            $this->sendEmailPurchaseBranding($params);
            return Rest::success(['msg' => 'Purchase Branding was subscribed successfully']);
        } 
    }
}
