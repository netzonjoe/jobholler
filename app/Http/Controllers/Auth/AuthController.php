<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    protected $maxLoginAttempts = 3; // Amount of bad attempts user can make
    
    protected $lockoutTime = 60; // Time for which user is going to be blocked in seconds

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'user_type' => 'required|max:9',
            'role_type' => 'required|max:10',
            'firstname' => 'required|max:255',
            'lastname' => 'required|max:255',
            'phone'    => 'min:14|max:15',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'user_type' => $data['user_type'],
            'role_type' => $data['role_type'],
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'phone' => $data['phone'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    protected function authenticated()
    {   
        if (Auth::user()->status === 1) {
            $user_type = Auth::user()->userGroup()->get()->toArray()[0];
            switch (strtolower($user_type['associated_access'])) {
                case 'none':
                    if (strtolower($user_type['name']) === 'administrator') {
                        return redirect()->intended(action('AdminDashboardController@index'));    
                    } else {
                        // associated_access == none or not administrator
                    }
                    
                    break;
                
                case 'candidate':
                    return redirect()->intended(action('CandidateDashboardController@index'));
                    break;

                case 'employer':
                    return redirect()->intended(action('EmployerDashboardController@index'));
                    break;
            }
        } else {
            Auth::logout();
            return redirect()->intended(asset('') . 'login');
        }
    }

}
