<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\User;
use App\User_group;
use Validator;
use App\Http\Requests;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Auth;
use Acme\Billing\BillingInterface;
use Config;
use Stripe\Stripe;
use Stripe\Plan;

class CustomUserAuthController extends Controller
{	
	use AuthenticatesAndRegistersUsers, ThrottlesLogins;

	protected $redirectTo = '/';
	public function __construct()
    {
      $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    private function set_session($key = null, $value = null) {
    	if (session_status() === PHP_SESSION_NONE) session_start();
    	if ($key !== null && $value !== null) {
    		$_SESSION[$key] = $value;
    	}	
    }

    private function get_session($key = null, $auto_delete = true) {
    	if (session_status() === PHP_SESSION_NONE) session_start();
    	if ($key !== null) {
    		$getValue = isset($_SESSION[$key]) ? $_SESSION[$key] : null;
    		if ($auto_delete) {
    			unset($_SESSION[$key]);	
    		}
    		return $getValue;
    	}	
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'user_type' => 'required|numeric',
            'firstname' => 'required|max:255',
            'lastname' => 'required|max:255',
            'phone'    => 'min:14|max:15',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {   
        $user_group = User_group::find($data['user_type']);
        return $user_group->userDetails()->save(new User([
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'phone' => $data['phone'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]));
    }


    public function postRegister(Request $request, BillingInterface $billing)
    {
        return $this->registerUserWithStripe($request, $billing);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function registerUserWithStripe($request, $billing)
    {	
    	if ($request->get('stripeToken') === null) {
    		$validator = $this->validator($request->all());	
    	} else {
    		$user_details = $this->get_session('pending_user_for_payment');
            if ($user_details === null) {
                return redirect(action('CustomUserAuthController@getRegister'));
            }
    		$validator = $this->validator($user_details);
    	}
        

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        if ($request->get('stripeToken') === null) { 
            $payment_type = ['amplify-me' , 'ignite-me']; // Later add payment plan
            $user_group = User_group::find($request->get('user_type'));
        	if (in_array($user_group->sub_role, $payment_type)) {
	        	$this->set_session('pending_user_for_payment', $request->all());
	        	Stripe::setApiKey(Config::get('services.stripe.secret'));
    			$plan = Plan::all();
    			foreach ($plan['data'] as $key => $value) {
    				if ($value['id'] == $user_group->sub_role) {
    					$price = $value['amount'] / 100;
    				}
    			}
	        	return view('auth.register', ['goto_payment' => true, 'price' => $price]);
	        } else {
	        	Auth::guard($this->getGuard())->login($this->create($request->all()));
	        }
        } else {
	        	Auth::guard($this->getGuard())->login($this->create($user_details));
                $user_group = User_group::find($user_details['user_type']);
	        	Auth::user()->subscription($user_group->sub_role)->create($request->get('stripeToken'), [
				    'email' => $user_details['email'], 
				    'description' => $user_group->name . ' ' . $user_group->sub_role
				]);
        }
        return redirect($this->redirectPath());
    }


    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function getRegister()
    {
        return $this->showRegistrationForm();
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        $user_groups_by_name = User_group::where('has_sub_role', 1)->groupBy('name')->get()->toArray();
        $user_groups = [];
        foreach ($user_groups_by_name as $key => $value) {
            $user_groups_obj = User_group::where([
                'has_sub_role'=> 1, 
                'name' => $value['name']
            ])->get()->toArray();
            $user_groups[$value['name']] = [];
            foreach ($user_groups_obj as $ke => $val) {
                $user_groups[$value['name']][] = $val;
            }
            
        }
        return view('auth.register', compact('user_groups'));
    }

    

    public function logintest() {
    	dd(\Route::getRoutes()->getRoutes()[0]->getMethods());
    	\Stripe\Stripe::setApiKey(\Config::get('services.stripe.secret'));
    	$plan = \Stripe\Plan::all();
    	dd($plan['data']);
    	dd('login');
    }

}

// http://code.tutsplus.com/tutorials/how-to-accept-payments-with-stripe--pre-80957

// https://tuts.codingo.me/create-stripe-checkout-form-in-laravel

