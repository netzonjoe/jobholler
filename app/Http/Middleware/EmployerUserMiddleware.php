<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
class EmployerUserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            $user_type = Auth::user()->userGroup()->get()->toArray()[0]['associated_access'];
            switch (strtolower($user_type)) {
                case 'administrator':
                    return redirect(action('AdminDashboardController@index'));
                    break;
                
                case 'candidate':
                    return redirect(action('CandidateDashboardController@index'));
                    break;

                case 'employer':
                    return $next($request);
                    break;
            }
        }
        return redirect('/');
    }
}
