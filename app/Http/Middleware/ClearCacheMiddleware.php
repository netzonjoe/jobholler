<?php

namespace App\Http\Middleware;

use Closure;

class ClearCacheMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {  
        $response = $next($request);
        $response->header("Cache-Control","no-cache");
        $response->header("Pragma", "no-cache"); //HTTP 1.0
        $response->header("Expires"," Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
        return $next($request);
    }
}
