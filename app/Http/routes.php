<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/




Route::group(['middleware' => ['web']], function () {

    Route::get('/', function () {
        return view('welcome');
    });
});



Route::group(['middleware' => ['web']], function () {



	Route::get('/list_user_group', 'SystemUserController@listUserGroup');

	Route::post('/delete_user_group', 'SystemUserController@deleteUserGroup');

	Route::get('/add_user_group', 'SystemUserController@addUserGroup');
	Route::post('/add_user_group', 'SystemUserController@saveUserGroup');

	Route::get('/edit_user_group', 'SystemUserController@editUserGroup');
	Route::post('/edit_user_group', 'SystemUserController@saveEditUserGroup');

    Route::get('/list_user', 'SystemUserController@listUser');
    Route::get('/add_user', 'SystemUserController@addUser');



    Route::auth();
    Route::get('/register', 'CustomUserAuthController@getRegister');
    Route::post('/register', 'CustomUserAuthController@postRegister');
    Route::get('/home', 'HomeController@index');

    Route::get('/employer', 'GuestUserController@employer');
    
    Route::get('/submitvacancy', 'GuestUserController@submitvacancy');
    Route::post('/submitvacancy', 'GuestUserController@postSubmitvacancy');

    Route::get('/checkout/amplify', 'GuestUserController@checkoutAmplify');
    Route::post('/checkout/amplify', 'GuestUserController@postCheckoutAmplify');
    Route::put('/checkout/amplify', 'GuestUserController@postAjaxCheckCouponAmplify');

    Route::get('/checkout/ignite', 'GuestUserController@checkoutIgnite');
    Route::post('/checkout/ignite', 'GuestUserController@postCheckoutIgnite');
    Route::put('/checkout/ignite', 'GuestUserController@postAjaxCheckCouponIgnite');

    Route::get('/test-mail', 'CandidateDashboardController@testmail');

    Route::get('/job-listing/getAllPostedJobs', 'AdminDashboardController@getAllPostedJobs');


    Route::group(['middleware' => 'admin_user'] , function () {

        Route::get('/admin-dashboard', 'AdminDashboardController@index');
        
        
        Route::post('/admin-dashboard/get-member', 'AdminDashboardController@getAllUsersWithUserGroupAjax');
        Route::post('/admin-dashboard/create-member', 'AdminDashboardController@createMember');
        Route::post('/admin-dashboard/update-member', 'AdminDashboardController@updateMember');
        Route::post('/admin-dashboard/delete-member', 'AdminDashboardController@deleteMember');
        Route::post('/admin-dashboard/delete-member', 'AdminDashboardController@deleteMember');
        Route::post('/admin-dashboard/update-member-status', 'AdminDashboardController@updateMemberStatus');

        Route::post('/admin-dashboard/create-job', 'AdminDashboardController@createJob');
        Route::post('/admin-dashboard/delete-job', 'AdminDashboardController@deleteJob');
        Route::post('/admin-dashboard/update-job', 'AdminDashboardController@updateJob');
        Route::post('/admin-dashboard/clone-job', 'AdminDashboardController@cloneJob');
        Route::post('/admin-dashboard/get-all-jobs', 'AdminDashboardController@getAllPostedJobsAjax');
        Route::post('/admin-dashboard/get-all-expired-jobs', 'AdminDashboardController@getAllExpiredJobsAjax');
        Route::post('/admin-dashboard/repost-job', 'AdminDashboardController@repostJob');
        Route::post('/admin-dashboard/get-all-companies', 'AdminDashboardController@getAllCompaniesAjax');
        

        
        Route::post('/admin-dashboard/create-product', 'AdminDashboardController@createProduct');
        Route::post('/admin-dashboard/delete-product', 'AdminDashboardController@deleteProduct');
        Route::post('/admin-dashboard/update-product', 'AdminDashboardController@updateProduct');

        Route::post('/admin-dashboard/get-user-groups', 'AdminDashboardController@getUserGroups');
        
        Route::post('/admin-dashboard/get-billing-credentials', 'AdminDashboardController@getBillingCredentials');
        Route::post('/admin-dashboard/save-billing-credentials', 'AdminDashboardController@saveBillingCredentials');
        
        Route::post('/admin-dashboard/update-admin-profile', 'AdminDashboardController@updateAdminProfile');
        Route::post('/admin-dashboard/get-admin-profile-details', 'AdminDashboardController@getAdminProfileDetails');

        Route::post('/admin-dashboard/get-users-without-resume', 'AdminDashboardController@getUsersWithoutResumeAjax');
        Route::post('/admin-dashboard/create-resume', 'AdminDashboardController@createResume');
        Route::post('/admin-dashboard/get-users-with-resume', 'AdminDashboardController@getUsersWithResumeAjax');
        Route::post('/admin-dashboard/update-user-with-resume-status', 'AdminDashboardController@updateUserWithResumeStatus');
        Route::post('/admin-dashboard/update-user-with-resume-viewable', 
                    'AdminDashboardController@updateUserWithResumeViewable');
        Route::post('/admin-dashboard/delete-user-with-resume', 'AdminDashboardController@deleteUserWithResume');
        Route::post('/admin-dashboard/update-resume', 'AdminDashboardController@updateResume');

        Route::post('/admin-dashboard/create-coupon', 'AdminDashboardController@createCoupon');
        Route::post('/admin-dashboard/get-all-coupon', 'AdminDashboardController@getAllCoupon');
        Route::post('/admin-dashboard/delete-coupon', 'AdminDashboardController@deleteCoupon');
        Route::post('/admin-dashboard/update-coupon', 'AdminDashboardController@updateCoupon');

        Route::post('/admin-dashboard/get-code-google-analytics', 'AdminDashboardController@getCodeGoogleAnalytics');
        Route::post('/admin-dashboard/save-code-google-analytics', 'AdminDashboardController@saveCodeGoogleAnalytics');

        Route::post('/admin-dashboard/get-all-notifications', 'AdminDashboardController@getAllNotificationsAjax');
        Route::post('/admin-dashboard/update-all-notifications-to-on-clicked', 
            'AdminDashboardController@updateAllNotificationOnClicked');
        Route::post('/admin-dashboard/update-all-notifications-to-on-viewed', 
            'AdminDashboardController@updateAllNotificationOnViewed');
        Route::post('/admin-dashboard/show-all-notifications-to-on-viewed', 
            'AdminDashboardController@showAllNotificationOnViewed');
        Route::post('/admin-dashboard/hide-all-notifications-to-on-viewed', 
            'AdminDashboardController@hideAllNotificationOnViewed');
        
        Route::post('/admin-dashboard/create-branding', 'AdminDashboardController@createBranding');
        Route::post('/admin-dashboard/update-branding', 'AdminDashboardController@updateBranding');
        Route::post('/admin-dashboard/delete-branding', 'AdminDashboardController@deleteBranding');
        Route::post('/admin-dashboard/get-all-brandings', 'AdminDashboardController@getAllBrandingsAjax');
        
        Route::post('/admin-dashboard/get-company-profile', 'AdminDashboardController@getCompanyProfileAjax');

        // Route::post('/admin-dashboard/create-resume', 'AdminDashboardController@getUsersWithResumeAjax');

        

        
        // Route::get('/admin-dashboard/getAllPostedJobs', 'AdminDashboardController@getAllPostedJobs');
    });


    Route::group(['middleware' => 'candidate_user'] , function () {

        Route::get('/candidate-dashboard', 'CandidateDashboardController@index');

        Route::post('/candidate-dashboard/get-resume-by-user', 'CandidateDashboardController@getResumeByUserAjax');
        Route::post('/candidate-dashboard/create-resume', 'CandidateDashboardController@createResume');
        Route::post('/candidate-dashboard/update-resume', 'CandidateDashboardController@updateResume');

        Route::post('/candidate-dashboard/get-all-jobs-applied', 'CandidateDashboardController@getAllJobsApliedAjax');
    });

    Route::group(['middleware' => 'employer_user'] , function () {

        Route::get('/employer-dashboard', 'EmployerDashboardController@index');

        Route::post('/employer-dashboard/create-job', 'EmployerDashboardController@createJob');
        Route::post('/employer-dashboard/delete-job', 'EmployerDashboardController@deleteJob');
        Route::post('/employer-dashboard/update-job', 'EmployerDashboardController@updateJob');
        Route::post('/employer-dashboard/clone-job', 'EmployerDashboardController@cloneJob');

        Route::post('/employer-dashboard/get-all-jobs', 'EmployerDashboardController@getAllPostedJobsAjax');
        Route::post('/employer-dashboard/get-all-expired-jobs', 'EmployerDashboardController@getAllExpiredJobsAjax');
        Route::post('/employer-dashboard/repost-job', 'EmployerDashboardController@repostJob');

        Route::post('/employer-dashboard/save-company-profile', 'EmployerDashboardController@saveCompanyProfile');
        Route::post('/employer-dashboard/get-company-profile', 'EmployerDashboardController@getCompanyProfileAjax');
        
        Route::post('/employer-dashboard/save-employer-account', 'EmployerDashboardController@saveEmployerAccount');
        Route::post('/employer-dashboard/get-employer-account', 'EmployerDashboardController@getEmployerAccountAjax');
        Route::post('/employer-dashboard/get-all-brandings', 'EmployerDashboardController@getAllBrandingsAjax');
        Route::post('/employer-dashboard/purchase-branding', 'EmployerDashboardController@purchaseBranding');
        Route::get('/employer-dashboard/purchase-brandingasdasd', 'EmployerDashboardController@sendEmailPurchaseBranding');
        
        

    });


    // /comapany-name
    // /company-name/jobtitle/id
    // /fullname/resume/id   
    Route::get('/{fullname}/resume/{resume_id}', 'GuestUsersController@resumeDetails'); 
    Route::get('/{company_name_slug}', 'GuestUsersController@jobsListing'); 
    Route::get('/{company_name_slug}/{job_title}/{job_id}', 'GuestUsersController@jobDetails'); 
    Route::get('/{fullname}/resume/{resume_id}/download', 'GuestUsersController@downloadResumeDocumentFile'); 
    Route::post('/guest-or-candidate-job-apply', 'GuestUsersController@jobApply'); 
   
});




// Route::get('/job-listing/{job_title}/{job_post_id}', 'GuestUserController@jobsListing'); 
// Route::get('/resume-listing/{full_name}/{resume_id}', 'GuestUserController@resumeListing'); 
