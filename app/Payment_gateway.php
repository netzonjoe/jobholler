<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Payment_gateway extends Model {

	protected $table = 'payment_gateways';
	public $timestamps = true;
	protected $fillable = array('name', 'test_key', 'test_secret', 'live_key', 'live_secret');


	public static function getPaymentKey($name, $key) {

		if (env('APP_ENV') === 'local') {
			$key_attribute = "test_$key";
			return DB::table('payment_gateways')
            ->where('name', $name)
            ->first()->$key_attribute;
		} else {
			$key_attribute = "live_$key";
			return DB::table('payment_gateways')
            ->where('name', $name)
            ->first()->$key_attribute;
		}
	}
}