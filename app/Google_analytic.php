<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Google_analytic extends Model {

	protected $table = 'google_analytics';
	public $timestamps = true;
	protected $fillable = array('code');

}