<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branding extends Model {

	protected $table = 'brandings';
	public $timestamps = true;
	protected $fillable = array('title', 'name', 'status', 'description', 'price', 'per_payment_type', 'associated_access', 'payment_plan_type');

}