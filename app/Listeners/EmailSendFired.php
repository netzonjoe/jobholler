<?php

namespace App\Listeners;

use App\Events\ActionEmailSender;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;
use Config;
use App\Notification;
use Acme\GlobalCtrlHelper;
// use Pusher;
use Event;


class EmailSendFired
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  ActionEmailSender  $event
     * @return void
     */
    public function handle(ActionEmailSender $event)
    {   
        $data = $event->data;
        switch ($data['template']) {
            case 'job-posting':
                Mail::send('email.job-posting', $data, function($message) use ($data)
                {   
                    $message->from(Config::get('mail.from.address'), 'Job Posting');
                    $message->to($data['admin_email'])->cc('kudosableapps@gmail.com')->subject('Job Posting!');
                }); 
                break;

            case 'purchase-branding':
                Mail::send('email.purchase-branding', $data, function($message) use ($data)
                {   
                    $message->from(Config::get('mail.from.address'), 'Purchase Branding');
                    $message->to($data['admin_email'])->cc('kudosableapps@gmail.com')->subject('Purchase Branding!');
                    $data['title_notification'] = 
                        'New Purchased branding ' . $data['title'] . ' from ' . $data['from_user_email'] . '.';
                    (new Notification([
                        'user_id' => $data['user_id'],
                        'data' => serialize($data)
                    ]))->save();

                    $notifications = 
                        Notification::where('user_id', $data['user_id'])
                                    ->where('is_viewed', false)->orderBy('created_at', 'desc')->get()->toArray();

                    /*foreach ($notifications as $key => $value) {
                        $value['data'] = unserialize($value['data']);
                        $value['time_ago'] = GlobalCtrlHelper::time_elapsed_string(strtotime($value['created_at']));
                        $notifications[$key] = $value;
                    }*/

                    /*$pusher = new Pusher(
                        Config::get('services.pusher.key'),
                        Config::get('services.pusher.secret'),
                        Config::get('services.pusher.app_id'),
                        ['encrypted' => true]
                    );*/
                    /* uncomment header.html on admindashboard */
                    /*$pusher->trigger('purchase_branding_channel', 'purchase_branding_event', $notifications);*/

                }); 
                break;
            case 'create-member':
                Mail::send('email.create-member', $data, function($message) use ($data)
                {   
                    $message->from(Config::get('mail.from.address'), 'Member is created');
                    $message->to($data['email'])->cc('kudosableapps@gmail.com')->subject('Member is created!');
                });
            break; 
            case 'update-member':
                Mail::send('email.update-member', $data, function($message) use ($data)
                {   
                    $message->from(Config::get('mail.from.address'), 'Member is Updated');
                    $message->to($data['email'])->cc('kudosableapps@gmail.com')->subject('Member is Updated!');
                });
            break;
            case 'job-apply':
                Mail::send('email.job-apply', $data, function($message) use ($data)
                {   
                    $message->from(Config::get('mail.from.address'), 'Job was applied successfully.');
                    $message->to($data['admin_email'])->cc('kudosableapps@gmail.com')->subject('Job applied!');
                });
            break;
            default:
                # code...
                break;
        }


        
    }
}
