<?php namespace Acme;

use Response;
/**
* 
*/
class Rest 
{
	public static function success($data = array()) {
		return Response::json(['_token' => csrf_token(), 'type' => 'success'] + $data);
	}

	public static function warning($data = array()) {
		return Response::json(['_token' => csrf_token(), 'errors' => $data + ['type' => 'warning']], 400);

	}

	public static function failed($validator) {
		return Response::json(['_token' => csrf_token(), 'errors' => $validator->getMessageBag()->toArray()], 400);
	}

	public static function dataExist($data = array()) {
		return Response::json(['_token' => csrf_token(), 'errors' => $data], 400);
	}
}