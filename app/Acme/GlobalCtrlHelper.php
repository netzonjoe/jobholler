<?php namespace Acme;

use Route;
/**
* Controller Helper
*/
class GlobalCtrlHelper 
{
	public static function getRouteList($controller_name) {
		$listRoutes = Route::getRoutes()->getRoutes();
        $onlyCtrls = ["$controller_name@"];
        $uris = [];
        $get_uris = [];
        $post_uris = [];
        foreach ($listRoutes as $key => $value) {
            $ctrl = $value->getAction();
            if (isset($ctrl['controller'])) {
                $isQualified = false;
                for ($i = 0; $i < count($onlyCtrls); $i++) {
                    if (strpos($ctrl['controller'],  '\\' . $onlyCtrls[$i])) {
                        $isQualified = true;
                        break;
                    }
                }
                if ($isQualified) {
                   $controller = explode('App\Http\Controllers\\', $ctrl['controller'])[1];
                    if (in_array('GET', $value->getMethods())) {
                        $uris['GET'][(string)$controller] = $value->getUri();

                    } else if (in_array('POST', $value->getMethods())) {
                        $uris['POST'][(string)$controller] = $value->getUri();
                    }
                }
            }
        }
        return $uris;
	}

	public static function getAngularDirectiveAlert() {
		return '<uib-alert on-ready-state style="display: none;" ng-repeat="alert in $alerts" type="{{alert.type}}" close="closeAlert($index)">{{alert.msg}}</uib-alert>';
	}


    public static  function time_elapsed_string($ptime) {
        $etime = time() - $ptime;

        if ($etime < 1)
        {
            return '0 seconds';
        }

        $a = array( 365 * 24 * 60 * 60  =>  'year',
                     30 * 24 * 60 * 60  =>  'month',
                          24 * 60 * 60  =>  'day',
                               60 * 60  =>  'hour',
                                    60  =>  'minute',
                                     1  =>  'second'
                    );
        $a_plural = array( 'year'   => 'years',
                           'month'  => 'months',
                           'day'    => 'days',
                           'hour'   => 'hours',
                           'minute' => 'minutes',
                           'second' => 'seconds'
                    );

        foreach ($a as $secs => $str)
        {
            $d = $etime / $secs;
            if ($d >= 1)
            {
                $r = round($d);
                return $r . ' ' . ($r > 1 ? $a_plural[$str] : $str) . ' ago';
            }
        }
    }
}