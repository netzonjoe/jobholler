<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resume extends Model {

	protected $table = 'resumes';
	public $timestamps = true;

	protected $fillable = array('user_id', 'status', 'viewable','full_name', 'tagline', 'about_me', 'profile_photo', 'years_of_experience', 'expected_salary', 'personal_core_skills', 'technical_core_skills', 'hobbies', 'educations', 'experiences', 'resume_document', 'cover_photo', 'cover_photo_selection');

	/**
     * Get the user details associated with the resume record.
     */
    public function user()
    {
        return $this->hasOne('App\User');
    }

}