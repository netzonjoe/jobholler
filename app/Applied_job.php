<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Applied_job extends Model {

	protected $table = 'applied_jobs';
	public $timestamps = true;
	protected $fillable = array('firstname', 'lastname', 'email', 'linkedin_profile', 'curriculum_vitae', 'cover_leter', 'jobs_id', 'user_id', 'status_request');

}