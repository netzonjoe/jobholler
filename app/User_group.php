<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_group extends Model {

	protected $table = 'user_groups';
	public $timestamps = true;
    protected $fillable = array('name', 'status', 'sku', 'associated_access', 'price', 'payment_plan_type', 'description', 'attributes', 'purchase_link');

	 /**
     * Get the the User_permission for the User_group.
     */
    public function userPermission()
    {
        return $this->hasMany('App\User_permission');
    }

     /**
     * Get the the User details for the User_group.
     */
    public function userDetails()
    {
        return $this->hasMany('App\User');
    }

    /*
     * Makes a MongoID like Key value.
     * Based off time, machine Id, Pid and a sequence no.
     * From Stackoverflow: http://stackoverflow.com/questions/24355927/which-algorithm-does-mongodb-use-for-id
     */
    static public function MakeAttributeId($yourTimestamp = null)
    {
        static $inc = 0;
        $id = '';

        if ($yourTimestamp === null) {
            $yourTimestamp = time();
        }
        $ts = pack('N', $yourTimestamp);
        $m = substr(md5(gethostname()), 0, 3);
        $pid = pack('n', posix_getpid());
        $trail = substr(pack('N', $inc++), 1, 3);
        $bin = sprintf("%s%s%s%s", $ts, $m, $pid, $trail);

        for ($i = 0; $i < 12; $i++) {
            $id .= sprintf("%02X", ord($bin[$i]));
        }

        return $id;
    }

}