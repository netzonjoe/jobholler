@extends('layouts.admin-dashboard')

@section('content')

<div id="content" class="content-container" ng-controller="JobhollerCtrl">
  {!! html_entity_decode($angular_directive_alert) !!}
  <section data-ng-view class="view-container [[main.pageTransition.class]]"></section>
</div>

@endsection


@section('scripts-files')

<script src="http://maps.googleapis.com/maps/api/js?sensor=false&libraries=places" type="text/javascript"></script>
<script type="text/javascript">
	
(function () {
	$user_groups = JSON.parse('{!!$user_groups!!}');
	$users = JSON.parse('{!!$users!!}');
	$route_list = JSON.parse('{!!$route_list!!}');
	$_token = '{{$_token}}';
	$baseurl = '{{$baseurl}}';
	$jobs = []; //JSON.parse('{!!$jobs!!}');
	$job_listing = '{{$job_listing}}';
	$resume_listing = '{{$resume_listing}}';
	$products = JSON.parse('{!!$products!!}');
	$imagesDir = '{{$imagesDir}}';
	$users_without_resume = JSON.parse('{!!$users_without_resume!!}');
	$users_with_resume = JSON.parse('{!!$users_with_resume!!}');
	$expired_jobs = []; //JSON.parse('{!!$expired_jobs!!}');
	$pusher_key = '{{$pusher_key}}';
})();

</script>

@endsection