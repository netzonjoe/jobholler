<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="{{ asset('/css/libs/font-awesome/font-awesome.min.css') }}" rel='stylesheet' type='text/css'>
    <link href="{{ asset('/css/libs/font-awesome/fonts.googleapis.css') }}" rel='stylesheet' type='text/css'>
    
    <!-- Styles -->
    <link href="{{ asset('/css/libs/bootstrap/bootstrap.min.css') }}" rel="stylesheet">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/temp/guest.layout.css') }}">
    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }

    </style>
</head>
<body id="app-layout">

        @include('notifications.simple_flash_message', array('item' => 'test'))    

    <div id="DIV_1">
        <div id="DIV_2">
             <!-- HEADER SECTION -->
            <header id="HEADER_3">
                <div id="DIV_4">
                    <div id="DIV_5">
                        <div id="DIV_6">
                        </div>
                        <div id="DIV_7">
                            <div id="DIV_8">
                                <nav id="NAV_9">
                                    <ul id="UL_10">
                                        <li id="LI_11">
                                            <a href="http://jobhollernew.staging.wpengine.com/about/job-holler/" class="navigation_link" id="A_12">About</a>
                                        </li>
                                        <li id="LI_13">
                                            <a href="http://jobhollernew.staging.wpengine.com/employer/" class="navigation_link" id="A_14">Employer</a>
                                        </li>
                                        <li id="LI_15">
                                            <a href="http://jobhollernew.staging.wpengine.com/social/" class="navigation_link" id="A_16">Social</a>
                                        </li>
                                        <li id="LI_17">
                                            <a href="http://jobhollernew.staging.wpengine.com/login/" class="navigation_link" id="A_18"><i id="I_19"></i>Login</a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                            <div id="DIV_20">
                                <div id="DIV_21">
                                    <div id="DIV_22">
                                    </div>
                                    <div id="DIV_23">
                                    </div>
                                    <div id="DIV_24">
                                    </div>
                                </div>
                            </div>
                            <div id="DIV_25">
                                 <a href="http://jobhollernew.staging.wpengine.com/" title="Job Holler" id="A_26"><img alt="Job Holler" src="http://jobhollernew.staging.wpengine.com/wp-content/uploads/2016/03/Jobholler-top3.png" id="IMG_27" /><img alt="Job Holler" src="http://jobhollernew.staging.wpengine.com/wp-content/uploads/2016/03/Jobholler-top3.png" id="IMG_28" /><img alt="Job Holler" src="http://jobhollernew.staging.wpengine.com/wp-content/uploads/2016/03/Jobholler-top3.png" id="IMG_29" /></a>
                            </div>
                            <div id="DIV_30">
                            </div>
                        </div>
                        <div id="DIV_31">
                        </div>
                    </div>
                </div>
                <div id="DIV_32">
                </div>
                <div id="DIV_33">
                </div>
                <div id="DIV_34">
                </div>
                <div id="DIV_35">
                </div>
                <div id="DIV_36">
                </div>
            </header>
             <!-- END HEADER SECTION -->

             @yield('content')

             <!-- FOOTER SECTION -->
            <section id="SECTION_305">
                <div id="DIV_306">
                    <div id="DIV_307">
                        <div id="DIV_308">
                            <section id="SECTION_309">
                                <div id="DIV_310">
                                    <img src="http://jobhollernew.staging.wpengine.com/wp-content/uploads/2016/03/Jobholler-large.png" id="IMG_311" alt='' />
                                </div>
                            </section>
                        </div>
                        <div id="DIV_312">
                            <section id="SECTION_313">
                                <div id="DIV_314">
                                    Tweet Tweet...
                                </div>
                                <div id="DIV_315">
                                    <ul id="UL_316">
                                        <li id="LI_317">
                                             <span id="SPAN_318">#Weekend is here! #LovelyTime https://t.co/yAeMquuwsd</span> <a href="http://twitter.com/JobHoller/statuses/715947157866590208" id="A_319">3 days ago</a>
                                        </li>
                                        <li id="LI_320">
                                             <span id="SPAN_321">Frustrated in your current #IT #Job? Check out these new jobs at <a href="http://twitter.com/ps_software" id="A_322">@ps_software</a> in #Rotherham! https://t.co/XwkiKs8nA0 https://t.co/EkycuxrQba</span> <a href="http://twitter.com/JobHoller/statuses/715919240474140673" id="A_323">3 days ago</a>
                                        </li>
                                    </ul>
                                </div>
                            </section>
                        </div>
                        <div id="DIV_324">
                            <section id="SECTION_325">
                                <div id="DIV_326">
                                    Contact Us…
                                </div>
                                <div id="DIV_327">
                                    Email: info@jobholler.com<br id="BR_328" /> Telephone: 01244 567 967<br id="BR_329" /> Registered in England<br id="BR_330" /> No. 7977871<br id="BR_331" />
                                </div>
                            </section>
                            <section id="SECTION_333">
                                <div id="DIV_334">
                                    Connect with us…
                                </div>
                                <div id="DIV_335">
                                    <a href="https://plus.google.com/u/0/b/118426094916294904473/118426094916294904473/" id="A_336"><img src="http://jobhollernew.wpengine.com/wp-content/uploads/2016/01/Googleplus.png" alt="Googleplus" width="37" height="37" id="IMG_337" /></a><a href="https://twitter.com/JobHoller" id="A_338"><img src="http://jobhollernew.wpengine.com/wp-content/uploads/2016/01/twitter.png" alt="twitter" width="37" height="37" id="IMG_339" /></a><a href="https://www.facebook.com/jobholler" id="A_340"><img src="http://jobhollernew.wpengine.com/wp-content/uploads/2016/01/facebook.png" alt="facebook" width="37" height="37" id="IMG_341" /></a><a href="https://www.linkedin.com/company/job-holler" id="A_342"><img src="http://jobhollernew.wpengine.com/wp-content/uploads/2016/01/linkedin.png" alt="linkedin" width="37" height="37" id="IMG_343" /></a><a href="https://www.instagram.com/jobholler/" id="A_344"><img src="http://jobhollernew.staging.wpengine.com/wp-content/uploads/2016/03/instagram.png" alt="Instagram" width="37" height="37" id="IMG_345" /></a>
                                </div>
                            </section>
                        </div>
                        <div id="DIV_346">
                        </div>
                    </div>
                </div>
                <div id="DIV_347">
                    <div id="DIV_348">
                         <span id="SPAN_349">Copyright All Rights Reserved © 2016</span>
                    </div>
                    <div id="DIV_350">
                    </div>
                </div>
            </section>
            <!-- END FOOTER SECTION -->
        </div>
    </div>

    <!-- JavaScripts -->
    <script src="{{ asset('/js/libs/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('/js/libs/bootstrap/bootstrap.min.js') }}"></script>
    <script src="{{ asset('/js/libs/bootstrap/bootstrap-formhelpers.min.js') }}"></script>
    <script src="{{ asset('/js/libs/angular/angular.js') }}"></script>

    @yield('scripts-files')
    
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
</body>
</html>
