<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>{{ $company['company_name'] }} - Job Holler</title>

    @if($company['company_icon'] != '')
        <link rel="shortcut icon" href="{{ $company['company_icon'] }}" type="ico/image/png"/>
    @else
        <link rel="shortcut icon" href="{{ asset('favicon.ico')}}" type="ico/image/png"/>
    @endif

    <!--[if lt IE 9]>
    <script src="{{ asset('frontend/ie/html5shiv.js') }}" type="text/javascript"></script>
    <link rel='stylesheet' href='{{ asset('frontend/ie/ie.css') }}'/>
    <![endif]-->

    <!--[if IE 9]>
    <script src="{{ asset('frontend/ie/placeholder.js') }}" type="text/javascript"></script>
    <![endif]-->

    <!--[if IE 7 ]>
    <link href="{{ asset('frontend/ie/ie7.css') }}" media="screen" rel="stylesheet"
          type="text/css"/>
    <![endif]-->
    <!--[if IE 8 ]>
    <link href="{{ asset('frontend/ie/ie8.css') }}" media="screen" rel="stylesheet"
          type="text/css"/>
    <![endif]-->

    <!--[if lte IE 8]>
    <script type="text/javascript" src="{{ asset('frontend/ie/respond.js') }}"></script>
    <![endif]-->


    <!-- icon picker -->
    <!-- <link rel="stylesheet" href="{{ asset('/magic/client/bower_components/bootstrap-iconpicker/bootstrap-3.2.0/css/docs.css') }}"/> -->
    <link rel="stylesheet" href="{{ asset('/magic/client/bower_components/bootstrap-iconpicker/bootstrap-3.2.0/css/pygments-manni.css') }}"/>
    <link rel="stylesheet" href="{{ asset('/magic/client/bower_components/bootstrap-iconpicker/icon-fonts/elusive-icons-2.0.0/css/elusive-icons.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('/magic/client/bower_components/bootstrap-iconpicker/icon-fonts/elusive-icons-2.0.0/css/elusive-icons.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('/magic/client/bower_components/bootstrap-iconpicker/icon-fonts/elusive-icons-2.0.0/css/elusive-icons.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('/magic/client/bower_components/bootstrap-iconpicker/icon-fonts/font-awesome-4.2.0/css/font-awesome.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('/magic/client/bower_components/bootstrap-iconpicker/icon-fonts/ionicons-1.5.2/css/ionicons.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('/magic/client/bower_components/bootstrap-iconpicker/icon-fonts/map-icons-2.1.0/css/map-icons.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('/magic/client/bower_components/bootstrap-iconpicker/icon-fonts/material-design-1.1.1/css/material-design-iconic-font.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('/magic/client/bower_components/bootstrap-iconpicker/icon-fonts/octicons-2.1.2/css/octicons.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('/magic/client/bower_components/bootstrap-iconpicker/icon-fonts/typicons-2.0.6/css/typicons.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('/magic/client/bower_components/bootstrap-iconpicker/icon-fonts/weather-icons-1.2.0/css/weather-icons.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('/magic/client/bower_components/bootstrap-iconpicker/bootstrap-iconpicker/css/bootstrap-iconpicker.min.css') }}"/>
    <!-- #icon-picker -->


    <script src="{{ asset('frontend/js/jquery.min.js') }}"></script>
    <script src="{{ asset('frontend/js/jquery.scrollUp.min.js') }}"></script>

    <link rel="stylesheet" href="{{ asset('frontend/css/bootstrap.min.css') }}">

    <link rel="stylesheet" href="{{ asset('frontend/css/font-awesome.min.css') }}">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,700,800&subset=latin,latin-ext'
          rel='stylesheet' type='text/css'>

    <link id="scrollUpTheme" rel="stylesheet" href="{{ asset('frontend/css/scrollup/themes/pill.css') }}">

    <link rel="stylesheet" href="{{ asset('frontend/plugins/venobox/venobox.css') }}" type="text/css" media="screen"/>

    <link rel="stylesheet" href="{{ asset('frontend/css/style.css') }}">

</head>
<body>

<header class="company-header" style="background-color: {{ $company['company_color'] }};">
    <div class="container">
        <!--<div class="row">-->
        <div class="col-sm-2">

        </div>
        <div class="col-sm-7">
            <div class="text-center">
                @if (isset($company['company_logo']) && !empty($company['company_logo'])) 
                    <img src="{{ $company['company_logo'] }}" alt=""
                         class="company-logo">
                @endif

                <h3> {{ $company['tagline']}} </h3>
            </div>
        </div>
        <div class="col-sm-3">
            <a href="{{asset('/')}}" target="_blank" class="powered-by mk-image-shortcode-link">
                <img
                        class="lightbox-false"
                        alt="" title=""
                        src="{{ asset('img/poweredby-white.png') }}">
            </a>
        </div>
        <!--</div>-->

    </div>
</header>

<section id="company-description">
    <div class="container">
        <div class="row">
            <div class="col-sm-8 description">
                <h2 class="company-title">{{ $company['company_name'] }}</h2>

                <div class="content">
                    {!! $company['company_description'] !!}
                </div>

                <ul class="social-media text-right">

                    @foreach ($company['social_networks'] as $social)
                        @if($social[0]['value'] != '')
                            <a href="{{ $social[1]['value'] }}" target="_blank"></a>
                            <li>
                                <a href="{{ $social[1]['value'] }}" target="_blank"> 
                                    <i class="{{ $social[0]['value'] }}" style="background-color: {{$company['company_color']}}"></i>
                                </a>
                            </li>
                        @endif
                    @endforeach  

                    @if(isset($company['company_website']))
                        <a href="{{ $company['company_website'] }}" target="_blank"></a>
                        <li>
                            <a href="{{ $company['company_website'] }}" target="_blank"> 
                                <i class="fa fa-link" style="background-color: {{$company['company_color']}}"></i>
                            </a>
                        </li>  
                    @endif
                                

                </ul>

            </div>
            <div class="col-sm-4 perks">
                <h2 class="title">
                    The Perks...
                </h2>

                <div class="content">
                    <ul>
                        @foreach ($company['company_perks'] as $perk)
                            @if($perk[0]['value'] != '')
                                <li>
                                    <div class="perks-icon-wrapper">
                                        <i class="{{ $perk[2]['value'] }}"></i>
                                    </div>
                                    {{ $perk[0]['value'] }}
                                </li>
                            @endif
                        @endforeach
                    </ul>
                </div>

                <div class="links row">
                    @if ($company['company_video'] !== '') 
                        <a href="{{ $company['company_video'] }}" class="view-video btn venobox col-sm-6" style="margin-left: 15px;width: 44.99999%;"
                           data-type="youtube">View Company Video</a>
                        <a style="background: {{ $company['company_color'] }}; border-color: {{ $company['company_color'] }};width: 44.99999%;margin-left: 7px;"  
                           href="#the-perks" onclick="openVenobox()" class="view-perks btn venobox-inline col-sm-6" data-type="inline">View All The
                            Perks</a>
                    @else 
                        <a style="background: {{ $company['company_color'] }}; border-color: {{ $company['company_color'] }};width: 89.99998%;margin-left: 7px;"  
                           href="#the-perks" onclick="openVenobox()" class="view-perks btn venobox-inline col-sm-6" data-type="inline">View All The
                            Perks</a>
                    @endif
                    
                </div>
            </div>
        </div>
    </div>
</section>

<!--<section id="job-lists">
    <div class="container">
        <div class="row job-list">
            <div class="col-sm-3 job-title">
                <div class="thumb">
                    <img src="http://jobholler.com/wp-content/uploads/2015/08/Wrten-icon.jpg" alt=""
                         class="company-thumb">
                </div>
                <div class="title">
                    <h2>UX / UI Designer</h2>
                </div>
            </div>

            <div class="col-sm-3 job-skills">
                <ul class="skills-column left">
                    <li>Web</li>
                    <li>Information Architect</li>
                    <li>eCommerce</li>
                </ul>
                <ul class="skills-column right">
                    <li>Wireframing</li>
                    <li>Storyboarding</li>
                    <li>Creative</li>
                </ul>
            </div>

            <div class="col-sm-2">
                <div class="experience">
                    <img src="http://jobholler.com/wp-content/themes/jupiter-child/images/meter2.png" alt=""
                         class="meter" style="background: #2bb673">
                    <span class="exp-title">Skill</span>
                </div>
            </div>
        </div>
    </div>
</section>-->

<section id="job-lists">
    <div class="container">
        <div class="row">
            <ul class="job-lists company-profile">
                 @foreach ($jobs as $job)
                    <a href="{{$company['job_link'] .'/' . $job['job_title'] . '/' . $job['id'] }}">
                        <li class="job-list">
                            <div class="logo-thumb"><img
                                    src="{{ $company['company_icon'] }}" alt=""
                                    class="company-thumb"></div>
                            <div class="job-title"><h2> {{ $job['job_title'] }}</h2></div>
                            <div class="job-skills">
                                @if (count($job['skills']) <= 8)
                                    <ul class="left">
                                    @foreach ($job['skills']  as $index => $skill)
                                        @if ($index < 4)
                                            <li>{{$skill['name']}}</li>
                                        @endif
                                    @endforeach
                                    </ul>
                                    <ul class="right">
                                    @foreach ($job['skills']  as $index => $skill)
                                        @if ($index >= 4)
                                            <li>{{$skill['name']}}</li>
                                        @endif
                                    @endforeach
                                    </ul>
                                @else

                                    <ul class="left">
                                    @foreach ($job['skills']  as $index => $skill)
                                        @if ($index < round(count($job['skills']) / 2)) 
                                            <li>{{$skill['name']}}</li>
                                        @endif
                                    @endforeach
                                    </ul>
                                    <ul class="right">
                                    @foreach ($job['skills']  as $index => $skill)
                                        @if ($index >= round(count($job['skills']) / 2)) 
                                            <li>{{$skill['name']}}</li>
                                        @endif
                                    @endforeach
                                    </ul>
                                @endif

                                
                                
                            </div>
                            <div class="experience">
                                <img src="{{ $company['meter'][$job['experience_level']] }}"
                                     alt="" class="meter" style="background: {{ $company['company_color'] }}">
                                <span class="exp-title">Skill</span>
                            </div>

                            <div class="location">
                                <i style="color: {{ $company['company_color'] }};"
                                   class="fa fa-map-marker"></i></br>
                                <p class="location">{{ $job['job_location'] }}</p>
                            </div>
                            <div class="max-salary">
                                <i style="color: {{ $company['company_color'] }};" class="fa fa-gbp"></i>

                                <p class="maximum">
                                <{{ (strlen($job['maximum_salary']) > 6) ? substr($job['maximum_salary'], 0, strlen($job['maximum_salary']) - 6) . 'k' : $job['maximum_salary'] }}</p>
                            </div>

                            <div class="meta-info">
                                <p class="job-type">{{ $job['job_type'] }}</p></br>
                                <p style="background-color: {{ $company['company_color'] }};" class="date-posted">
                                    {{$job['start_date']}}
                                </p>
                            </div>
                        </li>
                    </a>
                @endforeach

            </ul>
        </div>
    </div>
</section>



<div class="container">
    <div class="row">
        <div class="col-xs-5">
            <div id="the-perks" style="display:none;">
                <div class="perks">
                    <ul>
                        @foreach ($company['company_perks'] as $perk)
                            @if($perk[0]['value'] != '')
                                <li>
                                    <div class="perks-icon-wrapper"><i class="{{ $perk[2]['value'] }}"></i></div>
                                    <div class="perks-description-wrapper"><span>{{ $perk[0]['value'] }}</span><br>
                                        <p>
                                        {!!  $perk[1]['value'] !!}
                                        </p>
                                    </div>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                </div>

            </div>

        </div>
    </div>
</div>


<div id="powered-by-footer">
    <div class="container">
        <div class="row">
            <div class="text-center">
                <img
                        class="lightbox-false"
                        alt="" title=""
                        src="{{ asset('img/poweredby-white.png') }}">

            </div>
        </div>
    </div>
</div>
<footer style="background-color: {{ $company['company_color'] }};">
    <div class="container">
        <div class="row">
            <div class="text-center">
                @if ($company['company_logo']) 
                <img src="{{ $company['company_logo'] }}" alt=""
                     class="company-logo">
                @endif
            </div>
        </div>
    </div>
</footer>


<script src="{{ asset('frontend/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('frontend/plugins/venobox/venobox.min.js') }}"></script>

<script>
    $(function () {
        $.scrollUp({
            scrollName: 'scrollUp',
            animation: 'fade',
            scrollText: '<i class="fa fa-chevron-up"></i>'
        });

        $('.venobox').venobox();

        $('.venobox-inline').venobox({
            framewidth: '600px',        // default: ''
            frameheight: '1640px',
            border: '10px'
        });

        openVenobox = function () {
            setTimeout(function () {
                $('.vbox-inline.figlio').css({
                    height: $('.vbox-inline ul').height() + 20 + 'px'
                });
                $('.vbox-preloader').css('display', 'none');
            }, 500);
        };


    });
</script>

</body>
</html>