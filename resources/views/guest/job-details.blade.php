<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>{{ $company['company_name'] }} - Job Holler</title>

    @if($company['company_icon'] != '')
        <link rel="shortcut icon" href="{{ $company['company_icon'] }}" type="ico/image/png"/>
    @else
        <link rel="shortcut icon" href="{{ asset('favicon.ico')}}" type="ico/image/png"/>
    @endif

    <!--[if lt IE 9]>
    <script src="{{ asset('frontend/ie/html5shiv.js') }}" type="text/javascript"></script>
    <link rel='stylesheet' href='{{ asset('frontend/ie/ie.css') }}'/>
    <![endif]-->

    <!--[if IE 9]>
    <script src="{{ asset('frontend/ie/placeholder.js') }}" type="text/javascript"></script>
    <![endif]-->

    <!--[if IE 7 ]>
    <link href="{{ asset('frontend/ie/ie7.css') }}" media="screen" rel="stylesheet"
          type="text/css"/>
    <![endif]-->
    <!--[if IE 8 ]>
    <link href="{{ asset('frontend/ie/ie8.css') }}" media="screen" rel="stylesheet"
          type="text/css"/>
    <![endif]-->

    <!--[if lte IE 8]>
    <script type="text/javascript" src="{{ asset('frontend/ie/respond.js') }}"></script>
    <![endif]-->

    <script src="{{ asset('frontend/js/jquery.min.js') }}"></script>
    <script src="{{ asset('frontend/js/jquery.scrollUp.min.js') }}"></script>

    <link rel="stylesheet" href="{{ asset('frontend/css/bootstrap.min.css') }}">

    <link rel="stylesheet" href="{{ asset('frontend/css/font-awesome.min.css') }}">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,700,800&subset=latin,latin-ext'
          rel='stylesheet' type='text/css'>

    <link id="scrollUpTheme" rel="stylesheet" href="{{ asset('frontend/css/scrollup/themes/pill.css') }}">

    <link rel="stylesheet" href="{{ asset('frontend/plugins/venobox/venobox.css') }}" type="text/css" media="screen"/>

    <link rel="stylesheet" href="{{ asset('frontend/css/style.css') }}">

</head>
<body>

<header class="company-header" style="background-color: {{ $company['company_color'] }};">
    <div class="container">
        <!--<div class="row">-->
        <div class="col-sm-2">
            <a href="{{ $company['job_link'] }}" class="btn btn-bordered">
                <i class="fa fa-bullhorn"></i>
                View all jobs
            </a>
        </div>
        <div class="col-sm-7">
            <div class="text-center">
                @if (isset($company['company_logo']) && !empty($company['company_logo'])) 
                    <img src="{{ $company['company_logo'] }}" alt=""
                         class="company-logo">
                @endif

                <h3> {{ $company['tagline']}} </h3>
            </div>
        </div>
        <div class="col-sm-3">
            <a href="{{ asset('/') }}" target="_blank" class="powered-by mk-image-shortcode-link">
                <img
                        class="lightbox-false"
                        alt="" title=""
                        src="{{ asset('img/poweredby-white.png') }}">
            </a>
        </div>
        <!--</div>-->

    </div>
</header>
<div class="container">
    @include('notifications.simple_flash_message', array('item' => 'test'))    
</div>
<section id="job-heading">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <div class="title">
                    <h1>{{ $job['job_title'] }}</h1>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="social pull-right">
                    <ul>
                        @foreach ($company['social_networks'] as $social)
                            @if($social[0]['value'] != '')
                                @if (isset($social[2]['sharelink']))
                                <li class="{{ substr($social[0]['value'], 6) }}">
                                    <a href="{{ $social[2]['sharelink'] }}">
                                        <i class="{{ $social[0]['value'] }}"></i>
                                    </a>
                                </li>
                                @endif
                            @endif
                        @endforeach  
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="separator"></div>
            </div>
        </div>
    </div>
</section>


<section id="job-content">
    <div class="container">
        <div class="row">
            <aside class="col-sm-4">


                <div class="general-job-details">
                    <ul>
                        <li class="job-type">
                            <i class="fa fa-arrow-right"></i>
                            <h4>{{ ucfirst($job['job_type']) }}</h4>
                        </li>
                        <li class="job-date">
                            <p>Posted: {{$job['start_date']}}</p>
                        </li>
                        <li class="job-location" style="padding-bottom: 0px;background: {{ $company['company_color'] }};">
                            <i class="fa fa-map-marker"></i>
                            <h4 style="padding-left: 20px; margin-top: 0px; margin-bottom: 0px; padding-right: 20px;
                             width: 300px; position: relative; right: -60px; top: -25px;">
                                {{ ucfirst(explode(",",$job['job_location'])[0]) }}
                            </h4>
                        </li>
                        <li class="job-salary">
                            <i class="fa fa-gbp"></i>
                            <h4><{{ (strlen($job['maximum_salary']) > 6) ? substr($job['maximum_salary'], 0, strlen($job['maximum_salary']) - 6) . 'k' : $job['maximum_salary'] }}</h4>
                        </li>
                        @if(count($company['company_perks']) > 0)
                        <li class="job-perks" style="background: {{ $company['company_color'] }};">
                            <i class="fa fa-arrow-down"></i>
                            <h4>The Perks</h4>
                        </li>
                        @endif
                    </ul>
                </div>

                @if(count($company['company_perks']) > 0)
                <div class="perks">
                    <ul>

                        @foreach ($company['company_perks'] as $perk)
                            @if($perk[0]['value'] != '')
                                <li>
                                    <div class="perks-icon-wrapper"><i class="{{ $perk[2]['value'] }}"></i></div>
                                    {{ $perk[0]['value'] }}
                                </li>
                            @endif
                        @endforeach

                    </ul>
                </div>
                @endif

                <div class="links">
                    @if($company['company_video'] !== '')
                    <a href="{{ $company['company_video'] }}" class="view-video btn venobox btn-block"
                       data-type="youtube">View Company Video</a>
                    @endif
                    @if(count($company['company_perks']) > 0)
                    <a style="background: {{ $company['company_color'] }}; border-color: {{ $company['company_color'] }};" 
                        href="#the-perks" onclick="openVenobox()" class="view-perks btn venobox-inline btn-block" data-type="inline">View All The
                        Perks</a>
                    @endif
                </div>
               

                <div class="title">
                    <h3>Skill</h3>
                </div>

                <div class="job-content-skills text-center">
                    <img src="{{ $company['meter'][$job['experience_level']] }}" alt=""
                         class="meter" style="background: {{ $company['company_color'] }};">
                </div>

                <div class="title">
                    <h3>Job Skills</h3>
                </div>

                <div class="job-content-job-skills">

                    <ul>

                        @foreach ($job['skills']  as $index => $skill)

                            <li>
                                <span class="title">{{$skill['name']}}</span>

                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="100"
                                         aria-valuemin="0" aria-valuemax="{{$skill['level'] * 10}}" style="width: {{$skill['level'] * 10}}%">
                                        <span class="sr-only">{{$skill['level'] * 10}}% Complete</span>
                                    </div>
                                </div>
                            </li>

                        @endforeach

                    </ul>

                </div>


                <div class="title">
                    <h3>Share Listing</h3>
                </div>

                <div class="share-listing">
                    <div class="social">
                        <ul>
                            @foreach ($company['social_networks'] as $social)
                                @if($social[0]['value'] != '')
                                    @if (isset($social[2]['sharelink']))
                                    <li class="{{ substr($social[0]['value'], 6) }}">
                                        <a href="{{ $social[2]['sharelink'] }}">
                                            <i class="{{ $social[0]['value'] }}"></i>
                                        </a>
                                    </li>
                                    @endif
                                @endif
                            @endforeach  
                        </ul>
                    </div>
                </div>


            </aside>
            <article class="col-sm-8">
                <div class="content">
                    <p><strong>{{ $job['job_title'] }}</strong></p>
                    <br/>
                    <br/>
                    {!! $job['job_description'] !!}

                    <br/>
                </div>

                <div class="form">
                @if(!$if_authenticated) 
                    <form method="post" action="{{ action('GuestUsersController@jobApply') }}" accept-charset="UTF-8" enctype="multipart/form-data">
                        <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                        <input name="guest_applicant" type="hidden" value="yes"/>
                        <input name="company_name_slug" type="hidden" value="{{$company_name_slug}}"/>
                        <input name="job_title" type="hidden" value="{{$job_title}}"/>
                        <input name="jobs_id" type="hidden" value="{{$job_id}}"/>
                        
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="">Contant Person : {{$job['contact_name']}}</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="">Contant Number : {{$job['contact_number']}}</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group {{ $errors->has('firstname') ? ' has-error' : '' }}">
                                    <label for="firstname">First Name</label>
                                    <input type="text" class="form-control" name="firstname" id="firstname" 
                                        value="{{ old('firstname') }}">
                                    @if ($errors->has('firstname'))
                                        <span class="help-block">
                                            <b>{{ $errors->first('firstname') }}</b>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group {{ $errors->has('lastname') ? ' has-error' : '' }}">
                                    <label for="lastname">Last Name</label>
                                    <input type="text" class="form-control" name="lastname" id="lastname"
                                        value="{{ old('lastname') }}">
                                    @if ($errors->has('lastname'))
                                        <span class="help-block">
                                            <b>{{ $errors->first('lastname') }}</b>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email">Email Address</label>
                                    <input type="text" class="form-control" name="email" id="email"
                                        value="{{ old('email') }}">
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <b>{{ $errors->first('email') }}</b>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group {{ $errors->has('linkedin_profile') ? ' has-error' : '' }}">
                                    <label for="linkedin_profile">Linkedin Profile</label>
                                    <input type="text" class="form-control" name="linkedin_profile" id="linkedin_profile" value="{{ old('linkedin_profile') }}">
                                    @if ($errors->has('linkedin_profile'))
                                        <span class="help-block">
                                            <b>{{ $errors->first('linkedin_profile') }}</b>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group {{ $errors->has('curriculum_vitae') ? ' has-error' : '' }}">
                                    <label for="email">Upload CV</label>
                                    <div class="upload-box">
                                        <p id="no-file-selected" class="title" style="position: absolute;left: 145px;">No File Selected</p>
                                        <a class="file-input-wrapper btn upload-btn col-sm-4 ng-isolate-scope">
                                            <span>Add File</span>
                                            <input onchange="onFileChanged()" class="btn-warning col-sm-7 ng-isolate-scope" type="file"        title="Add File" name="curriculum_vitae"
                                                    style="position: absolute; left: 0; top: 0px;opacity: 0; z-index: 99; outline: 0;">
                                        </a>
                                    </div>
                                    <script type="text/javascript">
                                        onFileChanged = function () {
                                            if (event.target.value == '') {
                                                document.getElementById('no-file-selected').innerText = 'No File Selected';
                                            } else {
                                                var slash = event.target.value.split('\\');
                                                document.getElementById('no-file-selected').innerText = slash[slash.length - 1];
                                            }
                                        };
                                    </script>
                                </div>
                                 <div style="margin-top: 40px;" class="form-group {{ $errors->has('curriculum_vitae') ? ' has-error' : '' }}">
                                    @if ($errors->has('curriculum_vitae'))
                                        <span class="help-block">
                                            <b>{{ $errors->first('curriculum_vitae') }}</b>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-6">

                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="email">Add Cover Letter</label>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="addcoverletter" onchange="onCheckboxChanged()" value="true" checked>
                                            Yes, I want to add a cover letter to my application.
                                        </label>
                                    </div>
                                    <script type="text/javascript">
                                        onCheckboxChanged = function () {
                                            document.getElementById('cover_leter_id').disabled = !event.target.checked;
                                        };
                                    </script>
                                </div>
                            </div>
                            <div class="col-sm-6">

                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group {{ $errors->has('cover_leter') ? ' has-error' : '' }}">
                                    <label for="email">Cover Letter</label>
                                    <textarea class="form-control" name="cover_leter" id="cover_leter_id" cols="30" rows="10" >{{ old('cover_leter') }}</textarea>
                                    @if ($errors->has('cover_leter'))
                                        <span class="help-block">
                                            <b>{{ $errors->first('cover_leter') }}</b>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <button type="submit">Apply Now</button>
                            </div>
                        </div>
                    </form>
                @else 

                    @if (strtolower($user_type) === 'candidate') 
                        <form method="post" action="{{ action('GuestUsersController@jobApply') }}" accept-charset="UTF-8" enctype="multipart/form-data">
                            <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                            <input name="guest_applicant" type="hidden" value="no"/>
                            <input name="company_name_slug" type="hidden" value="{{$company_name_slug}}"/>
                            <input name="job_title" type="hidden" value="{{$job_title}}"/>
                            <input name="jobs_id" type="hidden" value="{{$job_id}}"/>
                            
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="">Contact Person : {{$job['contact_name']}}</label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="">Contact Number : {{$job['contact_number']}}</label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="email">Add Cover Letter</label>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="addcoverletter" onchange="onCheckboxChanged()" value="true" checked>
                                            Yes, I want to add a cover letter to my application.
                                        </label>
                                    </div>
                                    <script type="text/javascript">
                                        onCheckboxChanged = function () {
                                            document.getElementById('cover_leter_id').disabled = !event.target.checked;
                                        };
                                    </script>

                                    </div>
                                </div>
                                <div class="col-sm-6">

                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group {{ $errors->has('cover_leter') ? ' has-error' : '' }}">
                                        <label for="email">Cover Letter</label>
                                        <textarea class="form-control" name="cover_leter" id="cover_leter_id" cols="30" rows="10" >{{ old('cover_leter') }}</textarea>
                                        @if ($errors->has('cover_leter'))
                                            <span class="help-block">
                                                <b>{{ $errors->first('cover_leter') }}</b>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <button type="submit">Apply Now</button>
                                </div>
                            </div>
                        </form>
                    @elseif (strtolower($user_type) === 'employer' || strtolower($user_type) === 'none') 
                        <form action="">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="">Contact Person : {{$job['contact_name']}}</label>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="">Contact Number : {{$job['contact_number']}}</label>
                                    </div>
                                </div>
                            </div>
                        </form>
                    @endif

                @endif

                </div>


            </article>
        </div>
    </div>
</section>


<div class="container">
    <div class="row">
        <div class="col-xs-5">
            <div id="the-perks" style="display:none;">
                <div class="perks">
                    <ul>

                        @foreach ($company['company_perks'] as $perk)
                            @if($perk[0]['value'] != '')
                                <li>
                                    <div class="perks-icon-wrapper"><i class="{{ $perk[2]['value'] }}"></i></div>
                                    <div class="perks-description-wrapper"><span>{{ $perk[0]['value'] }}</span><br>

                                        <p>
                                         {!!  $perk[1]['value'] !!}
                                        </p>
                                    </div>
                                </li>
                            @endif
                        @endforeach
                       
                    </ul>
                </div>

            </div>

        </div>
    </div>
</div>

<div id="powered-by-footer">
    <div class="container">
        <div class="row">
            <div class="text-center">
                <img
                        class="lightbox-false"
                        alt="" title=""
                        src="{{ asset('img/poweredby-white.png') }}">

            </div>
        </div>
    </div>
</div>
<footer style="background-color: {{ $company['company_color'] }};">
    <div class="container">
        <div class="row">
            <div class="text-center">
                @if ($company['company_logo']) 
                    <img src="{{ $company['company_logo'] }}" alt=""
                         class="company-logo">
                @endif
            </div>
        </div>
    </div>
</footer>


<script src="{{ asset('frontend/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('frontend/plugins/venobox/venobox.min.js') }}"></script>

<script>
    $(function () {
        $.scrollUp({
            scrollName: 'scrollUp',
            animation: 'fade',
            scrollText: '<i class="fa fa-chevron-up"></i>'
        });

        $('.venobox').venobox();

        $('.venobox-inline').venobox({
            framewidth: '600px',        // default: ''
            frameheight: '1640px',
            border: '10px'
        });

        openVenobox = function () {
            setTimeout(function () {
                $('.vbox-inline.figlio').css({
                    height: $('.vbox-inline ul').height() + 20 + 'px'
                });
                $('.vbox-preloader').css('display', 'none');
            }, 500);
        };
    });
</script>

</body>
</html>