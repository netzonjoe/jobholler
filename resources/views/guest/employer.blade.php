@extends('layouts.guest')


@section('content')

<div id="DIV_37">
    <div id="DIV_38">
        <div id="DIV_39">
            <div id="DIV_40">
            </div>
        </div>
    </div>
    <div id="DIV_41">
        <div id="DIV_42">
            <div id="DIV_43">
                <div id="DIV_44">
                </div>
                <div id="DIV_45">
                    <h2 id="H2_46">
                        <span id="SPAN_47"></span>
                        <p id="P_48">
                            Employers
                        </p>
                    </h2>
                    <div id="DIV_49">
                    </div>
                    <h2 id="H2_50">
                        <span id="SPAN_51">Want to boost your recruitment marketing and digital employer branding? Check out our packages below!</span>
                        <p id="P_52">
                        </p>
                    </h2>
                    <div id="DIV_53">
                    </div>
                </div>
                <div id="DIV_54">
                </div>
            </div>
        </div>
    </div>
    <div id="DIV_55">
        <div id="DIV_56">
            <div id="DIV_57">
                <!-- Row Backgrounds -->

                <div id="DIV_58">
                    <div id="DIV_59">
                    </div>
                    <div id="DIV_60">
                        <div id="DIV_61">
                            <div id="DIV_62">
                            </div>
                        </div>
                        <div id="DIV_63">
                        </div>
                        <h2 id="H2_64">
                            <span id="SPAN_65"></span>
                            <p id="P_66">
                                Submit Job
                            </p>
                        </h2>
                        <div id="DIV_67">
                        </div>
                        <div id="DIV_68">
                             <span id="SPAN_69"><span id="SPAN_70"></span></span>
                            <h4 id="H4_71">FREE</h4> 
                            <span id="SPAN_72"><span id="SPAN_73"></span></span>
                        </div>
                        <div id="DIV_74">
                            <div id="DIV_75">
                                <div id="DIV_76">
                                    <div id="DIV_77">
                                    </div>
                                </div>
                                <div id="DIV_78">
                                </div>
                                <h2 id="H2_79">
                                    <span id="SPAN_80"></span>
                                    <p id="P_81">
                                        Unlimited
                                    </p>
                                </h2>
                                <div id="DIV_82">
                                </div>
                                <h2 id="H2_83">
                                    <span id="SPAN_84"></span>
                                    <p id="P_85">
                                        Job Submissions to our job portal per month
                                    </p>
                                </h2>
                                <div id="DIV_86">
                                </div>
                            </div>
                        </div>
                        <div id="DIV_87">
                            <div id="DIV_88">
                            </div>
                        </div>
                        <div id="DIV_89">
                            <h2 id="H2_90">
                                <span id="SPAN_91"><a href="{{action('GuestUserController@submitvacancy')}}" id="A_92">Submit Vacancy…</a><br id="BR_94" /></span>
                            </h2>
                            <div id="DIV_95">
                            </div>
                            <div id="DIV_96">
                            </div>
                        </div>
                        <div id="DIV_97">
                            <div id="DIV_98">
                            </div>
                        </div>
                        <div id="DIV_99">
                        </div>
                        <div id="DIV_100">
                            <div id="DIV_101">
                            </div>
                        </div>
                    </div>
                    <div id="DIV_102">
                        <div id="DIV_103">
                            <div id="DIV_104">
                            </div>
                        </div>
                        <div id="DIV_105">
                        </div>
                        <h2 id="H2_106">
                            <span id="SPAN_107"></span>
                            <p id="P_108">
                                Option 1
                            </p>
                        </h2>
                        <div id="DIV_109">
                        </div>
                        <div id="DIV_110">
                            <span id="SPAN_111"><span id="SPAN_112"></span></span>
                            <h4 id="H4_113">£995 + VAT per month</h4> 
                            <span id="SPAN_114"><span id="SPAN_115"></span></span>
                        </div>
                        <div id="DIV_116">
                            <div id="DIV_117">
                                <div id="DIV_118">
                                    <div id="DIV_119">
                                    </div>
                                </div>
                                <div id="DIV_120">
                                </div>
                                <h2 id="H2_121">
                                    <span id="SPAN_122"></span>
                                    <p id="P_123">
                                        Unlimited
                                    </p>
                                </h2>
                                <div id="DIV_124">
                                </div>
                                <h2 id="H2_125">
                                    <span id="SPAN_126"></span>
                                    <p id="P_127">
                                        Job Submissions to our job portal per month
                                    </p>
                                </h2>
                                <div id="DIV_128">
                                </div>
                                <div id="DIV_129">
                                    <div id="DIV_130">
                                    </div>
                                </div>
                                <div id="DIV_131">
                                </div>
                                <h2 id="H2_132">
                                    <span id="SPAN_133"></span>
                                    <p id="P_134">
                                        Featured job posts on our job portal
                                    </p>
                                </h2>
                                <div id="DIV_135">
                                </div>
                                <div id="DIV_136">
                                    <div id="DIV_137">
                                    </div>
                                </div>
                                <div id="DIV_138">
                                </div>
                                <h2 id="H2_139">
                                    <span id="SPAN_140"></span>
                                    <p id="P_141">
                                        Careers portal complete with your branding
                                    </p>
                                </h2>
                                <div id="DIV_142">
                                </div>
                                <div id="DIV_143">
                                    <div id="DIV_144">
                                    </div>
                                </div>
                                <div id="DIV_145">
                                </div>
                                <h2 id="H2_146">
                                    <span id="SPAN_147"></span>
                                    <p id="P_148">
                                        Up to 3 roles amplified weekly to our Social media Channels
                                    </p>
                                </h2>
                                <div id="DIV_149">
                                </div>
                                <div id="DIV_150">
                                    <div id="DIV_151">
                                    </div>
                                </div>
                                <div id="DIV_152">
                                </div>
                                <h2 id="H2_153">
                                    <span id="SPAN_154"></span>
                                    <p id="P_155">
                                        Up to 3 roles amplified weekly to the Job Boards (Monster, Indeed, Adzuna, Dice, Technojobs, CW Jobs)
                                    </p>
                                </h2>
                                <div id="DIV_156">
                                </div>
                                <div id="DIV_157">
                                    <div id="DIV_158">
                                    </div>
                                </div>
                                <div id="DIV_159">
                                </div>
                                <h2 id="H2_160">
                                    <span id="SPAN_161"></span>
                                    <p id="P_162">
                                        1 paid Facebook Campaign per month
                                    </p>
                                </h2>
                                <div id="DIV_163">
                                </div>
                                <div id="DIV_164">
                                    <div id="DIV_165">
                                    </div>
                                </div>
                                <div id="DIV_166">
                                </div>
                                <h2 id="H2_167">
                                    <span id="SPAN_168"></span>
                                    <p id="P_169">
                                        Monthly report
                                    </p>
                                </h2>
                                <div id="DIV_170">
                                </div>
                            </div>
                        </div>
                        <div id="DIV_171">
                            <div id="DIV_172">
                            </div>
                        </div>
                        <div id="DIV_173">
                            <h2 id="H2_174">
                                <span id="SPAN_175"><a href="{{ action('GuestUserController@checkoutAmplify') }}" id="A_176">Subscribe…</a></span>
                                <p id="P_178">
                                </p>
                            </h2>
                            <div id="DIV_180">
                            </div>
                            <div id="DIV_181">
                            </div>
                        </div>
                        <div id="DIV_182">
                            <div id="DIV_183">
                            </div>
                        </div>
                        <div id="DIV_184">
                        </div>
                        <div id="DIV_185">
                            <div id="DIV_186">
                            </div>
                        </div>
                    </div>
                    <div id="DIV_187">
                        <div id="DIV_188">
                            <div id="DIV_189">
                            </div>
                        </div>
                        <div id="DIV_190">
                        </div>
                        <h2 id="H2_191">
                            <span id="SPAN_192"></span>
                            <p id="P_193">
                                Option 2
                            </p>
                        </h2>
                        <div id="DIV_194">
                        </div>
                        <div id="DIV_195">
                            <span id="SPAN_196"><span id="SPAN_197"></span></span>
                            <h4 id="H4_198">£1995 + VAT per month</h4> 
                            <span id="SPAN_199"><span id="SPAN_200"></span></span>
                        </div>
                        <div id="DIV_201">
                            <div id="DIV_202">
                                <div id="DIV_203">
                                    <div id="DIV_204">
                                    </div>
                                </div>
                                <div id="DIV_205">
                                </div>
                                <h2 id="H2_206">
                                    <span id="SPAN_207"></span>
                                    <p id="P_208">
                                        Unlimited
                                    </p>
                                </h2>
                                <div id="DIV_209">
                                </div>
                                <h2 id="H2_210">
                                    <span id="SPAN_211"></span>
                                    <p id="P_212">
                                        Job Submissions to our job portal per month
                                    </p>
                                </h2>
                                <div id="DIV_213">
                                </div>
                                <div id="DIV_214">
                                    <div id="DIV_215">
                                    </div>
                                </div>
                                <div id="DIV_216">
                                </div>
                                <h2 id="H2_217">
                                    <span id="SPAN_218"></span>
                                    <p id="P_219">
                                        Featured+ job posts on our job portal
                                    </p>
                                </h2>
                                <div id="DIV_220">
                                </div>
                                <div id="DIV_221">
                                    <div id="DIV_222">
                                    </div>
                                </div>
                                <div id="DIV_223">
                                </div>
                                <h2 id="H2_224">
                                    <span id="SPAN_225"></span>
                                    <p id="P_226">
                                        Careers portal complete with your branding
                                    </p>
                                </h2>
                                <div id="DIV_227">
                                </div>
                                <div id="DIV_228">
                                    <div id="DIV_229">
                                    </div>
                                </div>
                                <div id="DIV_230">
                                </div>
                                <h2 id="H2_231">
                                    <span id="SPAN_232"></span>
                                    <p id="P_233">
                                        Up to 10 roles amplified weekly to our Social media Channels
                                    </p>
                                </h2>
                                <div id="DIV_234">
                                </div>
                                <div id="DIV_235">
                                    <div id="DIV_236">
                                    </div>
                                </div>
                                <div id="DIV_237">
                                </div>
                                <h2 id="H2_238">
                                    <span id="SPAN_239"></span>
                                    <p id="P_240">
                                        Up to 10 roles amplified weekly to the Job Boards (Monster, Indeed, Adzuna, Dice, Technojobs, CW Jobs)
                                    </p>
                                </h2>
                                <div id="DIV_241">
                                </div>
                                <div id="DIV_242">
                                    <div id="DIV_243">
                                    </div>
                                </div>
                                <div id="DIV_244">
                                </div>
                                <h2 id="H2_245">
                                    <span id="SPAN_246"></span>
                                    <p id="P_247">
                                        2 paid Facebook Campaigns per month
                                    </p>
                                </h2>
                                <div id="DIV_248">
                                </div>
                                <div id="DIV_249">
                                    <div id="DIV_250">
                                    </div>
                                </div>
                                <div id="DIV_251">
                                </div>
                                <h2 id="H2_252">
                                    <span id="SPAN_253"></span>
                                    <p id="P_254">
                                        Bespoke Twitter account + following build and content for your company managed by Job Holler
                                    </p>
                                </h2>
                                <div id="DIV_255">
                                </div>
                                <div id="DIV_256">
                                    <div id="DIV_257">
                                    </div>
                                </div>
                                <div id="DIV_258">
                                </div>
                                <h2 id="H2_259">
                                    <span id="SPAN_260"></span>
                                    <p id="P_261">
                                        Enhanced employer branding through published blogs and articles
                                    </p>
                                </h2>
                                <div id="DIV_262">
                                </div>
                                <div id="DIV_263">
                                    <div id="DIV_264">
                                    </div>
                                </div>
                                <div id="DIV_265">
                                </div>
                                <h2 id="H2_266">
                                    <span id="SPAN_267"></span>
                                    <p id="P_268">
                                        Monthly report
                                    </p>
                                </h2>
                                <div id="DIV_269">
                                </div>
                            </div>
                        </div>
                        <div id="DIV_270">
                            <div id="DIV_271">
                            </div>
                        </div>
                        <div id="DIV_272">
                            <h2 id="H2_273">
                                <span id="SPAN_274"><a href="{{ action('GuestUserController@checkoutIgnite') }}" id="A_275">Subscribe…</a></span>
                                <p id="P_277">
                                </p>
                            </h2>
                            <div id="DIV_279">
                            </div>
                            <div id="DIV_280">
                            </div>
                        </div>
                        <div id="DIV_281">
                            <div id="DIV_282">
                            </div>
                        </div>
                        <div id="DIV_283">
                        </div>
                        <div id="DIV_284">
                            <div id="DIV_285">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Row Backgrounds -->

                <div id="DIV_286">
                    <div id="DIV_287">
                        <div id="DIV_288">
                            <div id="DIV_289">
                            </div>
                        </div>
                        <div id="DIV_290">
                        </div>
                    </div>
                </div>
                <div id="DIV_291">
                    <div id="DIV_292">
                        <div id="DIV_293">
                            <p id="P_294">
                                Our packages are priced at manageable monthly costs with no extra fees if you place a candidate who applies directly through the Job Holler campaign!
                            </p>
                            <div id="DIV_295">
                            </div>
                        </div>
                    </div>
                    <div id="DIV_296">
                        <div id="DIV_297">
                            <p id="P_298">
                                Want a more bespoke package? Contact us today to speak to one of our Job Holler Account Managers – <a href="tel:01244567967" id="A_299">01244 567 967</a> / <a href="mailto:info@jobholler.com" id="A_300">info@jobholler.com</a>.
                            </p>
                            <div id="DIV_301">
                            </div>
                        </div>
                    </div>
                </div>
                <div id="DIV_302">
                </div>
            </div>
            <div id="DIV_303">
            </div>
        </div>
        <div id="DIV_304">
        </div>
    </div>
</div>

@endsection