<section class="pricing-table">
    <header> <h2>{{$title}}</h2> </header>
    <p class="pricing-price"> &pound;{{$price}}<span>/{{$per_payment_type}}</span> </p>
    <p class="pricing-price-detail">{{$description}}</p>
</section>