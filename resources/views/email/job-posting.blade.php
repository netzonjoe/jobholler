<section class="pricing-table">
    <header> <h2>{{$job_title}}</h2> </header>
    <p class="pricing-price-detail">{{$job_type}}</p>
    <p class="pricing-price-detail">{{$minimum_salary}}</p>
    <p class="pricing-price-detail">{{$maximum_salary}}</p>
    <p class="pricing-price-detail">{{$paid_per}}</p>
    <p class="pricing-price-detail">{{$job_location}}</p>
    <p class="pricing-price-detail">{{$job_description}}</p>
    <p class="pricing-price-detail">{{$contact_name}}</p>
    <p class="pricing-price-detail">{{$contact_number}}</p>
    <p class="pricing-price-detail">{{$reference}}</p>
    <p class="pricing-price-detail">{{$company_name}}</p>
</section>
