@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            

                @if (!isset($goto_payment) || !$goto_payment) 
                <div class="panel panel-default">
                    <div class="panel-heading">Account Information</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                            {!! csrf_field() !!}


                            <div class="form-group{{ $errors->has('user_type') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">User Group</label>
                                
                                <div class="col-md-6"> 
                                    <select name="user_type" class="selectpicker form-control" required>

                                        <option value="">Select User Group</option>
                                        @foreach ($user_groups as $name => $user_group)
                                        <option data-divider="true"></option>
                                            @foreach ($user_group as $type)
                                                <option 
                                                    data-subtext="{{$type['sub_role']}}"
                                                    value="{{$type['id']}}" 
                                                    {{(old('user_type') == $type['id']) ? 'selected="selected"' : '' }}>
                                                    {{$type['name']}}</option>
                                            @endforeach
                                        @endforeach
                                        
                                    </select>
                                  
                                    @if ($errors->has('user_type'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('user_type') }}</strong>
                                        </span>
                                    @endif

                                </div>
                            </div>


                            <div class="form-group{{ $errors->has('firstname') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">First Name</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="firstname" value="{{ old('firstname') }}">

                                    @if ($errors->has('firstname'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('firstname') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Last Name</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="lastname" value="{{ old('lastname') }}">

                                    @if ($errors->has('lastname'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('lastname') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Phone</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control input-medium bfh-phone " name="phone" value="{{ old('phone') }}" data-format="(ddd) ddd-dddd">

                                    @if ($errors->has('phone'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('phone') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">E-Mail Address</label>

                                <div class="col-md-6">
                                    <input type="email" class="form-control" name="email" value="{{ old('email') }}">

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Password</label>

                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="password">

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Confirm Password</label>

                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="password_confirmation">

                                    @if ($errors->has('password_confirmation'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-btn fa-user"></i>Register Now
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                @else

                    @if (isset($goto_payment)) 
                    <div class="panel panel-default">
                        <div class="panel-heading">Billing Details</div>
                        <div class="panel-body">
                            <form id="billing-form" class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                                {!! csrf_field() !!}

                                <div class="form-group">
                                    <label class="col-md-8 control-label">
                                        <img src="{{ asset('/img/cclogos.gif') }}" width="199" height="30" alt="Visa, Master Card, American Express, Discover">
                                    </label>
                                </div>

                                 <div class="form-group "> <!-- has-error -->
                                    <label class="col-md-4 control-label ">Price </label>
                                    
                                    <label class="col-md-6 control-label  col-md-pull-3">
                                        &pound;{{$price}} every 30 days
                                    </label>
                                </div>


                                <div class="form-group number "> <!-- has-error -->
                                    <label class="col-md-4 control-label">Card Number</label>
                                    
                                    <div class="col-md-6">
                                        <input type="text" data-stripe="number" 
                                                class="form-control input-medium bfh-phone " 
                                                data-format="dddd dddd dddd dddd">
                                        
                                        <span class="help-block number hidden">
                                            <strong></strong>
                                        </span>
                                        

                                    </div>
                                </div>

                                <div class="form-group cvc"> <!-- has-error -->
                                    <label class="col-md-4 control-label">CVC</label>
                                    
                                    <div class="col-md-6">
                                        <input type="text" data-stripe="cvc" 
                                                class="form-control input-medium bfh-phone " 
                                                data-format="ddd">
                                        
                                        <span class="help-block cvc hidden">
                                            <strong></strong>
                                        </span>
                                        

                                    </div>
                                </div>

                                <div class="form-group "> <!-- has-error -->
                                    <label class="col-md-4 control-label">Expiration Date</label>
                                    
                                    <div class="col-md-3 exp_month">
                                        <select class="form-control" data-stripe="exp-month" >
                                            <option value="1">January</option>
                                            <option value="2">February</option>
                                            <option value="3">March</option>
                                            <option value="4">April</option>
                                            <option value="5">May</option>
                                            <option value="6">June</option>
                                            <option value="7">July</option>
                                            <option value="8">August</option>
                                            <option value="9">September</option>
                                            <option value="10">October</option>
                                            <option value="11">November</option>
                                            <option value="12">December</option>
                                        </select>

                                        <span class="help-block exp_month hidden">
                                            <strong></strong>
                                        </span>
                                    </div>
                                    <div class="col-md-3 exp_year">
                                        <select class="form-control" data-stripe="exp-year" >
                                            @for ($d = date('Y'); $d < date('Y') + 20; $d++)
                                                <option value="{{$d}}">{{$d}}</option>
                                            @endfor
                                        </select>
                                        <span class="help-block exp_year hidden">
                                            <strong></strong>
                                        </span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary">
                                            <i class="fa fa-btn fa-user"></i>Complete Registration
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    @endif
                @endif

            
        </div>
    </div>
</div>
@endsection


@section('scripts-files')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
    <script type="text/javascript" src="{{ asset('/js/libs/stripe/stripe.min.js') }}"></script>
    <script type="text/javascript">
        (function () {
            $('.selectpicker').selectpicker({
              showSubtext: true
            });


            var StringBilling = {
                publishable_key: '{{Config::get("services.stripe.key")}}',
                init: function () {
                    this.form = $('#billing-form');
                    this.submitButton = this.form.find('button[type=submit]'); 
                    Stripe.setPublishableKey(this.publishable_key);
                    this.bindEvents();
                },
                bindEvents: function () {
                    this.form.on('submit', $.proxy(this.sendToken, this));
                },
                sendToken: function (event) {
                    if (this.error_message !== undefined) {
                        if (!this.error_message.hasClass('hidden')) {
                            this.error_message.addClass('hidden');
                        }
                    }
                    if (this.input_wrapper !== undefined) {
                        if (this.input_wrapper.hasClass('has-error')) {
                            this.input_wrapper.removeClass('has-error');
                        }   
                    }

                    this.submitButton.prop('disabled', true);
                    Stripe.createToken(this.form, $.proxy(this.stripeResponseHandler, this));
                    event.preventDefault();
                },
                stripeResponseHandler: function (status, response) {
                    this.submitButton.prop('disabled', false);
                    if (status === 402) {
                        var error_name = response.error.param;
                        this.input_wrapper = this.form.find('div.' + error_name);
                        this.input_wrapper.addClass('has-error');
                        this.error_message = this.input_wrapper.find('span.' + error_name);
                        this.error_message.removeClass('hidden');
                        this.error_message.find('strong').text(response.error.message);
                    } else if (status === 200) {
                        $('<input>', {
                            type: 'hidden',
                            name: 'stripeToken',
                            value: response.id
                        }).appendTo(this.form);
                        this.form[0].submit();
                    }
                    console.log(status, response);
                }
            };

            StringBilling.init();
        })();
    </script>
@endsection

