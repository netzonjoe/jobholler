@extends('layouts.employer-dashboard')

@section('content')

<div id="content" class="content-container" ng-controller="JobhollerCtrl">
  {!! html_entity_decode($angular_directive_alert) !!}
  <section data-ng-view class="view-container [[main.pageTransition.class]]"></section>
</div>

@endsection


@section('scripts-files')

<script src="http://maps.googleapis.com/maps/api/js?sensor=false&libraries=places" type="text/javascript"></script>
<script type="text/javascript">


(function () {
	$_token = '{{$_token}}';
	$route_list = JSON.parse('{!!$route_list!!}');
	$baseurl = '{{$baseurl}}';
	$imagesDir = '{{$imagesDir}}';
	$job_listing = '{{$job_listing}}';
	$jobs = [];// JSON.parse('{!! $jobs !!}');
	$expired_jobs = [];//JSON.parse('{!!$expired_jobs!!}');
})();

</script>

@endsection