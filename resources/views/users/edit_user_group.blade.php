@extends('layouts.app')


@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
        	<div class="panel panel-default">
        		<div class="panel-heading"><i class="fa fa-pencil-square-o"></i> Edit User Group</div>

        		<div class="panel-body">

        			<form class="form-horizontal" role="form" method="POST" action="{{ action('SystemUserController@saveEditUserGroup') . '?edit=' . $edit_id }}">
                    {!! csrf_field() !!}

	                    <div class="form-group{{ $errors->has('user_group_name') ? ' has-error' : '' }}">
	                        <label class="col-md-4 control-label">User Group Name</label>

	                        <div class="col-md-6">
	                            <input type="text" class="form-control" name="user_group_name" value="{{ old('user_group_name') }}">
	                            @if ($errors->has('user_group_name'))
	                                <span class="help-block">
	                                    <strong>{{ $errors->first('user_group_name') }}</strong>
	                                </span>
	                            @endif
	                        </div>
	                    </div>

	                    <div class="form-group{{ $errors->has('sub_role') ? ' has-error' : '' }}">
	                        <label class="col-md-4 control-label">Sub Role Name</label>

	                        <div class="col-md-6">
	                            <input type="text" class="form-control" disabled="disabled" name="sub_role" >
	                            @if ($errors->has('sub_role'))
	                                <span class="help-block">
	                                    <strong>{{ $errors->first('sub_role') }}</strong>
	                                </span>
	                            @endif
	                        </div>
	                        <div class="col-md-1">
	                        	<input type="checkbox"  
	                        		name="is_checked_sub_role"
	                        	onchange="$(this).parent().parent().find('input[name=sub_role]').prop('disabled', !this.checked);
	                        	if(!this.checked)$(this).parent().parent().find('input[name=sub_role]').val('');">
	                        </div>
	                    </div>
	                 
	                    <div class="form-group">
	                        <label class="col-md-4 control-label">Access Permission</label>

	                        <div class="col-md-6">
	                            
              					<div class="well well-sm" style="height: 150px; overflow: auto;">
              						@foreach ($get_uris as $uri)
									    <div class="checkbox">
	                  						<label>
	                  							<input type="checkbox" 
	                  							{{(isset($user_permissions_access[$uri])) ? ($user_permissions_access[$uri]['is_enabled'] === 1) ? 'checked' : '' : ''}}
	                  							name="permission[access][]" value="{{ $uri }}">{{ $uri }}
	                  						</label>
	                					</div>
									@endforeach
                              	</div>
              					<a onclick="$(this).parent().find(':checkbox').prop('checked', true);">Select All</a> / 
              					<a onclick="$(this).parent().find(':checkbox').prop('checked', false);">Unselect All</a>
              				</div>
	                    </div>

	                    <div class="form-group">
	                        <label class="col-md-4 control-label">Modify Permission</label>

	                        <div class="col-md-6">
	                            
              					<div class="well well-sm" style="height: 150px; overflow: auto;">
                                	@foreach ($post_uris as $uri)
									    <div class="checkbox">
	                  						<label>
	                  							<input type="checkbox" 
	                  							{{(isset($user_permissions_modify[$uri])) ? ($user_permissions_modify[$uri]['is_enabled'] === 1) ? 'checked' : '' : ''}}
	                  							name="permission[modify][]" value="{{ $uri }}">{{ $uri }}
	                  						</label>
	                					</div>
									@endforeach
                              	</div>
              					<a onclick="$(this).parent().find(':checkbox').prop('checked', true);">Select All</a> / 
              					<a onclick="$(this).parent().find(':checkbox').prop('checked', false);">Unselect All</a>
              				</div>
	                    </div>


	                    <div class="form-group">
	                        <div class="col-md-6 col-md-offset-4">
	                            <button type="submit" class="btn btn-primary">
	                                <i class="fa fa-btn fa-floppy-o"></i>Save 
	                            </button>

	                            <a href="{{ action('SystemUserController@listUserGroup') }}" class="btn btn-default">
	                                <i class="fa fa-btn fa-reply"></i>Cancel 
	                            </a>
	                        </div>

	                    </div>
	                </form>

        		</div>
        		
        	</div>

        </div>
    </div>
</div>

@endsection



@section('scripts-files')
<script type="text/javascript">
	
(function () {
	var edit_page = '{{$edit_page}}';
	if (edit_page) {
		var user_group_name = "{{$user_group['name']}}";
		var sub_role = "{{$user_group['sub_role']}}";
		$('input[name=user_group_name]').val(user_group_name);
		$('input[name=sub_role]').val(sub_role);
		if (sub_role.trim() !== '') {
			$('input[name=is_checked_sub_role]').prop('checked', true);
			$('input[name=sub_role]').prop('disabled', false);
		}		
	}
	
})();

</script>
@endsection