@extends('layouts.app')


@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
        	<div class="panel panel-default">
        		<div class="panel-heading clearfix "><i class="fa fa-list-alt"></i> User
					<div class="pull-right">
						<a href="{{ action('SystemUserController@addUser') }}" 
							data-toggle="tooltip" title="" 
							class="btn btn-primary" data-original-title="Add New">
							<i class="fa fa-plus"></i>
						</a>
						<button type="button" data-toggle="tooltip" title="" 
								class="btn btn-danger" 
								onclick="confirm('Are you sure?') ? $('#form-user-group').submit() : false;" 
								data-original-title="Delete">
							<i class="fa fa-trash-o"></i>
						</button>
					</div>
        		</div>

        		<div class="panel-body">

        			<form class="form-horizontal" role="form" method="POST" action="{{ action('SystemUserController@deleteUser') }}"  enctype="multipart/form-data" id="form-user-group">
                    {!! csrf_field() !!}

				          <div class="table-responsive">
				            <table class="table table-bordered table-hover">
				              <thead>
				                <tr>
				                  <td style="width: 1px;" class="text-center">
				                  	<input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);">
				                  </td>
				                  <td class="text-left">
				                  	<a href="{{ action('SystemUserController@listUser', $user_group_name ) }}">
				                  	<i class="fa fa-sort-amount-{{$user_group_class_name}}"></i> 
				                  	User Group Name</a>
				                  </td>
				                  <td class="text-left">
				                  	<a href="{{ action('SystemUserController@listUser', $sub_role_name ) }}">
				                  	<i class="fa fa-sort-amount-{{$sub_role_class_name}}"></i> 
				                  		Sub Role Name</a>
				                  </td>
				                  <td class="text-right">Action</td>
				                </tr>
				              </thead>
				              <tbody>
				              	@foreach ($user_groups as $u_group)
				              	
				                <tr>
				                  <td class="text-center"><input type="checkbox" name="selected[]" value="{{$u_group->id}}"></td>
				                  <td class="text-left">{{$u_group->name}}</td>
				                  <td class="text-left">{{$u_group->sub_role}}</td>
				                  <td class="text-right">
				                  	<a href="{{ action('SystemUserController@editUser', 'edit=' . $u_group->id) }}" data-toggle="tooltip" title="" 
				                  		class="btn btn-primary" data-original-title="Edit">
				                  		<i class="fa fa-pencil"></i>
				                  	</a>
				                  </td>
				                </tr>
				                @endforeach
				                
				              </tbody>
				            </table>

				            {!! $user_groups->appends($paginate_params)->render() !!}
				          </div>



	                </form>

        		</div>
        		
        	</div>

        </div>
    </div>
</div>

@endsection