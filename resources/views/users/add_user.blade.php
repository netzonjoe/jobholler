@extends('layouts.app')


@section('content')
<div class="container">
	
     <div class="row">
        <div class="col-md-8 col-md-offset-2">
        	<div class="panel panel-default">
        		<div class="panel-heading"><i class="fa fa-pencil-square-o"></i> Add User Group</div>

        		<div class="panel-body">

        			<form class="form-horizontal" role="form" method="POST" action="{{ action('SystemUserController@addUser') }}">
                    {!! csrf_field() !!}

	                    <div class="form-group{{ $errors->has('firstname') ? ' has-error' : '' }}">
	                        <label class="col-md-4 control-label">First Name</label>

	                        <div class="col-md-6">
	                            <input type="text" class="form-control" name="firstname" value="{{ old('firstname') }}">
	                            @if ($errors->has('firstname'))
	                                <span class="help-block">
	                                    <strong>{{ $errors->first('firstname') }}</strong>
	                                </span>
	                            @endif
	                        </div>
	                    </div>

	                    <div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
	                        <label class="col-md-4 control-label">Last Name</label>

	                        <div class="col-md-6">
	                            <input type="text" class="form-control" name="lastname" value="{{ old('lastname') }}">
	                            @if ($errors->has('lastname'))
	                                <span class="help-block">
	                                    <strong>{{ $errors->first('lastname') }}</strong>
	                                </span>
	                            @endif
	                        </div>
	                    </div>

	                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
	                        <label class="col-md-4 control-label">E-Mail</label>

	                        <div class="col-md-6">
	                            <input type="email" class="form-control" name="email" value="{{ old('email') }}">
	                            @if ($errors->has('email'))
	                                <span class="help-block">
	                                    <strong>{{ $errors->first('email') }}</strong>
	                                </span>
	                            @endif
	                        </div>
	                    </div>

	                    
	                    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
	                        <label class="col-md-4 control-label">Phone</label>

	                        <div class="col-md-6">
	                        	<input type="text" class="form-control input-medium bfh-phone " name="phone" value="{{ old('phone') }}" data-format="(ddd) ddd-dddd">
	                            @if ($errors->has('phone'))
	                                <span class="help-block">
	                                    <strong>{{ $errors->first('phone') }}</strong>
	                                </span>
	                            @endif
	                        </div>
	                    </div>	

	                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-2">
                            	<input type="checkbox" /> class="btn btn-default" <button>Generate</button>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input type="password" class="form-control" name="password_confirmation">

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

	                    <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
	                        <label class="col-md-4 control-label">Status</label>

	                        <div class="col-md-6">
	                        	<select class="form-control" name="status">
                                    @foreach ($status as $key => $value)
                                        <option value="{{$key}}" {{(old('status') == $key)? 'selected' : ''}}> {{$value}}</option>
                                    @endforeach
                                </select>
	                            @if ($errors->has('status'))
	                                <span class="help-block">
	                                    <strong>{{ $errors->first('status') }}</strong>
	                                </span>
	                            @endif
	                        </div>
	                    </div>

	                    <div class="form-group">
	                        <div class="col-md-6 col-md-offset-4">
	                            <button type="submit" class="btn btn-primary">
	                                <i class="fa fa-btn fa-floppy-o"></i>Save 
	                            </button>

	                            <a href="{{ action('SystemUserController@listUserGroup') }}" class="btn btn-default">
	                                <i class="fa fa-btn fa-reply"></i>Cancel 
	                            </a>
	                        </div>

	                    </div>
	                </form>

        		</div>
        		
        	</div>

        </div>
    </div>
</div>

@endsection

<script type="text/javascript">
	// Math.random().toString(36).slice(-8)
</script>